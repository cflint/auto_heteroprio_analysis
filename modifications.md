Détails de tous les modifications réalisés dans le cadre des mesures

# Premières mesures

## ScalFMM

Dans `/projets/schnaps/autoprio/ScalFMM/Src/GroupTree/Core/FGroupTaskStarpuAlgorithm.hpp`,
ajout d'un perfmodel au nom de chacun des codelets, pour obtenir des résultats correct sur Auto-Heteroprio
type de perfmodel: `HISTORY_BASED`

# Priorités manuelles

# ScalFMM

Le fichier `/projets/schnaps/autoprio/ScalFMM/Src/GroupTree/StarPUUtils/FStarPUFmmPrioritiesV2.hpp`
a été mis à jour, pour que l'assignation des priorités soit aux normes de la dernière version Heteroprio

Le fichier `/projets/schnaps/autoprio/ScalFMM/Src/GroupTree/Core/FGroupTaskStarpuAlgorithm.hpp`
a également été édité pour empecher l'inclusion de morceaux de code anciens, ainsi qu'un modification
pour faire référence à la bonne fonction d'initialisation Heteroprio

# Chameleon

Les priorités manuelles pour l'opération _Factorisation de Cholesky_ et indirectement _Multiplication de matrices_
sont basés sur [https://tel.archives-ouvertes.fr/tel-01538516/document]

```
Suraj Kumar. Scheduling of Dense Linear Algebra Kernels on Heterogeneous Resources. Other
[cs.OH]. Université de Bordeaux, 2017. English. ffNNT : 2017BORD0572ff. fftel-01538516
```

Notamment aux alentours de la page 69

Un fichier `/projets/schnaps/autoprio/chameleon/runtime/starpu/include/runtime_heteroprio_priorities.h`
a été rajouté pour gérer l'assignation des priorités. En particulier, l'operation (qui determine des prioritées particulières)
est indiqué à l'aide de la variable d'environnement `HP_OPNAME`.

Le fichier `/projets/schnaps/autoprio/chameleon/runtime/starpu/control/runtime_control.c`
a été modifié pour indiquer la fonction d'initialisation des priorités

Enfin, certains fichier codelet présent dans `/projets/schnaps/autoprio/chameleon/runtime/starpu/codelets/`
ont été modifié pour assigner la bonne priorité à une tâche, dans le cas de l'utilisation d'Heteroprio

# Pastix

Les priorités manuelles sont basés sur une mesure des tâches, sur une execution de taille 50:50:50 (avec 100 le fichier de mesures était trop gros pour LibreOffice), et sur l'éxperience personnelle: connaissance des opérations et autres programmes.

Un fichier `/projets/schnaps/autoptio/pastix/sopalin/starpu/starpu_heteroprio_priorities.h`, similaire à celui de Chaméleon, gère la configuration de Heteroprio et l'ordre des priorités.

Le fichier `/projets/schnaps/autoptio/pastix/sopalin/starpu/starpu.c`
a été modifié pour indiquer la fonction d'initialisation des priorités

Enfin, certains fichier codelet présent dans `/projets/schnaps/autoptio/pastix/sopalin/starpu/`
ont été modifié pour assigner la bonne priorité à une tâche, dans le cas de l'utilisation d'Heteroprio


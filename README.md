
# Guide

* Le dossier `results/` contient l'ensemble des mesures de performances effectuées, sous la forme de fichiers CSV. La convention de nommage est expliqué dans le fichier `test-cases.md` détaillant les différents cas de tests.
* Le sous-dossier `results/prios/` contient les résultats des différentes priorités manuelles testées, pour les applications qui le nécessitaient. Les noms des fichiers suivent la même convention de nommage que pour les fichiers de mesures.
* Le sous-dossier `results/heuristics/` contient les mesures, pour l'opération POTRF, mais ne gardant que les résultats pour AutoHeteroprio (sans LA), et en utilisant le nom complet des heuristiques en tant que nom de colonne. Ces fichiers sont utilisés pour la figure de comparaison des heuristiques.

* Le dossier `plot-scripts/` contient l'ensemble des scripts utilisés pour réaliser les Figures du papier de recherche. Le fichier `script-manual.md` détaille leur utilisation.
* Le fichier `figures.md`, quant à lui, répertorie les commandes exactes utilisés pour réaliser chaque Figure insérée dans le papier de recherche.

* Le dossier `bench-scripts/` contient les scripts utilisés pour l'automatisation des mesures de performance et donnés à titre indicatif. Les fichiers batch utilisés pour les lancements sur les différentes configurations sont également fournies.
* Le sous-dossier `bench-scripts/prios/` contient les scripts utilisés pour les mesures de temps réalisés pour tester différentes combinaisons de priorités manuelles.

Enfin, le fichier `modifications.md` détaille, de manière précise les quelques modifications apportées aux applications. Celles-ci sont actives dans les dossiers de build, nommées `build-prio/`, et placés à la racine du répertoire de chaque application.


* The `simulator-code/` contains the code used for the simulations. The code will not compile, as there are dependencies with spetabaru whose code is not fully public yet.

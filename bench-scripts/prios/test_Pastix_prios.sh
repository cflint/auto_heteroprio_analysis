#!/bin/bash -x

# How many times each configuration will be executed
NB_EXECS=5

work_dir=/projets/schnaps/autoprio
jobs_dir=$HOME/jobs
out_dir=$jobs_dir/$SLURM_JOB_ID

# Folder for Heteroprio data files
data_dir=$out_dir/data

# Check and create directories
if [ -d "$out_dir" ] ; then
	echo "folder $out_dir already exists, aborting"
	exit
fi
mkdir -p "$out_dir"
mkdir -p "$data_dir"

# Starpu
starpu_dir=$work_dir/starpu/install

# Job files

# Load modules
module load compiler/cuda
module load compiler/gcc/8.2.0
module load build/cmake
module load linalg/mkl/2020_update4
module load partitioning/scotch/int64/6.0.9
module load hardware/hwloc/2.4.0

cd $work_dir/pastix/build-prio/install
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/projets/schnaps/autoprio/starpu/install/lib

ulimit -c 0

ngpus=2
ncpus=16

export STARPU_HISTORY_MAX_ERROR=9999999
export STARPU_NWORKER_PER_CUDA=16
export STARPU_NCUDA=$ngpus
export STARPU_NCPU=$ncpus
export STARPU_HETEROPRIO_USE_LA=1
export STARPU_SCHED=heteroprio
export STARPU_HETEROPRIO_USE_AUTO_CALIBRATION=0

# Priorities
cblk_getrf1d=1
cblk_gemm=2
blok_getrf=3
blok_trsm=4
blok_gemm=5
solve_trsm=6
solve_gemm=7


# Tests
N=100
job_facto=$out_dir/results-facto.out
job_solve=$out_dir/results-solve.out
touch $job_facto
touch $job_solve

# Version courante
name="manual"
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "$name;$N;" >> $job_facto
    printf "$name;$N;" >> $job_solve
    
    exec=$(numactl -i all examples/simple -i IPARM_MIN_BLOCKSIZE 900 -i IPARM_MAX_BLOCKSIZE 1100 -i IPARM_TASKS2D_WIDTH 900 -g $ngpus -s 3 -9 $N:$N:$N)
    
    echo -e "$exec" | grep "Time to factorize" | tail -n 1 | tr -s ' ' | cut -d' ' -f5 >> $job_facto
    echo -e "$exec" | grep "Time to solve" | tr -s ' ' | cut -d' ' -f5 >> $job_solve
done

# Version avec potientiellement meilleurs slowdown factors
name="better-slows"
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "$name;$N;" >> $job_facto
    printf "$name;$N;" >> $job_solve
    
    exec=$(HP_CPU_PRIOS=$solve_trsm-$solve_gemm-$cblk_getrf1d-$cblk_gemm-$blok_getrf-$blok_trsm-$blok_gemm HP_CUDA_PRIOS=$cblk_gemm-$blok_trsm-$blok_gemm HP_CPU_SLOWS=$cblk_gemm:4.0-$blok_trsm:2.0-$blok_gemm:3.0 numactl -i all examples/simple -i IPARM_MIN_BLOCKSIZE 900 -i IPARM_MAX_BLOCKSIZE 1100 -i IPARM_TASKS2D_WIDTH 900 -g $ngpus -s 3 -9 $N:$N:$N)
    
    echo -e "$exec" | grep "Time to factorize" | tail -n 1 | tr -s ' ' | cut -d' ' -f5 >> $job_facto
    echo -e "$exec" | grep "Time to solve" | tr -s ' ' | cut -d' ' -f5 >> $job_solve
done

# Version avec inversion des groupes d'étapes
name="ginv"
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "$name;$N;" >> $job_facto
    printf "$name;$N;" >> $job_solve
    
    exec=$(HP_CPU_PRIOS=$blok_getrf-$blok_trsm-$blok_gemm-$cblk_getrf1d-$cblk_gemm-$solve_trsm-$solve_gemm HP_CUDA_PRIOS=$blok_trsm-$blok_gemm-$cblk_gemm HP_CPU_SLOWS=$cblk_gemm:1.0-$blok_trsm:10.0-$blok_gemm:10.0 numactl -i all examples/simple -i IPARM_MIN_BLOCKSIZE 900 -i IPARM_MAX_BLOCKSIZE 1100 -i IPARM_TASKS2D_WIDTH 900 -g $ngpus -s 3 -9 $N:$N:$N)
    
    echo -e "$exec" | grep "Time to factorize" | tail -n 1 | tr -s ' ' | cut -d' ' -f5 >> $job_facto
    echo -e "$exec" | grep "Time to solve" | tr -s ' ' | cut -d' ' -f5 >> $job_solve
done

# Version avec slowdown possiblement meilleurs, qui ont ensuite été un peu relevés
name="better-slows-higher"
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "$name;$N;" >> $job_facto
    printf "$name;$N;" >> $job_solve
    
    exec=$(HP_CPU_PRIOS=$solve_trsm-$solve_gemm-$cblk_getrf1d-$cblk_gemm-$blok_getrf-$blok_trsm-$blok_gemm HP_CUDA_PRIOS=$cblk_gemm-$blok_trsm-$blok_gemm HP_CPU_SLOWS=$cblk_gemm:5.0-$blok_trsm:3.0-$blok_gemm:4.0 numactl -i all examples/simple -i IPARM_MIN_BLOCKSIZE 900 -i IPARM_MAX_BLOCKSIZE 1100 -i IPARM_TASKS2D_WIDTH 900 -g $ngpus -s 3 -9 $N:$N:$N)
    
    echo -e "$exec" | grep "Time to factorize" | tail -n 1 | tr -s ' ' | cut -d' ' -f5 >> $job_facto
    echo -e "$exec" | grep "Time to solve" | tr -s ' ' | cut -d' ' -f5 >> $job_solve
done

# Version avec slowdowns assez faibles
name="high-slows"
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "$name;$N;" >> $job_facto
    printf "$name;$N;" >> $job_solve
    
    exec=$(HP_CPU_PRIOS=$blok_getrf-$blok_trsm-$blok_gemm-$cblk_getrf1d-$cblk_gemm-$solve_trsm-$solve_gemm HP_CUDA_PRIOS=$blok_trsm-$blok_gemm-$cblk_gemm HP_CPU_SLOWS=$cblk_gemm:5.0-$blok_trsm:15.0-$blok_gemm:15.0 numactl -i all examples/simple -i IPARM_MIN_BLOCKSIZE 900 -i IPARM_MAX_BLOCKSIZE 1100 -i IPARM_TASKS2D_WIDTH 900 -g $ngpus -s 3 -9 $N:$N:$N)
    
    echo -e "$exec" | grep "Time to factorize" | tail -n 1 | tr -s ' ' | cut -d' ' -f5 >> $job_facto
    echo -e "$exec" | grep "Time to solve" | tr -s ' ' | cut -d' ' -f5 >> $job_solve
done

# Version avec slowdowns plus élevés
name="low-slows"
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "$name;$N;" >> $job_facto
    printf "$name;$N;" >> $job_solve
    
    exec=$(HP_CPU_PRIOS=$blok_getrf-$blok_trsm-$blok_gemm-$cblk_getrf1d-$cblk_gemm-$solve_trsm-$solve_gemm HP_CUDA_PRIOS=$blok_trsm-$blok_gemm-$cblk_gemm HP_CPU_SLOWS=$cblk_gemm:1.0-$blok_trsm:1.0-$blok_gemm:1.5 numactl -i all examples/simple -i IPARM_MIN_BLOCKSIZE 900 -i IPARM_MAX_BLOCKSIZE 1100 -i IPARM_TASKS2D_WIDTH 900 -g $ngpus -s 3 -9 $N:$N:$N)
    
    echo -e "$exec" | grep "Time to factorize" | tail -n 1 | tr -s ' ' | cut -d' ' -f5 >> $job_facto
    echo -e "$exec" | grep "Time to solve" | tr -s ' ' | cut -d' ' -f5 >> $job_solve
done


#!/bin/bash -x

# How many times each configuration will be executed
NB_EXECS=10

work_dir=/projets/schnaps/autoprio
jobs_dir=$HOME/jobs
out_dir=$jobs_dir/$SLURM_JOB_ID

# Folder for Heteroprio data files
data_dir=$out_dir/data

# Check and create directories
if [ -d "$out_dir" ] ; then
	echo "folder $out_dir already exists, aborting"
	exit
fi
mkdir -p "$out_dir"
mkdir -p "$data_dir"

# Starpu
starpu_dir=$work_dir/starpu/install

# Job files

# Load modules
module load compiler/cuda
module load compiler/gcc/8.2.0
module load build/cmake
module load hardware/hwloc/2.4.0

ulimit -c 0

cd $work_dir/chameleon/build-prio/install
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/projets/schnaps/autoprio/starpu/install/lib
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/lpaillat/starpu/starpu-exp/starpu/lib/

ngpus=2
ncpus=16

export STARPU_HISTORY_MAX_ERROR=9999999
export STARPU_NWORKER_PER_CUDA=16
export STARPU_NCUDA=$ngpus
export STARPU_NCPU=$ncpus
export STARPU_HETEROPRIO_USE_LA=1
export STARPU_SCHED=heteroprio
export STARPU_HETEROPRIO_USE_AUTO_CALIBRATION=0

# Priorities
plrnt=1
gemm=2
geqrt=3
lacpy=4
laset=5
tpqrt=6
ormqr=7
tpmqrt=8
plgsy=9
potrf=10
trsm=11
syrk=12

# Gemm
P=gemm
N=40000
job_times=$out_dir/results-time-$P.out
job_flops=$out_dir/results-flops-$P.out
touch $job_times
touch $job_flops

# Version courante
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "manual;$N;" >> $job_times
    printf "manual;$N;" >> $job_flops
    
    exec=$(HP_OPNAME=gemm numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f23 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f24 >> $job_flops
done

# Inverser prio CPU 
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "inv;$N;" >> $job_times
    printf "inv;$N;" >> $job_flops
    
    exec=$(HP_CPU_PRIOS=$gemm-$plrnt HP_CUDA_PRIOS=$gemm HP_CPU_SLOWS=$gemm:29.0 numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f23 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f24 >> $job_flops
done

# Slowdown très bas
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "low-slow;$N;" >> $job_times
    printf "low-slow;$N;" >> $job_flops
    
    exec=$(HP_CPU_PRIOS=$plrnt-$gemm HP_CUDA_PRIOS=$gemm HP_CPU_SLOWS=$gemm:1.0 numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f23 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f24 >> $job_flops
done

# Slowdown très bas
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "high-slow;$N;" >> $job_times
    printf "high-slow;$N;" >> $job_flops
    
    exec=$(HP_CPU_PRIOS=$plrnt-$gemm HP_CUDA_PRIOS=$gemm HP_CPU_SLOWS=$gemm:40.0 numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f23 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f24 >> $job_flops
done


# Potrf
P=potrf
N=50000
job_times=$out_dir/results-time-$P.out
job_flops=$out_dir/results-flops-$P.out
touch $job_times
touch $job_flops

# Version courante
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "manual;$N;" >> $job_times
    printf "manual;$N;" >> $job_flops
    
    exec=$(HP_OPNAME=potrf numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f14 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f15 >> $job_flops
done

# Inverser prio CPU
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "inv;$N;" >> $job_times
    printf "inv;$N;" >> $job_flops
    
    exec=$(HP_CPU_PRIOS=$trsm-$syrk-$gemm-$potrf-$plgsy HP_CUDA_PRIOS=$trsm-$syrk-$gemm HP_CPU_SLOWS=$trsm:11.0-$syrk:26.0-$gemm:29.0 numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f14 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f15 >> $job_flops
done

# Inverser ordre prio GPU
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "inv_gpu;$N;" >> $job_times
    printf "inv_gpu;$N;" >> $job_flops
    
    exec=$(HP_CPU_PRIOS=$potrf-$plgsy-$gemm-$syrk-$trsm HP_CUDA_PRIOS=$gemm-$syrk-$trsm HP_CPU_SLOWS=$trsm:11.0-$syrk:26.0-$gemm:29.0 numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f14 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f15 >> $job_flops
done

# Slowdown très bas
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "low-slow;$N;" >> $job_times
    printf "low-slow;$N;" >> $job_flops
    
    exec=$(HP_CPU_PRIOS=$potrf-$plgsy-$trsm-$syrk-$gemm HP_CUDA_PRIOS=$trsm-$syrk-$gemm HP_CPU_SLOWS=$trsm:2.0-$syrk:2.0-$gemm:4.0 numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f14 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f15 >> $job_flops
done

# Slowdown très haut
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "high-slow;$N;" >> $job_times
    printf "high-slow;$N;" >> $job_flops
    
    exec=$(HP_CPU_PRIOS=$potrf-$plgsy-$trsm-$syrk-$gemm HP_CUDA_PRIOS=$trsm-$syrk-$gemm HP_CPU_SLOWS=$trsm:25.0-$syrk:45.0-$gemm:49.0 numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f14 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f15 >> $job_flops
done


# Geqrf
P=geqrf
N=40000
job_times=$out_dir/results-time-$P.out
job_flops=$out_dir/results-flops-$P.out
touch $job_times
touch $job_flops

# Version courante
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "manual;$N;" >> $job_times
    printf "manual;$N;" >> $job_flops
    
    exec=$(HP_OPNAME=geqrf numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f16 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f17 >> $job_flops
done

# Inverser les prios CPU
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "inv;$N;" >> $job_times
    printf "inv;$N;" >> $job_flops
    
    exec=$(HP_CPU_PRIOS=$ormqr-$tpmqrt-$geqrt-$tpqrt-$plrnt-$lacpy-$laset HP_CUDA_PRIOS=$ormqr-$tpmqrt HP_CPU_SLOWS=$ormqr:10.0-$tpmqrt:10.0 numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f16 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f17 >> $job_flops
done

# Mettre la* au début
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "autre;$N;" >> $job_times
    printf "autre;$N;" >> $job_flops
    
    exec=$(HP_CPU_PRIOS=$lacpy-$laset-$geqrt-$tpqrt-$plrnt-$ormqr-$tpmqrt HP_CUDA_PRIOS=$ormqr-$tpmqrt HP_CPU_SLOWS=$ormqr:10.0-$tpmqrt:10.0 numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f16 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f17 >> $job_flops
done

# Slowfactor faibles
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "low-slow;$N;" >> $job_times
    printf "low-slow;$N;" >> $job_flops
    
    exec=$(HP_CPU_PRIOS=$lacpy-$laset-$geqrt-$tpqrt-$plrnt-$ormqr-$tpmqrt HP_CUDA_PRIOS=$ormqr-$tpmqrt HP_CPU_SLOWS=$ormqr:2.0-$tpmqrt:2.0 numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f16 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f17 >> $job_flops
done

# Slowfactor élevés
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    printf "high-slow;$N;" >> $job_times
    printf "high-slow;$N;" >> $job_flops
    
    exec=$(HP_CPU_PRIOS=$lacpy-$laset-$geqrt-$tpqrt-$plrnt-$ormqr-$tpmqrt HP_CUDA_PRIOS=$ormqr-$tpmqrt HP_CPU_SLOWS=$ormqr:22.0-$tpmqrt:22.0 numactl -i all bin/chameleon_stesting -H -t $ncpus -g $ngpus -b 1600 -o $P -m $N -n $N -k $N)
    
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f16 >> $job_times
    echo -e "$exec" | tail -n 1 | tr -s ' ' | cut -d' ' -f17 >> $job_flops
done


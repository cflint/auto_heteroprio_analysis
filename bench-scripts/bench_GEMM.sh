#!/bin/bash

# How many times each configuration will be executed
NB_EXECS=32
# Schedulers to be compared with Heteroprio variants (names should work with STARPU_SCHED)
SCHEDULERS=(lws eager dm dmda dmdas random)
# Priority ordering policies to be tested
ORD_POLICIES=(13 14 15 23 27 0)
# Whether auto calibration is tested against manual calibration
TEST_MANUAL=1
# Number of calibration rounds, excluded from measurements, for schedulers which needs them
CALIBRATION=1

work_dir=/projets/schnaps/autoprio
jobs_dir=$HOME/jobs
out_dir=$jobs_dir/$SLURM_JOB_ID

# Folder for Heteroprio data files
data_dir=$out_dir/data

# Check and create directories
if [ -d "$out_dir" ] ; then
	echo "folder $out_dir already exists, aborting"
	exit
fi
mkdir -p "$out_dir"
mkdir -p "$data_dir"

# Starpu
starpu_dir=$work_dir/starpu/install

# Job files
job_log=$out_dir/log
job_config=$out_dir/config
job_csv=$out_dir/results.csv
touch $job_log
touch $job_config
touch $job_csv

# Load modules
module load compiler/cuda
module load compiler/gcc/8.2.0
module load build/cmake
module load hardware/hwloc/2.4.0
module load linalg/mkl/2020_update4

ulimit -c 0

# To keep up with program execution (with time indication)
function log
{
    echo "[$(date --rfc-3339=seconds)] $1" >> $job_log
}

# Retrieve configuration of the current node (sets $ncpus, $maxgpu and $nworkerspergpu)
log "Retrieving config"
v100_num=`nvidia-smi -L | grep V100 | wc -l`
p100_num=`nvidia-smi -L | grep P100 | wc -l`
k40m_num=`nvidia-smi -L | grep K40m | wc -l`

max_gpu_type_num=`printf "$v100_num\n$p100_num\n$k40m_num\n" | awk 'n < $0 {n=$0}END{print n}'`

if [ `expr "$v100_num" + "$p100_num" + "$k40m_num"` -ne $max_gpu_type_num ] ; then
	log "gpus are not of same type, aborting"
	exit
fi

arch='unknown'

if [ "$v100_num" -gt 0 ] ; then
	arch='v100'
	maxgpu=2
	nworkerspergpu=16
	ncpus=16
fi
if [ "$p100_num" -gt 0 ] ; then
	arch='p100'
	maxgpu=2
	nworkerspergpu=16
	ncpus=16
fi
if [ "$k40m_num" -gt 0 ] ; then
	arch='k40m'
	maxgpu=4
	nworkerspergpu=7
	ncpus=20
fi

echo 'v100_num='$v100_num >> $job_config
echo 'p100_num='$p100_num >> $job_config
echo 'k40m_num='$k40m_num >> $job_config
#echo 'max_gpu_type_num='$max_gpu_type_num
echo 'arch='$arch >> $job_config
#echo "job_output_directory=$job_output_directory"
echo 'hostname='$HOSTNAME >> $job_config
echo 'job_id='$SLURM_JOB_ID >> $job_config
echo 'node_name='$SLURMD_NODENAME >> $job_config
echo 'lscpu :' >> $job_config
lscpu >> $job_config
echo '#########################################' >> $job_config
echo 'nvidia-smi :' >> $job_config
nvidia-smi >> $job_config

# Separate STARPU home for each experiments to be independant
# also set custom hostname, so experiments can be resume
export STARPU_HOME=$out_dir # StarPU files will be stored into '$out_dir/.starpu/'
export STARPU_HOSTNAME=$arch

# Force StarPU first run configuration, and put it into config file
echo 'starpu_machine_display :' >> $job_config
$starpu_dir/bin/starpu_machine_display >> $job_config

# Prepare for application execution
cd $work_dir/chameleon/build-prio/install
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/projets/schnaps/autoprio/starpu/install/lib

N=40000
prog=gemm
export HP_OPNAME=$prog

# Common environment
export STARPU_HISTORY_MAX_ERROR=999999999
export STARPU_NCUDA=$maxgpu
export STARPU_NCPU=$ncpus
export STARPU_NWORKER_PER_CUDA=$nworkerspergpu

# Warm up by launching a distinct execution for nothing
log "Warming up"
STARPU_SCHED=heteroprio \
STARPU_HETEROPRIO_DATA_FILE=$data_dir/warm-up.data \
numactl -i all bin/chameleon_stesting -H -t $ncpus -g $maxgpu -b 1600 -o $prog -m $N -n $N -k $N

log "Starting tests"
for (( n=0 ; n <= NB_EXECS ; n++ ))
do
    log "Test round $n"
    
    for (( sched_ind=0 ; sched_ind <= ${#SCHEDULERS[@]} ; sched_ind++ ))
    do
        if [ "$sched_ind" == "0" ]
        then
            scheduler=heteroprio
            use_heteroprio=1
        else
            scheduler=${SCHEDULERS[$(($sched_ind-1))]}
            use_heteroprio=0
        fi
        
        # Iterate over Heteroprio parameters (auto calibration, locality aware, ordering policies)
        for (( use_auto=0 ; use_auto <= TEST_MANUAL*use_heteroprio ; use_auto++ )) ; do
        for (( use_la=0 ; use_la <= use_heteroprio ; use_la++ )) ; do
        for (( ord_policy_ind=0 ; ord_policy_ind <= use_heteroprio*use_auto*(${#ORD_POLICIES[@]}-1) ; ord_policy_ind++))
        do
            ord_policy=${ORD_POLICIES[$ord_policy_ind]}
            
            # Creating a codename for the execution parameters (and associated data file if heteroprio)
            if [ "$sched_ind" == "0" ]
            then
                name=""
                if [ "$use_la" == "1" ] ; then name=la ; fi
                if [ "$use_auto" == "1" ] ; then name="$name"a ; fi
                name="$name"hp
                if [ "$use_auto" == "1" ] ; then name="$name"_$ord_policy ; fi
                
                heteroprio_file=$data_dir/$name.data
            else
                name=$scheduler
                heteroprio_file=no-file
            fi
            
            if [ "$n" == "0" ]
            then
                printf "$name" >> $job_csv

                # If calibration enabled, run some executions for calibration
                if [ "$CALIBRATION" -gt "0" ] ; then
                    if [ "$use_auto" == "1" ] || [ "$scheduler" == "dm" ] || [ "$scheduler" == "dmda" ] || [ "$scheduler" == "dmdas" ] ; then
                        for (( nc=0 ; nc < CALIBRATION ; nc++ ))
                        do
                            echo "--- Calibration $name ---"
                            STARPU_CALIBRATE=1 \
			    STARPU_SCHED=$scheduler \
			    STARPU_HETEROPRIO_DATA_FILE=$heteroprio_file \
			    STARPU_HETEROPRIO_USE_AUTO_CALIBRATION=$use_auto \
		            STARPU_HETEROPRIO_USE_LA=$use_la \
			    STARPU_AUTOHETEROPRIO_PRIORITY_ORDERING_POLICY=$ord_policy \
                            numactl -i all bin/chameleon_stesting -H -t $ncpus -g $maxgpu -b 1600 -o $prog -m $N -n $N -k $N
                        done
                    fi
                fi
            else
                # For better output
                echo "--- $name No. $n ---"
                
                printf "$(STARPU_SCHED=$scheduler \
                    STARPU_HETEROPRIO_DATA_FILE=$heteroprio_file\
                    STARPU_HETEROPRIO_USE_AUTO_CALIBRATION=$use_auto \
                    STARPU_HETEROPRIO_USE_LA=$use_la \
                    STARPU_AUTOHETEROPRIO_PRIORITY_ORDERING_POLICY=$ord_policy \
                    numactl -i all bin/chameleon_stesting -H -t $ncpus -g $maxgpu -b 1600 -o $prog -m $N -n $N -k $N | tail -n 1 | tr -s ' ' | cut -d' ' -f23)" >> $job_csv
            fi
            
            if [ "$sched_ind" -lt "${#SCHEDULERS[@]}" ]
            then
                printf ";" >> $job_csv
            fi      
        done   
        done
        done
    done
    
    echo "" >> $job_csv
done

log "End of tests"


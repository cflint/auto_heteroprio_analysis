# !/bin/bash

ARCHS=(v100 p100 k40m)

function config_args()
{
    args=""
    for conf in "${ARCHS[@]}"; do
        args=$args"$1-$conf.csv $conf "
    done
    printf "$args"
}

function config_title()
{
    title=$1
    shift 1
    while [ "$#" -gt "0" ]; do
        title=$title-$1
        shift 1
    done
    printf "$title"
}

function multiple_config_args()
{
    args=""
    for app in $@; do
        for conf in "${ARCHS[@]}"; do
            args=$args"$app-$conf.csv $app-$conf "
        done
    done
    printf "$args"
}

if [ $# -le 0 ]; then
    echo "Err: Expected at least one argument"
    exit 1
fi

if [ $# -eq 1 ]; then
    python speedup_plot.py "" $(config_args $1)
elif [ $# -gt 1 ]; then
    python speedup_plot.py "" $(multiple_config_args $@)
fi


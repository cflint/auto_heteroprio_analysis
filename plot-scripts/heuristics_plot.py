"""
This script is based on the following demos:
- 'stacked bar chart': https://matplotlib.org/stable/gallery/lines_bars_and_markers/bar_stacked.html
- 'grouped bar chart': https://matplotlib.org/stable/gallery/lines_bars_and_markers/barchart.html
"""

import sys

import pandas
import matplotlib.pyplot as plt
import numpy as np

# Config values
conf_heuristics_col_name_list = [ 'ahp_13', 'ahp_14', 'ahp_15', 'ahp_23', 'ahp_27', 'ahp_0' ]
conf_csv_separator = ';'
conf_csv_decimal_separator = '.'

class conf_data:
    def __init__(self):
        self.avg = 0.0
        self.avg_count = 0
        self.hseries = {}
        for col in conf_heuristics_col_name_list:
            self.hseries[col] = pandas.Series(dtype=float)
    
    def add_series(self, series, col_name):
        """
        Add data series for a given column name and update global average
        series: <pandas.Series>
        col_name: column name of the data series
        """
        self.avg = (self.avg * self.avg_count + series.count() * series.mean()) / (self.avg_count + series.count())
        self.avg_count += series.count()
        self.hseries[col_name] = self.hseries[col_name].append(series)
        
    def get_series_relative_diff(self):
        """
        Compute each series relative difference to the average
        """
        res = []
        for name in self.hseries:
            res += [self.hseries[name].mean() / self.avg]
        return res
        
    def get_series_relative_error(self):
        """
        Compute each series min and max error, relative to average
        """
        min_err = []
        max_err = []
        for name in self.hseries:
            min_err += [self.hseries[name].min() / self.avg]
            max_err += [self.hseries[name].max() / self.avg]
        return [min_err, max_err]

# Global values: stores the plot data
plot_n = 0
plot_labels = [ 'Offset model', 'Softplus model', 'Interpolation model', 'PURWS', 'PRWS', 'NTC' ]
plot_k40m_data = conf_data()
plot_p100_data = conf_data()
plot_v100_data = conf_data()

def repeat(a, n):
    """
    Returns 'a' repeated 'n' times into a list
    """
    l = []
    for i in range(n):
        l += [a]
    return l

def extract_conf_name(s):
    known_confs = [ 'p100', 'v100', 'k40m' ]
    
    for conf in known_confs:
        if conf in s:
            return conf
    return None
        

if __name__ == '__main__':
    if len(sys.argv) <= 2:
        print("Usage: python %s [title] [csv_1] ... [csv_n]" % sys.argv[0])
        sys.exit(1)
        
    for i in range(2, len(sys.argv)):
        csv_file = sys.argv[i]
        
        conf_name = extract_conf_name(csv_file)
        if conf_name == None:
            print("Unknown configuration name for %s, Ignoring this file" % csv_file)
            continue
        
        print("Reading %s measures as conf %s ..." % (csv_file, conf_name))
        data = pandas.read_csv(csv_file, sep=conf_csv_separator, na_values=conf_csv_decimal_separator)
        
        for col_name in conf_heuristics_col_name_list:
            if conf_name == 'k40m':
                plot_k40m_data.add_series(data[col_name], col_name)
            elif conf_name == 'p100':
                plot_p100_data.add_series(data[col_name], col_name)
            elif conf_name == 'v100':
                plot_v100_data.add_series(data[col_name], col_name)
                
    print("v100 average is %f" % plot_v100_data.avg)
    for col_name in conf_heuristics_col_name_list:
        print("\taverage for %s is %f" % (col_name, plot_v100_data.hseries[col_name].mean()))

    print("Making plot ...")
    
    colors = ['#F5793A', '#A95AA1', '#0F2080']
    
    x = np.arange(len(plot_labels))
    width = 0.25

    fig, ax = plt.subplots(figsize=(12, 8))
    
    ax.bar(x - width, plot_v100_data.get_series_relative_diff(), width, color=colors[0], label='v100')
    ax.bar(x, plot_p100_data.get_series_relative_diff(), width, color=colors[1], label='p100')
    ax.bar(x + width, plot_k40m_data.get_series_relative_diff(), width, color=colors[2], label='k40m')
    
    ax.set_ylabel('Makespan (relative, average)')
    ax.set_title(sys.argv[1])
    ax.set_xticks(x)
    ax.set_xticklabels(plot_labels)
    ax.set_ylim(ymax=1.3)
    
    ax.legend()

    fig.tight_layout()
    plt.show()


import sys

import pandas
import matplotlib.pyplot as plt

# Config values
conf_csv_separator = ';'
conf_csv_decimal_separator = '.'
conf_colors = {'v100': ('red', 'yellow'), 'p100': ('darkgreen', 'darkviolet'), 'k40m': ('dodgerblue', 'forestgreen')}
conf_labels = {'hp': 'Heteroprio', 'lahp': 'LaHeteroprio', 'ahp': 'AutoHeteroprio', 'laahp': 'La+AutoHeteroprio', 'lws': 'Lws', 'eager': 'Eager', 'dm': 'DM', 'dmda': 'DMDA', 'dmdas': 'DMDAS', 'random': 'Random'}

def change_boxplot_colors(boxplot, edge_color, median_color):
    """
    Change the colors of a drawn boxplot
    boxplot: <dict<Line2D>> the boxplot returned by Axes.boxplot
    edge_color: <str> wanted color for the boxplot edges
    median_color: <str> wanted color for the median line of the boxplot
    Inspired from https://stackoverflow.com/questions/41997493/python-matplotlib-boxplot-color
    """
    for elt in ('boxes', 'whiskers', 'fliers', 'means', 'caps'):
        plt.setp(boxplot[elt], color=edge_color)
    plt.setp(boxplot['medians'], color=median_color)
    
def extract_conf_name(s):
    known_confs = [ 'p100', 'v100', 'k40m' ]
    
    for conf in known_confs:
        if conf in s:
            return conf
    return None
    
def search_prefixed_cols(data, prefix):
    """
    Create a list of all DataFrame columns which names matches a given prefix
    data: <pandas.DataFrame>
    prefix: <str>
    """
    l = []
    for col in data.columns:
        if col.startswith(prefix):
            l += [col]
    return l
            
def merge_series_from_data(data, cols_names):
    """
    Merge wanted columns (given names) into a single pandas.Series
    data: <pandas.DataFrame>
    cols_names: <list<str>>
    """
    series = pandas.Series(dtype=float)
    for name in cols_names:
        series = series.append(data[name])
    return series

# Data associated to a group of results (an experiment with multiple boxplots)
class results_group:
    def __init__(self, data, fields, config):
        """
        data: <pandas.DataFrame> measures from the CSV file
        fields: <array<str>> prefix of choosen configurations
        config: <str> name of the running configuration (usually extracted from CSV name)
        """
        self.conf_name = config
        self.count = len(fields)
        self.series = {}
        
        self.labels = []
        # Set reader friendly labels
        for field in fields:
            self.labels += [conf_labels[field]]
        
        for prefix in fields:
            columns = search_prefixed_cols(data, prefix + '_')
            if len(columns) > 0: # Is a prefix and not juste a name
                self.series[prefix] = merge_series_from_data(data, columns)
                print("  Found %s columns for %s: %s" % (str(len(columns)), prefix, str(columns)))
            else: # Prefix is just the name of one column
                self.series[prefix] = data[prefix]
                
            self.series[prefix].name = prefix
            
        self.data = pandas.DataFrame(dtype=float)
        for key in self.series:
            self.data = self.data.join(self.series[key], how='outer')
        
    def draw_boxes(self, ax, start_pos):
        """
        Draw boxplots from this group data
        ax: <Axes> where to draw plots
        start_pos: <int> starting index to draw boxes
        colors: <array<str>> boxes colors [0] is edge color and [1] is median color
        """
        boxes = ax.boxplot(self.data, positions=range(start_pos, start_pos + self.count), labels=self.labels, widths=0.5)
        change_boxplot_colors(boxes, conf_colors[self.conf_name][0], conf_colors[self.conf_name][1])

if __name__ == '__main__':
    is_argc_even = len(sys.argv) % 2 == 0
    
    if len(sys.argv) <= 2:
        print("Usage: python %s {title} [csv_1] [prefix_1|...|prefix_n] ... [csv_n] [prefix_1|...|prefix_n]" % sys.argv[0])
        sys.exit(1)
        
    title = 'title'
    arg_list_start = 1
    if is_argc_even:
        title = sys.argv[1]
        arg_list_start = 2
        
    results_groups = []
        
    for i in range(arg_list_start, len(sys.argv), 2):
        csv_file = sys.argv[i]
        prefixes = sys.argv[i+1]
        
        print("Reading %s ..." % csv_file)
        data = pandas.read_csv(csv_file, sep=conf_csv_separator, na_values=conf_csv_decimal_separator)
        prefix_list = prefixes.split('|')
        
        results_groups += [ results_group(data, prefix_list, extract_conf_name(csv_file)) ]
        
    print("Making plot ...")
    
    fig, axs = plt.subplots(1, len(results_groups))
    
    if len(results_groups) == 1:
        axs.set_ylabel("Makespan (seconds)")
            
        axs.set_title(results_groups[0].conf_name)
        axs.set_xticklabels(results_groups[0].labels, rotation=45, rotation_mode='anchor', ha='right')
    
        results_groups[0].draw_boxes(axs, 1)
    else:
        position = 1
        for (ax, group) in zip(axs, results_groups):
            if position == 1:
                ax.set_ylabel("Makespan (seconds)")
		    
            ax.set_title(group.conf_name)
            ax.set_xticklabels(group.labels, rotation=45, rotation_mode='anchor', ha='right')
	    
            group.draw_boxes(ax, position)
            position += group.count
    
    fig.tight_layout()
    plt.subplots_adjust(left=0.1, bottom=0.25, right=0.98, top=0.9, wspace=0.2, hspace=0.2)
    plt.show()


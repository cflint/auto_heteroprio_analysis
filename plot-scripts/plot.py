import sys

import pandas
import matplotlib.pyplot as plt

if len(sys.argv) < 2:
    print("Usage: python plot.py [data.csv] {title}")
    sys.exit(1) 

data = pandas.read_csv(sys.argv[1], sep=';', na_values='.')

data.boxplot()

if len(sys.argv) >= 3:
    plt.title(sys.argv[2])
else:
    plt.title(sys.argv[1])
    
plt.ylabel("Makespan (seconds)")
    
#plt.subplots_adjust(left=0.04, bottom=0.1, right=0.98, top=0.9, wspace=0.2, hspace=0.2)
plt.tight_layout()
plt.show()


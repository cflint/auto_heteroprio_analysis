"""
This script is based on the following demos:
- 'stacked bar chart': https://matplotlib.org/stable/gallery/lines_bars_and_markers/bar_stacked.html
- 'grouped bar chart': https://matplotlib.org/stable/gallery/lines_bars_and_markers/barchart.html

Dark colored bar: heteroprio average against average of all heuristics
Light colored bar: heteroprio average against best heuristic average
Dot / line: heteroprio best time against autoheteroprio best time
"""

import sys

import pandas
import matplotlib.pyplot as plt
from matplotlib.ticker import StrMethodFormatter
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
import numpy as np

# Config values
conf_manual_name = 'hp'
conf_lamanual_name = 'lahp'
conf_auto_prefix = 'ahp'
conf_laauto_prefix = 'laahp'
conf_csv_separator = ';'
conf_csv_decimal_separator = '.'

width = 0.25

# Data associated to one bar for each group
class bar_data:
    def __init__(self, name):
        self.name = name
        self.values = []
        self.light_pos = []
        self.dark_pos = []
        self.lows = []
        self.meds = []
        self.highs = []
        self.heights = []
        self.light_bottoms = []
        self.dark_bottoms = []
        
    def add_measure(self, high, med, low, values): # high is light colored
        self.values += [values]
        self.lows += [low]
        self.meds += [med]
        self.highs += [high]
        self.heights += [(high-low)]
        self.dark_pos += [1]
        #if (high-1) * (low-1) < 0:
        #    self.light_pos += [high-1]
        #    self.dark_pos += [low-1]
        #    
        #    self.light_bottoms += [0]
        #    self.dark_bottoms += [0]
        #elif high-1 < 0: # both negatives -> high is the 'bottom' of low
        #    self.light_pos += [high-1]
        #    self.dark_pos += [low-high]
        #    
        #    self.light_bottoms += [0]
        #    self.dark_bottoms += [high-1]
        #else:
        #    self.light_pos += [high-low]
        #    self.dark_pos += [low-1]
        #    
        #    self.light_bottoms += [low-1]
        #    self.dark_bottoms += [0]

# Global values: stores the plot data
plot_n = 0
plot_labels = []
plot_la_bars = bar_data('LaHeteroprio')
plot_auto_bars = bar_data('AutoHeteroprio')
plot_laauto_bars = bar_data('La+AutoHeteroprio')

def repeat(a, n):
    """
    Returns 'a' repeated 'n' times into a list
    """
    l = []
    for i in range(n):
        l += [a]
    return l

def series_stats(series):
    """
    Compute wanted stats of a series (min, avg)
    series: <pandas.Series>
    """
    #return (series.min(), series.median(), series.max())
    return (series.quantile(q=0.05), series.quantile(q=0.5), series.quantile(q=0.95))
    
def search_prefixed_cols(data, prefix):
    """
    Create a list of all DataFrame columns which names matches a given prefix
    data: <pandas.DataFrame>
    prefix: <str>
    """
    l = []
    for col in data.columns:
        if col.startswith(prefix):
            l += [col]
    return l

def best_heuristic_from_data(data, cols_names):
    """
    Get column name corresponding to best heuristics (best is the one with best average (smallest average))
    data: <pandas.DataFrame>
    cols_names: <list<str>>
    """
    best = cols_names[0]
    bestv = data[best].mean()
    for name in cols_names:
        avg = data[name].mean()
        if avg < bestv:
            best = name
            bestv = avg
    return best
            
def merge_series_from_data(data, cols_names):
    """
    Merge wanted columns (given names) into a single pandas.Series
    data: <pandas.DataFrame>
    cols_names: <list<str>>
    """
    series = pandas.Series(dtype=float)
    for name in cols_names:
        series = series.append(data[name])
    return series

def add_case(data, label):
    """
    data: <pandas.DataFrame> corresponding to CSV measures
    label: name of the measurements test case
    """
    global plot_n
    global plot_labels
    global plot_la_bars
    global plot_auto_bars
    global plot_laauto_bars
    
    plot_n += 1
    plot_labels += [label]
    
    manual_series = data[conf_manual_name]
    # warning ! avg here refers to the median value (not the average)
    m_stats = (m_min, m_avg, m_max) = series_stats(manual_series)
    print("Heteroprio stats:       min=%f avg=%f max=%f" % m_stats)
    
    la_series = data[conf_lamanual_name]
    la_stats = (la_min, la_avg, la_max) = series_stats(la_series)
    print("LaHeteroprio stats:     min=%f avg=%f max=%f" % la_stats)
    print("LaHeteroprio stats (speedups):     min=%f avg=%f max=%f" % (m_avg/la_max, m_avg/la_avg, m_avg/la_min))
    plot_la_bars.add_measure(m_avg/la_max, m_avg/la_avg, m_avg/la_min, m_avg/la_series)
    
    a_list = search_prefixed_cols(data, conf_auto_prefix)
    a_best = best_heuristic_from_data(data, a_list)
    a_series = merge_series_from_data(data, a_list)
    (a_min, a_avg, a_max) = series_stats(a_series)
    (a_best_min, a_best_avg, a_best_max) = series_stats(data[a_best])
    print("AutoHeteroprio stats:   min=%f avg=%f max=%f" % (a_min, a_avg, a_max))
    plot_auto_bars.add_measure(m_avg/a_max, m_avg/a_avg, m_avg/a_min, m_avg/a_series)
    
    laa_list = search_prefixed_cols(data, conf_laauto_prefix)
    laa_best = best_heuristic_from_data(data, laa_list)
    laa_series = merge_series_from_data(data, laa_list)
    (laa_min, laa_avg, laa_max) = series_stats(laa_series)
    (laa_best_min, laa_best_avg, laa_best_max) = series_stats(data[laa_best])
    print("LaAutoHeteroprio stats: min=%f avg=%f max=%f" % (laa_min, laa_avg, laa_max))
    plot_laauto_bars.add_measure(m_avg/laa_max, m_avg/laa_avg, m_avg/laa_min, m_avg/laa_series)
    
    print("AutoHeteroprio found columns:    %s" % str(a_list))
    print("LaAutoHeteroprio found columns:  %s" % str(laa_list))
    print("AutoHeteroprio best heuristic:   %s" % str(a_best))
    print("LaAutoHeteroprio best heuristic: %s" % str(laa_best))
    print("------")

# colors is a 2-tuples because one color used to represent the confidence interval
def add_bars(ax, bars, pos, w, med_widths, colors):
    """
    Add bars to plot using ax.bar()
    ax: ax from mathplotlib
    bars: <bar_data>
    pos: position of the bar in each group
    w: bars width
    med_widths: size of the medians bars
    colors: tuple: first is min color ans second is max color
    """

    values = np.array(bars.values)
    print(values)
    i=0
    for bar in bars.lows:
        box = ax.boxplot(values[i], whis='range', patch_artist=True, showfliers=False, positions=[pos[i]], widths=width, zorder=1)
        for b in box['boxes']:
            # change outline color
            b.set(color='black', linewidth=1)
            # change fill color
            b.set(facecolor = confidence_interval_color)
            #b.set(median = 'black')
            # change hatch
            #b.set(hatch = '/')
        #for _, line_list in box.items():
        #    for line in line_list:
        #        line.set_color('black')
        i+=1
    
    ax.bar(pos, bars.meds, w, color=colors[0], bottom=0, label=bars.name, edgecolor='black')
    #ax.bar(pos, bars.heights, w, color=colors[1], bottom=bars.lows, edgecolor='black', hatch='///')

    ax.plot(pos, bars.meds, color='black', linewidth=0, linestyle='-', marker='o', zorder=10)
    
    # Draw lines
    # draw middles
    start = pos[0]
    for value in bars.meds:
        ax.plot([start-width/2, start+width/2], [value, value], color='black', linewidth=1, linestyle='-', marker='', zorder=10)
        start += 1
    # draw lows
#    start = pos[0]
#    for value in bars.lows:
#        ax.plot([start-width/4, start, start+width/4], [value, value+0.002, value], color='black', linewidth=1, linestyle='-', marker='')
#        start += 1
    # draw highs
#    start = pos[0]
#    for value in bars.highs:
#        ax.plot([start-width/4, start, start+width/4], [value, value-0.002, value], color='black', linewidth=1, linestyle='-', marker='')
#        start += 1
    #ax.plot(pos, bars.meds, color='black', linewidth=1, linestyle='--', marker='')
    #ax.bar(pos, med_widths, w, color='black', bottom=bars.meds)

if __name__ == '__main__':
    is_argc_even = len(sys.argv) % 2 == 0
    
    if len(sys.argv) <= 2:
        print("Usage: python %s {title} [csv_1] [name_1] ... [csv_n] [name_n]" % sys.argv[0])
        sys.exit(1)
        
    title = 'title'
    arg_list_start = 1
    if is_argc_even:
        title = sys.argv[1]
        arg_list_start = 2
        
    for i in range(arg_list_start, len(sys.argv), 2):
        csv_file = sys.argv[i]
        case_name = sys.argv[i+1]
        print("Adding %s measures as \"%s\" ..." % (csv_file, case_name))
        
        data = pandas.read_csv(csv_file, sep=conf_csv_separator, na_values=conf_csv_decimal_separator)
        
        add_case(data, case_name)

    print("Making plot ...")

    med_widths = repeat(0.001, plot_n)
    
    x = np.arange(len(plot_labels))

    fig, ax = plt.subplots(figsize=(12, 9))

# colorblind-friendly palette according to https://venngage.com/blog/color-blind-friendly-palette/
    colors = ['#F5793A', '#A95AA1', '#0F2080']
    scheduler_names = ['LaHeteroprio', 'AutoHeteroprio', 'La+AutoHeteroprio']
    confidence_interval_color = '#85C0F9'
    
    add_bars(ax, plot_la_bars, x - width, width, med_widths, (colors[0], colors[0]))
    add_bars(ax, plot_auto_bars, x, width, med_widths, (colors[1], colors[1]))
    add_bars(ax, plot_laauto_bars, x + width, width, med_widths, (colors[2], colors[2]))
    
    ax.set_ylabel('Speedup against Manual Heteroprio')
    ax.set_xlabel('Architecture / Test case')
    ax.set_title(title)
    ax.set_xticks(x)
    ax.set_xticklabels(plot_labels)
    ax.set_ylim(ymin=0)
    ax.axhline(y=0, color='k', linewidth=1)
    #plt.yscale('log')
    
#    ytickslbl = []
#    for t in ax.get_yticks():
#        ytickslbl += ["%.2f" % (t)]
#    ax.set_yticklabels(ytickslbl)
    
    legend_elts = [ Patch(facecolor=colors[0], label=scheduler_names[0]),
                    Patch(facecolor=colors[1], label=scheduler_names[1]),
                    Patch(facecolor=colors[2], label=scheduler_names[2]),
                    Patch(facecolor='white', label=''), # Separation
                    Patch(facecolor='None', edgecolor='black', color=confidence_interval_color, label='Confidence intervals'),
                    Line2D([0], [0], color='black', linewidth=1, linestyle='-', marker='o', label='Median') ]
    
    ax.legend(handles=legend_elts)

    fig.tight_layout()
    plt.show()


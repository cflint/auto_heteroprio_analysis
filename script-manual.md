
# Guide des script générant des graphiques

## Configuration

La plupart des script commencent par une partie permettant de configurer certains paramètres de celui-ci. Ces paramètres vont du format du fichier CSV (utilisation de ',' ou de ';' pour séparer les valeurs), à la modification des couleurs utilisées.

## plot.py

Le script `plot.py` génère simplement des diagrammes en boite, à partir d'un seul fichier CSV. On peut optionnellement ajouté un titre au graphique généré.

```
python plot.py [fichier csv] {titre}
```

## speedup_plot.py

Le script `speedup-plot.py` génère des diagrammes en barres, dans le bût de montrer l'accélération apporter par les différentes améliorations d'Heteroprio. Chaque fichier CSV est donné avec un nom qui sera affiché en dessous du groupe de barres représentant l'accélération de le cas de ces mesures. Un titre optionnel peut également être indiqué.

Pour choisir les bonnes colonnes du fichier CSV, de même que pour grouper les valeurs de plusieurs heuristiques, le script se base sur les noms de colonnes données dans sa configuration (en haut du fichier), et les traite comme des préfixes, c'est à dire que toutes les colonnes commencent par ce nom seront regroupés.

```
python speedup_plot.py {titre} [fichier csv_1] [nom associé csv_1] ... [fichier csv_n] [nom associé csv_n]
```

## speedup_plot.sh

Le script `speedup_plot.sh` est un utilitaire basé sur `speedup_plot.py` et prend en argument un ou plusieurs noms de cas de tests, puis génère le speedup plot pour toutes les configurations de ce cas de tests.

Par exemple,
```
./speedup_plot.sh GEMM POTRF
```
équivaut à
```
python speedup_plot.py GEMM-k40m.csv GEMM-k40m GEMM-p100.csv GEMM-p100 GEMM-v100.csv GEMM-v100 POTRF-k40m.csv POTRF-k40m POTRF-p100.csv POTRF-p100 POTRF-v100.csv POTRF-v100
```

## heuristics_plot.py

Le script `heuristics_plot.py` permet de visualiser la différence moyenne relative, entre différentes heuristiques et pour différentes configurations, sous formes de barres.

Ce script prend en arguments les fichiers CSV associées à l'application voulue, puis récupère les mesures pour les heuristiques en se basant sur une liste de colonnes présente dans la configuration. La configuration associé au fichier de mesures (le modèle de carte graphique) est lui déduit du nom du fichier.

```
python heuristics_plot.py [titre] [fichier csv 1] ... [fichier csv n]
```

## heuristics_plot_modified.py

Le script `heuristics_plot_modified.py` est identique au script `heuristics_plot.py`, à la différence prêt que ce script utilise les nom complets des heuristiques dans sa liste hardcodée, alors que la version précedente, elle se base sur les noms de code, comprenant le numéro de l'heuristique.

## mult_box_plot_time.py

Le script `mult_box_plot_time.py` permet de générer des diagrammes en boîte séparés mais présentés sur la même figure, pour des mesures provenant de différents fichiers csv. Le script permet également de choisir les schedulers dont on veut afficher les mesures, de même si le nom d'un de ces schedulers correspond au préfixe de plusieurs colonnes, les résultats de ces différentes colonnes seront automatiquement réunies. 

Enfin, la configuration est déduite automatiquement du nom du fichier, est ensuite indiquée au dessus du diagramme et détermine la couleur des boîtes du diagramme.

```
python mult_box_plot_time.py {titre} [csv 1] "scheduler_1|...|scheduler_n" ... [csv n] "scheduler_1|...|scheduler_n"
```


# Convention de nommage des fichiers CSV

Les noms des fichiers CSV, stockés dans `results/` prennent la forme suivante `name-config.csv`, où `name` designe le nom de l'opération testée et config, la configuration sur laquelle le test a été réalisé (`k40`, `p100` ou `v100`).

# Noms et détails des opérations testées

* `FMM-base` désigne le lancement de ScalFMM, sans modifier certains paramètres. L'éxecution correspondante est `Tests/Release/testBlockedRotationCuda -N 10000000 -no-validation`.
* `FMM-2` désigne la seconde éxecution ScalFMM, modifiant davantage de paramètres. L'éxecution correspondante est `Tests/Release/testBlockedRotationCuda -N 60000000 -h 7 -bs 2000 -no-validation`.
* `QRM` désigne l'application QrMUMPS, lancé sur la matrice TF16. L'éxecution correspondante est `./examples/dqrm_test < test-cases/input.txt`.
* `GEMM` désigne l'opération Multiplication de matrice, de la librairie Chameleon. L'éxecution correspondante est `bin/chameleon_stesting -H -t $ncpus -g $maxgpu -b 1600 -o gemm -m 40000 -n 40000 -k 40000`.
* `POTRF` désigne l'operation Factorisation de Cholesky, de la librairie Chameleon. L'éxecution correspondante est `bin/chameleon_stesting -H -t $ncpus -g $maxgpu -b 1600 -o potrf -m 50000 -n 50000 -k 50000`.
* `GEQRF` désigne l'operation Factorisation QR, de la librairie Chameleon. L'éxecution correspondante est `bin/chameleon_stesting -H -t $ncpus -g $maxgpu -b 1600 -o geqrf -m 40000 -n 40000 -k 40000`.
* `Pastix` désigne l'application `simple` de Pastix. Elle comporte 2 étapes, séparés dans des fichiers différents. `Pastix-facto` désigne ainsi la factorisation LU et `Pastix-solve` l'étape de résolution. L'éxecution correspondante est `examples/simple -i IPARM_MIN_BLOCKSIZE 900 -i IPARM_MAX_BLOCKSIZE 1100 -i IPARM_TASKS2D_WIDTH 900 -g $maxgpu -s 3 -9 100:100:100`.

## Priorités manuelles

Pour utiliser les priorités manuelles sur Chameleon, il faut également indiquer l'opération avec une variable d'environment, pour que l'ordre des priorités soit celui correspondant.

* Pour GEMM: `HP_OPNAME=gemm bin/chameleon_stesting -H -t $ncpus -g $maxgpu -b 1600 -o gemm -m 40000 -n 40000 -k 40000`
* Pour POTRF: `HP_OPNAME=potrf bin/chameleon_stesting -H -t $ncpus -g $maxgpu -b 1600 -o potrf -m 50000 -n 50000 -k 50000`
* Pour GEQRF: `HP_OPNAME=geqrf bin/chameleon_stesting -H -t $ncpus -g $maxgpu -b 1600 -o potrf -m 50000 -n 50000 -k 50000`


# Entrées et scripts utilisés pour les Figures du papier de recherche

## Comparaison entre priorités manuelles et automatiques

* **scalfmm1_prio_comparison.png** -> `./speedup_plot.sh ../results/FMM-2`
* **scalfmm2_prio_comparison.png** -> `./speedup_plot.sh ../results/FMM-base`
* **chameleon_prio_comparison.png** -> `./speedup_plot.sh ../results/GEMM ../results/POTRF ../results/GEQRF`
* **qrmumps_prio_comparison.png** -> `./speedup_plot.sh ../results/QRM`
* **pastix_factorization_prio_comparison.png** -> `./speedup_plot.sh ../results/Pastix-facto`

## Cas particulier Pastix - étape de résolution

* **pastix_solve_times.png** -> `python plot-scripts/mult_box_plot_time.py results/Pastix-solve-p100.csv "hp|lahp|ahp|laahp|dm|dmda|dmdas|lws|eager"`

## Comparaison avec différents schedulers

* **qrmumps_times_all_conf.png** -> `all="hp|ahp|dm|dmda|lws|eager"; python plot-scripts/mult_box_plot_time.py results/QRM-p100.csv "$all" results/QRM-v100.csv "$all" results/QRM-k40m.csv "$all"`
* **gemm_p100_k40m_times.png** -> `all="lahp|laahp|dm|dmda|dmdas|lws|eager|random"; python plot-scripts/mult_box_plot_time.py results/GEMM-p100.csv $all results/GEMM-k40m.csv $all`
* **potrf_p100_k40m_times.png** -> `all="lahp|laahp|dm|dmda|dmdas|lws|eager|random"; python plot-scripts/mult_box_plot_time.py results/POTRF-p100.csv "$all" results/POTRF-k40m.csv "$all"`
* **geqrf_p100_k40m_times.png** -> `all="lahp|laahp|dm|dmda|dmdas|lws|eager|random"; python plot-scripts/mult_box_plot_time.py results/GEQRF-k40m.csv $all results/GEQRF-p100.csv $all`
* **pastix_factorization_times_all_conf.png** -> `all="hp|lahp|ahp|laahp|dm|dmda|dmdas|lws|eager"; python plot-scripts/mult_box_plot_time.py results/Pastix-facto-k40m.csv $all results/Pastix-facto-p100.csv $all results/Pastix-facto-v100.csv $all`
* **scalfmm1_times_all_conf.png** -> `all="hp|ahp|dm|dmda|lws|eager|random"; python plot-scripts/mult_box_plot_time_log.py results/FMM-2-k40m.csv $all results/FMM-2-p100.csv $all results/FMM-2-v100.csv $all`
* **scalfmm2_times_all_conf.png** -> `all="hp|ahp|dm|dmda|lws|eager|random"; python plot-scripts/mult_box_plot_time_log.py results/FMM-base-k40m.csv $all results/FMM-base-p100.csv $all results/FMM-base-v100.csv $all`

## Comparaison des heuristiques

* **potrf_heuristics_comp.png** -> `python plot-scripts/heuristics_plot_modified.py "" results/heuristics/POTRF*`

#include <vector>
#include <string>
#include <queue>
#include <iostream>
#include <algorithm>

/*#include "Utils/SpModes.hpp"
#include "Utils/SpUtils.hpp"
#include "Tasks/SpTask.hpp"
#include "Runtimes/SpRuntime.hpp"*/

#include "generator.hpp"

#define GRID_X_MIN 0.0
#define GRID_X_MAX 20.0
#define GRID_X_NUM 20
#define GRID_Y_MIN -8.0
#define GRID_Y_MAX 8.0
#define GRID_Y_NUM 16

int getGridXindexFloor(double x)
{
    if(x <= GRID_X_MIN)
    {
        return 0;
    }
    if(x >= GRID_X_MAX)
    {
        return GRID_X_NUM-1;
    }
    return (x-GRID_X_MIN)/(GRID_X_MAX-GRID_X_MIN)*(double)GRID_X_NUM;
}

int getGridYindexFloor(double y)
{
    if(y <= GRID_Y_MIN)
    {
        return 0;
    }
    if(y >= GRID_Y_MAX)
    {
        return GRID_Y_NUM-1;
    }
    return (y-GRID_Y_MIN)/(GRID_Y_MAX-GRID_Y_MIN)*(double)GRID_Y_NUM;
}

int getGridXindexCeil(double x)
{
    int val = getGridXindexFloor(x) + 1;
    if(val >= GRID_X_NUM)
    {
        return GRID_X_NUM-1;
    }
    return val;
}

int getGridYindexCeil(double y)
{
    int val = getGridYindexFloor(y) + 1;
    if(val >= GRID_Y_NUM)
    {
        return GRID_Y_NUM-1;
    }
    return val;
}

double getXdouble(int index)
{
    return ((double)index/(double)GRID_X_NUM)*(GRID_X_MAX-GRID_X_MIN) + GRID_X_MIN;
}

double getYdouble(int index)
{
    return ((double)index/(double)GRID_Y_NUM)*(GRID_Y_MAX-GRID_Y_MIN) + GRID_Y_MIN;
}

double getInterpolatedValue(double x, double y, double grid[GRID_X_NUM][GRID_Y_NUM])
{
    if(x < GRID_X_MIN)
    {
        x = GRID_X_MIN;
    }
    if(x > GRID_X_MAX)
    {
        x = GRID_X_MAX;
    }
    if(y < GRID_Y_MIN)
    {
        y = GRID_Y_MIN;
    }
    if(y > GRID_Y_MAX)
    {
        y = GRID_Y_MAX;
    }

    int mx = getGridXindexFloor(x);
    int Mx = getGridXindexCeil(x);
    int my = getGridYindexFloor(y);
    int My = getGridYindexCeil(y);

    double mxd = getXdouble(mx);
    double Mxd = getXdouble(Mx);
    double myd = getYdouble(my);
    double Myd = getYdouble(My);

    double alpha = 0.0f;
    double beta = 0.0f;

    if(fabs(Mxd - mxd) >= 0.00001f)
    {
        alpha = (x - mxd) / (Mxd - mxd);
    }

    if(fabs(Myd - myd) >= 0.00001f)
    {
        beta = (y - myd) / (Myd - myd);
    }

    return (1-beta)*(1-alpha)*grid[mx][my] + (1-beta)*alpha*grid[Mx][my] + beta*(1-alpha)*grid[mx][My] + beta*alpha*grid[Mx][My];
}

class EmulTask{
    const int id;
    const int type;

    std::vector<int> predecessorDependencies;
    std::vector<int> successorDependencies;

    std::string name;

    //! When the task has been created (construction time)
    double creationTime;
    //! When a task has been ready
    double readyTime;
    //! When the task has been computed (begin)
    double startingTime;
    //! When the task has been computed (end)
    double endingTime;

    //! The Id of the thread that computed the task
    long int threadIdComputer;

public:
    explicit EmulTask(const int inId, const int inType)
        : id(inId), type(inType), creationTime(0), readyTime(0),
          startingTime(0), endingTime(0), threadIdComputer(-1){
    }

    int getId() const{
        return id;
    }

    int getType() const{
        return type;
    }

    ////////////////////////////////////////////

    const std::vector<int>& getPredecessorDependencies() const {
        return predecessorDependencies;
    }

    void addPredecessorDependencies(const int inIdOther){
        predecessorDependencies.push_back(inIdOther);
    }

    const std::vector<int>& getSuccessorDependencies() const {
        return successorDependencies;
    }

    void addSuccessorDependencies(const int inIdOther){
        successorDependencies.push_back(inIdOther);
    }

    ////////////////////////////////////////////

    const std::string& getName() const {
        return name;
    }

    void setName(const std::string& inName){
        name = inName;
    }

    operator long int() const{
        return id;
    }

    double getStartingTime() const{
        return startingTime;
    }

    void setStartingTime(const double inStartingTime){
        startingTime = inStartingTime;
    }

    double getEndingTime() const{
        return endingTime;
    }

    void setEndingTime(const double inEndingTime){
        endingTime = inEndingTime;
    }

    double getCreationTime() const{
        return creationTime;
    }

    void setCreationTime(const double inCreationTime){
        creationTime = inCreationTime;
    }

    double getReadyTime() const{
        return readyTime;
    }

    void setReadyTime(const double inReadyTime){
        readyTime = inReadyTime;
    }

    int getThreadIdComputer() const{
        return threadIdComputer;
    }

    void setThreadIdComputer(const int inThreadIdComputed){
        threadIdComputer = inThreadIdComputed;
    }

    const std::string& getTaskName() const{
        return name;
    }
};


////////////////////////////////////////////////
////////////////////////////////////////////////

inline void EmulGenerateTrace(const std::string& outputFilename, const std::vector<EmulTask>& tasksFinished,
                          const double startingTime, const int nbWorkers, const std::vector<std::string>& workerLabels) {
    assert(nbWorkers == workerLabels.size());

    std::ofstream svgfile(outputFilename);

    if(svgfile.is_open() == false){
        throw std::invalid_argument("Cannot open filename : " + outputFilename);
    }

    const long int vsizeperthread = std::max(100, std::min(200, 2000/nbWorkers));
    const long int vmargin = 100;
    const long int threadstrock = 5;
    const long int vmarginthreadstats = 10;
    const long int vdim = nbWorkers * (vsizeperthread+threadstrock) + 2*vmargin + vmarginthreadstats + 2*vsizeperthread;

    double duration = 0;
    for(const auto& atask : tasksFinished){
        duration = std::max(duration, atask.getEndingTime()) - startingTime;
    }

    const long int hdimtime = std::max(6000, int(log(duration)*200));
    const long int hmargin = 50;
    const long int hdim = hdimtime + 2*hmargin;

    svgfile << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
    svgfile << "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"" << hdim << "\" height=\"" << vdim << "\">\n";
    svgfile << "  <title>Execution trace</title>\n";
    svgfile << "  <desc>\n";
    svgfile << "    Spetabaru traces for " << tasksFinished.size() << " tasks\n";
    svgfile << "  </desc>\n";
    svgfile << "\n";
    // Back
    svgfile << "  <rect width=\"" << hdim << "\" height=\"" << vdim << "\" x=\"0\" y=\"0\" fill=\"white\" />\n";


    svgfile << "  <line x1=\"" << hmargin << "\" y1=\"" << vmargin-10
            << "\" x2=\"" << hdimtime+hmargin << "\" y2=\"" << vmargin-10 << "\" stroke=\"" << "black" <<"\" stroke-width=\"" << "1" <<"\"/>\n";

    svgfile << "  <circle cx=\"" << hmargin << "\" cy=\"" << vmargin-10 << "\" r=\"" << "6" << "\" fill=\"" << "black" <<"\" />\n";
    svgfile << "  <circle cx=\"" << hdimtime+hmargin << "\" cy=\"" << vmargin-10 << "\" r=\"" << "6" << "\" fill=\"" << "black" << "\" />\n";

    for(double second = 1 ; second < duration ; second += 1.){
        svgfile << "  <line x1=\"" << static_cast<long  int>(double(hdimtime)*second/duration)+hmargin  << "\" y1=\"" << vmargin-14
                << "\" x2=\"" << static_cast<long  int>(double(hdimtime)*second/duration)+hmargin << "\" y2=\"" << vmargin-6 << "\" stroke=\"" << "black" <<"\" stroke-width=\"" << "1" <<"\"/>\n";
    }
    for(double second10 = 10 ; second10 < duration ; second10 += 10.){
        svgfile << "  <line x1=\"" << static_cast<long  int>(double(hdimtime)*second10/duration)+hmargin  << "\" y1=\"" << vmargin-18
                << "\" x2=\"" << static_cast<long  int>(double(hdimtime)*second10/duration)+hmargin << "\" y2=\"" << vmargin-2 << "\" stroke=\"" << "black" <<"\" stroke-width=\"" << "1" <<"\"/>\n";
    }

    {
        const std::string label = "Total time = " + std::to_string(duration) + " s";
        svgfile << "<text x=\"" << hmargin+hdimtime/2-int(label.size())*5 << "\" y=\"" << vmargin-20 <<
               "\" font-size=\"30\" fill=\"black\">" << label << "</text>\n";
    }

    for(int idxThread = 0 ; idxThread < nbWorkers ; ++idxThread){
        svgfile << "  <rect width=\"" << hdimtime << "\" height=\"" << vsizeperthread
                << "\" x=\"" << hmargin << "\" y=\"" << idxThread*(vsizeperthread+threadstrock) + vmargin << "\" style=\"fill:white;stroke:black;stroke-width:" << threadstrock << "\" />\n";

        const std::string label = workerLabels[idxThread];

        svgfile << "<text x=\"" << hmargin/2 << "\" y=\"" << idxThread*(vsizeperthread+threadstrock) + vmargin + label.size()*30 - vsizeperthread/2 <<
                   "\" font-size=\"30\" fill=\"black\" transform=\"rotate(-90, " << hmargin/2 << " "
                << idxThread*(vsizeperthread+threadstrock) + vmargin + label.size()*30 - vsizeperthread/2 << ")\">" << label << "</text>\n";
    }

    std::unordered_map<std::string, std::string> colors;

    for(const auto& atask : tasksFinished){
        const long int idxThreadComputer = atask.getThreadIdComputer() + 1;
        assert(idxThreadComputer != -1);
        assert(0 < idxThreadComputer && idxThreadComputer <= nbWorkers);
        const long int ypos_start = (idxThreadComputer-1)*(vsizeperthread+threadstrock) + threadstrock/2 + vmargin;
        const long int ypos_end = ypos_start + vsizeperthread - threadstrock;
        const double taskStartTime = atask.getStartingTime() - startingTime;
        const double taskEndTime = atask.getEndingTime() - startingTime;
        const long int xpos_start = static_cast<long  int>(double(hdimtime)*taskStartTime/duration) + hmargin;
        const long int xpos_end = std::max(xpos_start+1,static_cast<long  int>(double(hdimtime)*taskEndTime/duration) + hmargin);
        const long int taskstrocke = 2;

        std::string strForColor = atask.getTaskName();

        const std::size_t ddpos = strForColor.find("--");
        if(ddpos != std::string::npos){
            strForColor = strForColor.substr(0, ddpos);
        }
        if(atask.getTaskName().length() && atask.getTaskName().at(atask.getTaskName().length()-1) == '\''){
            strForColor.append("'");
        }

        if(colors.find(strForColor) == colors.end()){
            size_t hashname =  std::hash<std::string>()(strForColor);

            SpMTGenerator<> randEngine(hashname);
            const int colorR = int(randEngine.getRand01()*200) + 50;
            const int colorG = int(randEngine.getRand01()*200) + 50;
            const int colorB = int(randEngine.getRand01()*200) + 50;
            colors[strForColor] = std::string("rgb(")
                    + std::to_string(colorR) + ","
                    + std::to_string(colorG) + ","
                    + std::to_string(colorB) + ")";
        }

        svgfile << "<g>\n";
        svgfile << "    <title id=\"" << long(atask) << "\">" << SpUtils::ReplaceAllInString(SpUtils::ReplaceAllInString(SpUtils::ReplaceAllInString(atask.getTaskName(),"&", " REF "),"<","["),">","]")
                << " -- Duration " << atask.getEndingTime() - atask.getStartingTime() << "s"
                << " -- Enable = " << "TRUE" << "</title>\n";

        svgfile << "    <rect width=\"" << xpos_end-xpos_start << "\" height=\"" << ypos_end-ypos_start
                << "\" x=\"" << xpos_start << "\" y=\"" << ypos_start
                << "\" style=\"fill:" << colors[strForColor] << ";stroke:black" << ";stroke-width:" << taskstrocke << "\" />\n";

        const std::string label = SpUtils::ReplaceAllInString(SpUtils::ReplaceAllInString(SpUtils::ReplaceAllInString(atask.getTaskName(),"&", " REF "),"<","["),">","]");
        svgfile << "    <text x=\"" << (xpos_end+xpos_start)/2 << "\" y=\"" << (ypos_end+ypos_start)/2 << "\" font-size=\"10\" fill=\"black\">" << label << "</text>\n";

        svgfile << "</g>\n";
    }

    const long int offsetStat = nbWorkers*(vsizeperthread+threadstrock) + vmarginthreadstats;
    const char* statsNames[] = {"Submited", "Ready"};
    for(int idxStat = 0 ; idxStat < 2 ; ++idxStat){
        svgfile << "  <rect width=\"" << hdimtime << "\" height=\"" << vsizeperthread
                << "\" x=\"" << hmargin << "\" y=\"" << offsetStat + idxStat*(vsizeperthread+threadstrock) + vmargin << "\" style=\"fill:white;stroke:black;stroke-width:" << threadstrock << "\" />\n";

        const std::string label = statsNames[idxStat];

        svgfile << "<text x=\"" << hmargin/2 << "\" y=\"" << offsetStat + idxStat*(vsizeperthread+threadstrock+50) + vmargin + label.size()*30 - vsizeperthread/2 <<
                   "\" font-size=\"30\" fill=\"black\" transform=\"rotate(-90, " << hmargin/2 << " "
                << offsetStat + idxStat*(vsizeperthread+threadstrock+50) + vmargin + label.size()*30 - vsizeperthread/2 << ")\">" << label << "</text>\n";
    }

    std::vector<int> nbReady(hdimtime, 0);
    std::vector<int> nbSubmited(hdimtime, 0);

    for(const auto& atask : tasksFinished){
        const double taskSubmitedTime = atask.getCreationTime() - startingTime;
        const double taskReadyTime = atask.getReadyTime() - startingTime;
        const double taskStartTime = atask.getStartingTime() - startingTime;
        const long int xpos_submited = static_cast<long  int>(double(hdimtime)*taskSubmitedTime/duration);
        const long int xpos_ready = static_cast<long  int>(double(hdimtime)*taskReadyTime/duration);
        const long int xpos_start = static_cast<long  int>(double(hdimtime)*taskStartTime/duration);

        nbSubmited[xpos_submited] += 1;

        nbReady[xpos_ready] += 1;
        if(xpos_ready != xpos_start || xpos_ready == hdimtime-1){
            nbReady[xpos_start] -= 1;
        }
        else{
            nbReady[xpos_start+1] -= 1;
        }
    }

    int maxReady = 0;
    {
        int currentStat = 0;
        for(int idx = 0 ; idx < hdimtime ; ++idx){
            currentStat += nbReady[idx];
            maxReady = std::max(maxReady , currentStat);
        }
    }

    const std::reference_wrapper<const std::vector<int>> statVal[] = {nbSubmited, nbReady};
    const int maxStatVal[2] = {static_cast<int>(tasksFinished.size()), maxReady};

    for(int idxStat = 0 ; idxStat < 2 ; ++idxStat){
         svgfile << "<polyline points=\"";
         //20,20 40,25 60,40 80,120 120,140 200,180"
         const int maxStat = maxStatVal[idxStat];
         int currentStat = 0;
         for(int idx = 0 ; idx < hdimtime ; ++idx){
             currentStat += statVal[idxStat].get()[idx];
             const long int xpos = hmargin+idx;
             const long int ypos = offsetStat + idxStat*(vsizeperthread+threadstrock) + vmargin
                                   + vsizeperthread
                                   - static_cast<long int>(double(vsizeperthread-threadstrock)*double(currentStat)/double(maxStat))
                                   - threadstrock/2;
             svgfile << xpos << "," << ypos << " ";
         }
         svgfile << "\" style=\"fill:none;stroke:rgb(112,0,0);stroke-width:3\" />\n";
    }

    svgfile << "<text x=\"" << hmargin + hdimtime + 10
            << "\" y=\"" << offsetStat + 0*(vsizeperthread+threadstrock) + vmargin + 15 <<
               "\" font-size=\"30\" fill=\"black\">" << tasksFinished.size() << "</text>\n";
    svgfile << "<text x=\"" << hmargin + hdimtime + 10
            << "\" y=\"" << offsetStat + 1*(vsizeperthread+threadstrock) + vmargin + 15 <<
               "\" font-size=\"30\" fill=\"black\">" << maxReady << "</text>\n";

    svgfile << "</svg>\n";
}

////////////////////////////////////////////////
////////////////////////////////////////////////

std::vector<EmulTask> GenerateTasksFacto(const int inMatrixDim, const int BlockDim){
    const int NumThreads = SpUtils::DefaultNumThreads();
    SpRuntime runtime(NumThreads);

    // See https://pdfs.semanticscholar.org/ef8f/acdd395a4280c8af1fa76e0bb5f6915abaa6.pdf
    const int nbBlocks = (inMatrixDim+BlockDim-1)/BlockDim;

    std::vector<int> fakeBlock(nbBlocks*nbBlocks, -1);

    std::vector<EmulTask> allTasks;
    allTasks.reserve(nbBlocks*nbBlocks*nbBlocks);

    std::mutex cond_mutex;
    std::condition_variable cond_var;
    bool cond_bool = false;

    for(int k = 0 ; k < nbBlocks ; ++k){
        // POTRF( A(k,k) )
        runtime.task(SpPriority(0), SpWrite(fakeBlock[k*nbBlocks+k]),
                     [&,k](const int& /*block*/){
            if(k == 0){
                // Block the first task until all tasks have been inserted
                std::unique_lock<std::mutex> lock(cond_mutex);
                while(!cond_bool){
                    cond_var.wait(lock);
                }
            }
        }).setTaskName(std::string("POTRF -- (W-")+std::to_string(k)+","+std::to_string(k)+")");
        {
            EmulTask tk(allTasks.size(), 0);
            tk.setName(std::string("POTRF -- (W-")+std::to_string(k)+","+std::to_string(k)+")");
            allTasks.push_back(tk);
        }

        for(int m = k + 1 ; m < nbBlocks ; ++m){
            // TRSM( A(k,k) A(m, k) )
            runtime.task(SpPriority(1), SpRead(fakeBlock[k*nbBlocks+k]), SpWrite(fakeBlock[m*nbBlocks+k]),
                         [](const int& /*block*/, const int& /*block*/){
            }).setTaskName(std::string("TRSM -- (R-")+std::to_string(k)+","+std::to_string(k)+") (W-"+std::to_string(m)+","+std::to_string(k)+")");
            {
                EmulTask tk(allTasks.size(), 1);
                tk.setName(std::string("TRSM -- (R-")+std::to_string(k)+","+std::to_string(k)+") (W-"+std::to_string(m)+","+std::to_string(k)+")");
                allTasks.push_back(tk);
            }
        }

        for(int m = k + 1 ; m < nbBlocks ; ++m){
            // SYRK( A(m, k) A(m, m))
            runtime.task(SpPriority(2), SpRead(fakeBlock[m*nbBlocks+k]), SpWrite(fakeBlock[m*nbBlocks+m]),
                         [](const int& /*block*/, const int& /*block*/){
            }).setTaskName(std::string("SYRK -- (R")+std::to_string(m)+","+std::to_string(k)+")(W"+std::to_string(k)+","+std::to_string(k)+")");
            {
                EmulTask tk(allTasks.size(), 2);
                tk.setName(std::string("SYRK -- (R")+std::to_string(m)+","+std::to_string(k)+")(W"+std::to_string(k)+","+std::to_string(k)+")");
                allTasks.push_back(tk);
            }

            for(int n = k + 1 ; n < nbBlocks ; ++n){
                // GEMM( A(m, k) A(n, k) A(m, n))
                runtime.task(SpPriority(3), SpRead(fakeBlock[m*nbBlocks+k]), SpRead(fakeBlock[n*nbBlocks+k]), SpWrite(fakeBlock[m*nbBlocks+n]),
                             [](const int& /*block*/, const int& /*block*/, const int& /*block*/){
                }).setTaskName(std::string("GEMM -- (R-")+std::to_string(m)+","+std::to_string(k)+")(R-"+std::to_string(n)+","+std::to_string(k)+")(W-"+std::to_string(m)+","+std::to_string(n)+")");
                {
                    EmulTask tk(allTasks.size(), 3);
                    tk.setName(std::string("GEMM -- (R-")+std::to_string(m)+","+std::to_string(k)+")(R-"+std::to_string(n)+","+std::to_string(k)+")(W-"+std::to_string(m)+","+std::to_string(n)+")");
                    allTasks.push_back(tk);
                }
            }
        }
    }


    std::cout << "Tasks submited." << std::endl;
    cond_bool = true;
    cond_var.notify_all();

    runtime.waitAllTasks();
    runtime.stopAllThreads();
    runtime.generateDot("/tmp/graph.dot");

    {
        std::vector<SpAbstractTask*> deps;

        for(const auto& atask : runtime.getFinishedTasks()){
            deps.clear();
            atask->getDependences(&deps);

            std::set<SpAbstractTask*> alreadyExist;

            for(const auto& taskDep : deps){
                if(alreadyExist.find(taskDep) == alreadyExist.end()){
                    allTasks[atask->getId()].addSuccessorDependencies(taskDep->getId());
                    allTasks[taskDep->getId()].addPredecessorDependencies(atask->getId());
                    alreadyExist.insert(taskDep);
                }
            }
        }
    }

    {// Simply check coherency
        for(const auto& tsk : allTasks){
            for(const int idxPred : tsk.getPredecessorDependencies()){
                assert(std::find(allTasks[idxPred].getSuccessorDependencies().begin(),
                                 allTasks[idxPred].getSuccessorDependencies().end(),
                                 tsk.getId())
                       != allTasks[idxPred].getSuccessorDependencies().end());
            }
        }
    }

    return allTasks;
}


std::vector<EmulTask> GenerateTasksTree(const int inDepth){
    SpRuntime runtime(1);

    // See https://pdfs.semanticscholar.org/ef8f/acdd395a4280c8af1fa76e0bb5f6915abaa6.pdf
    const int nbBlocks = (1 << inDepth);

    std::vector<std::vector<int>> fakeBlock(nbBlocks);

    std::vector<EmulTask> allTasks;
    allTasks.reserve(nbBlocks);

    std::mutex cond_mutex;
    std::condition_variable cond_var;
    bool cond_bool = false;

    int truc;
    runtime.task(SpPriority(99), SpWrite(truc),
                 [&](int&){
        // Block the first task until all tasks have been inserted
        std::unique_lock<std::mutex> lock(cond_mutex);
        while(!cond_bool){
            cond_var.wait(lock);
        }
    }).setTaskName(std::string("Bloc worker"));

    for(int idxLevel = inDepth-1 ; idxLevel >= 0 ; --idxLevel){
        const int sizeAtLevel = (1 << idxLevel);
        fakeBlock[idxLevel].resize(sizeAtLevel);

        for(int idxNode = 0 ; idxNode < (1 << idxLevel) ; ++idxNode){
            runtime.task(SpPriority(0), SpWrite(fakeBlock[idxLevel][idxNode]),
                         [](const int& /*block*/){
            }).setTaskName(std::string("InCell (W-")+std::to_string(idxNode)+")");
            {
                EmulTask tk(allTasks.size(), 0);
                tk.setName(std::string("InCell -- (W-")+std::to_string(idxNode)+")");
                allTasks.push_back(tk);
            }
        }
    }

    for(int idxLevel = inDepth-2 ; idxLevel >= 0 ; --idxLevel){
        const int sizeAtLevel = (1 << idxLevel);

        for(int idxNode = 0 ; idxNode < (1 << idxLevel) ; ++idxNode){
            runtime.task(SpPriority(1), SpWrite(fakeBlock[idxLevel][idxNode]),
                         SpRead(fakeBlock[idxLevel+1][idxNode*2]),
                         SpRead(fakeBlock[idxLevel+1][idxNode*2+1]),
                         [](const int& /*block*/,const int& /*block*/,const int& /*block*/){
            }).setTaskName(std::string("M2M (W-")+std::to_string(idxNode)+" R-"+std::to_string(idxNode*2)+" R-"+std::to_string(idxNode*2+1)+")");
            {
                EmulTask tk(allTasks.size(), 1);
                tk.setName(std::string("M2M (W-")+std::to_string(idxNode)+" R-"+std::to_string(idxNode*2)+" R-"+std::to_string(idxNode*2+1)+")");
                allTasks.push_back(tk);
            }
        }
    }

    for(int idxLevel = inDepth-1 ; idxLevel >= 0 ; --idxLevel){
        const int sizeAtLevel = (1 << idxLevel);
        fakeBlock[idxLevel].resize(sizeAtLevel);

        for(int idxNode = 1 ; idxNode < (1 << idxLevel)-1 ; ++idxNode){
            runtime.task(SpPriority(2), SpWrite(fakeBlock[idxLevel][idxNode]),
                         SpRead(fakeBlock[idxLevel][idxNode-1]),
                         SpRead(fakeBlock[idxLevel][idxNode+1]),
                         [](const int& /*block*/,const int& /*block*/,const int& /*block*/){
            }).setTaskName(std::string("M2L (W-")+std::to_string(idxNode)+" R-"+std::to_string(idxNode-1)+" R-"+std::to_string(idxNode+1)+")");
            {
                EmulTask tk(allTasks.size(), 2);
                tk.setName(std::string("M2L (W-")+std::to_string(idxNode)+" R-"+std::to_string(idxNode-1)+" R-"+std::to_string(idxNode+1)+")");
                allTasks.push_back(tk);
            }
        }
    }

    for(int idxLevel = 0 ; idxLevel < inDepth-1 ; ++idxLevel){
        const int sizeAtLevel = (1 << idxLevel);

        for(int idxNode = 0 ; idxNode < (1 << idxLevel) ; ++idxNode){
            runtime.task(SpPriority(3), SpWrite(fakeBlock[idxLevel][idxNode]),
                         SpRead(fakeBlock[idxLevel+1][idxNode*2]),
                         SpRead(fakeBlock[idxLevel+1][idxNode*2+1]),
                         [](const int& /*block*/,const int& /*block*/,const int& /*block*/){
            }).setTaskName(std::string("M2M (W-")+std::to_string(idxNode)+" R-"+std::to_string(idxNode*2)+" R-"+std::to_string(idxNode*2+1)+")");
            {
                EmulTask tk(allTasks.size(), 3);
                tk.setName(std::string("M2M (W-")+std::to_string(idxNode)+" R-"+std::to_string(idxNode*2)+" R-"+std::to_string(idxNode*2+1)+")");
                allTasks.push_back(tk);
            }
        }
    }


    std::cout << "Tasks submited." << std::endl;
    cond_bool = true;
    cond_var.notify_all();

    runtime.waitAllTasks();
    runtime.stopAllThreads();
    runtime.generateDot("/tmp/graph.dot");

    {
        std::vector<SpAbstractTask*> deps;

        for(const auto& atask : runtime.getFinishedTasks()){
            if(atask->getId() != 0){
                deps.clear();
                atask->getDependences(&deps);

                std::set<SpAbstractTask*> alreadyExist;

                for(const auto& taskDep : deps){
                    if(alreadyExist.find(taskDep) == alreadyExist.end()){
                        allTasks[atask->getId()-1].addSuccessorDependencies(taskDep->getId()-1);
                        allTasks[taskDep->getId()-1].addPredecessorDependencies(atask->getId()-1);
                        alreadyExist.insert(taskDep);
                    }
                }
            }
        }
    }

    {// Simply check coherency
        for(const auto& tsk : allTasks){
            for(const int idxPred : tsk.getPredecessorDependencies()){
                assert(std::find(allTasks[idxPred].getSuccessorDependencies().begin(),
                                 allTasks[idxPred].getSuccessorDependencies().end(),
                                 tsk.getId()-1)
                       != allTasks[idxPred].getSuccessorDependencies().end());
            }
        }
    }

    return allTasks;
}

std::vector<EmulTask> GenerateTasksFromDotFile(const std::string& inFilename)
{
    std::vector<EmulTask> allTasks;

    DotFileData fileData = Generator::LoadEdgeFile(inFilename);

    for(auto taskIt = fileData.taskTypes.begin();taskIt != fileData.taskTypes.end();++taskIt)
    {
        EmulTask tk(taskIt->first, taskIt->second);
        tk.setName(fileData.taskTypeNames[taskIt->second]);
        allTasks.push_back(tk);
    }

    for(int d=0;d<fileData.dependencies.size();++d)
    {
        int from = fileData.dependencies[d].first;
        int to = fileData.dependencies[d].second;

        std::find_if(allTasks.begin(), allTasks.end(), [&](const EmulTask &e){return e.getId()==to;})->addPredecessorDependencies(from);
        std::find_if(allTasks.begin(), allTasks.end(), [&](const EmulTask &e){return e.getId()==from;})->addSuccessorDependencies(to);
    }

/*
    std::cout << "Task type names : ";
    for(int i=0;i<fileData.taskTypeNames.size();++i)
    {
        std::cout << fileData.taskTypeNames[i] << " ";
    }
    std::cout << std::endl;

    for(int i=0;i<3000;++i)
    {
        std::cout << i << " : " << fileData.taskTypeNames[fileData.taskTypes[i]] << std::endl;
    }
*/
/*
    std::cout << "Predecessors of task 3 :";
    std::vector<int> predecessors = std::find_if(allTasks.begin(), allTasks.end(), [&](const EmulTask &e){return e.getId()==3;})->getPredecessorDependencies();
    for(int i=0;i<predecessors.size();++i)
    {
        std::cout << predecessors[i] << " ";
    }
    std::cout << std::endl;
    std::cout << "Successors of task 3 :";
    std::vector<int> successors = std::find_if(allTasks.begin(), allTasks.end(), [&](const EmulTask &e){return e.getId()==3;})->getSuccessorDependencies();
    for(int i=0;i<successors.size();++i)
    {
        std::cout << successors[i] << " ";
    }
    std::cout << std::endl;
*/
    return allTasks;
}

std::vector<EmulTask> GenerateExampleTasks(void){
    std::vector<EmulTask> allTasks;

    EmulTask tk1(allTasks.size(), 0);
    tk1.setName("A");
    allTasks.push_back(tk1);

    EmulTask tk2(allTasks.size(), 1);
    tk2.setName("B");
    allTasks.push_back(tk2);

    EmulTask tk3(allTasks.size(), 0);
    tk3.setName("A");
    allTasks.push_back(tk3);

    EmulTask tk4(allTasks.size(), 1);
    tk4.setName("B");
    allTasks.push_back(tk4);

    EmulTask tk5(allTasks.size(), 2);
    tk5.setName("C");
    allTasks.push_back(tk5);

    EmulTask tk6(allTasks.size(), 0);
    tk6.setName("A");
    allTasks.push_back(tk6);

    EmulTask tk7(allTasks.size(), 1);
    tk7.setName("B");
    allTasks.push_back(tk7);

    EmulTask tk8(allTasks.size(), 2);
    tk8.setName("C");
    allTasks.push_back(tk8);

    EmulTask tk9(allTasks.size(), 0);
    tk9.setName("A");
    allTasks.push_back(tk9);

    EmulTask tk10(allTasks.size(), 1);
    tk10.setName("B");
    allTasks.push_back(tk10);

    EmulTask tk11(allTasks.size(), 1);
    tk11.setName("B");
    allTasks.push_back(tk11);

    tk1.addSuccessorDependencies(2);
    tk1.addSuccessorDependencies(3);

    tk2.addSuccessorDependencies(3);
    tk2.addSuccessorDependencies(4);

    tk3.addPredecessorDependencies(0);
    tk3.addSuccessorDependencies(5);
    tk3.addSuccessorDependencies(6);

    tk4.addPredecessorDependencies(1);
    tk4.addSuccessorDependencies(6);
    tk4.addSuccessorDependencies(7);

    tk5.addPredecessorDependencies(1);
    tk5.addSuccessorDependencies(7);
    tk5.addSuccessorDependencies(8);
    tk5.addSuccessorDependencies(9);

    tk6.addPredecessorDependencies(2);
    tk6.addSuccessorDependencies(10);

    tk7.addPredecessorDependencies(2);
    tk7.addPredecessorDependencies(3);
    tk7.addSuccessorDependencies(10);

    tk8.addPredecessorDependencies(3);
    tk8.addPredecessorDependencies(4);
    tk8.addSuccessorDependencies(10);

    tk9.addPredecessorDependencies(4);
    tk9.addSuccessorDependencies(10);

    tk10.addPredecessorDependencies(4);
    tk10.addSuccessorDependencies(10);

    tk11.addPredecessorDependencies(5);
    tk11.addPredecessorDependencies(6);
    tk11.addPredecessorDependencies(7);
    tk11.addPredecessorDependencies(8);
    tk11.addPredecessorDependencies(9);

    return allTasks;
}

int getBucketMinIndex(std::vector<float> bucket)
{
    int minIndex = 0;
    float minValue = bucket[0];

    for(int i=1;i<bucket.size();++i)
    {
        if(bucket[i] < minValue)
        {
            minIndex = i;
            minValue = bucket[i];
        }
    }

    return minIndex;
}

float getProportionInVector(std::vector<int> vec, int elt)
{
    int sum=0;
    for(int i=0;i<vec.size();++i)
    {
        sum += vec[i];
    }
    return (float)vec[elt]/(float)sum;
}

float randf01(void)
{
    return (float)rand()/(float)(RAND_MAX);
}

#include <bits/stdc++.h> 
#define CAN_EXECUTE_ON_CPU(cost) ((cost) >= 0.0f)
#define CAN_EXECUTE_ON_GPU(cost) ((cost) >= 0.0f)
#define NO_EXECUTION (-1.0f)
#define NO_ACCELERATION (-1.0f)

std::vector<EmulTask> GenerateTasksWithPerfectSolution(int seed, int taskNumber, std::map<int, std::vector<int>> predecessorRule, std::vector<float> taskTypeProportion,
               const std::vector<double>& inCpuCost,
               const std::vector<double>& inGpuCost,
               const int nbCpus,
               const int nbGpus){
    srand(seed);

    std::vector<EmulTask> allTasks;
    std::vector<int> finishedTasks;
    std::vector<std::pair<int, float>> pendingTasks; // task, finishTime

    std::vector<int> taskTypeNum; // number of task of each type

    for(unsigned int t=0;t<taskTypeProportion.size();++t)
    { // at the beginning, there is 0 of each task
        taskTypeNum.push_back(0);
    }

    std::vector<float> gpuAcceleration;
    for(unsigned int t=0;t<inCpuCost.size();++t)
    {
        if(!CAN_EXECUTE_ON_CPU(inCpuCost[t]))
        {
            gpuAcceleration.push_back(NO_ACCELERATION);
        }
        else if(!CAN_EXECUTE_ON_GPU(inGpuCost[t]))
        {
            gpuAcceleration.push_back(NO_ACCELERATION);
        }
        else
        {
            gpuAcceleration.push_back(inCpuCost[t] / inGpuCost[t]);
        }
    }

    std::vector<int> perfectTasksForCPU;
    std::vector<int> perfectTasksForGPU;

    for(int t=0;t<inCpuCost.size();++t)
    {
        if(!CAN_EXECUTE_ON_CPU(inCpuCost[t]))
        {
            if(CAN_EXECUTE_ON_GPU(inGpuCost[t]))
            {
                perfectTasksForGPU.push_back(t);
            }
        }
        else if(!CAN_EXECUTE_ON_GPU(inGpuCost[t]))
        {
            if(CAN_EXECUTE_ON_CPU(inCpuCost[t]))
            {
                perfectTasksForCPU.push_back(t);
            }
        }
        else
        {
            if(gpuAcceleration[t] > 1.0f)
            {
                perfectTasksForGPU.push_back(t);
            }
            else if(gpuAcceleration[t] < 1.0f)
            {
                perfectTasksForCPU.push_back(t);
            }
            else
            {
                perfectTasksForGPU.push_back(t);
                perfectTasksForCPU.push_back(t);
            }
        }
    }

    if(perfectTasksForCPU.size() == 0 || perfectTasksForGPU.size() == 0)
    {
        std::cerr << "Unable to generate without having tasks that are better on a CPU and a GPU at the moment" << std::endl;
        std::cerr << "Costs :" << std::endl;
        std::cout << "CPU : ";
        for(int i=0;i<inCpuCost.size();++i)
        {
            std::cout << inCpuCost[i] << " ";
        }
        std::cout << std::endl;
        std::cout << "GPU : ";
        for(int i=0;i<inGpuCost.size();++i)
        {
            std::cout << inGpuCost[i] << " ";
        }
        std::cout << std::endl;
        exit(1);
    }

    std::vector<float> expectedPorportionPerfectTaskCPU;
    std::vector<float> expectedPorportionPerfectTaskGPU;

    float sumCPU = 0.f;
    for(int t=0;t<perfectTasksForCPU.size();++t)
    {
        sumCPU += taskTypeProportion[perfectTasksForCPU[t]];
    }
    for(int t=0;t<perfectTasksForCPU.size();++t)
    {
        expectedPorportionPerfectTaskCPU.push_back(taskTypeProportion[perfectTasksForCPU[t]]/sumCPU);
    }
    float sumGPU = 0.f;
    for(int t=0;t<perfectTasksForGPU.size();++t)
    {
        sumGPU += taskTypeProportion[perfectTasksForGPU[t]];
    }
    for(int t=0;t<perfectTasksForGPU.size();++t)
    {
        expectedPorportionPerfectTaskGPU.push_back(taskTypeProportion[perfectTasksForGPU[t]]/sumGPU);
    }

/*
    Print expected proportion

    std::cout << "expected proportion CPU: ";
    for(int i=0;i<expectedPorportionPerfectTaskCPU.size();++i)
    {
        std::cout << expectedPorportionPerfectTaskCPU[i] << " ";
    }
    std::cout << std::endl;
    std::cout << "expected proportion GPU: ";
    for(int i=0;i<expectedPorportionPerfectTaskGPU.size();++i)
    {
        std::cout << expectedPorportionPerfectTaskGPU[i] << " ";
    }
    std::cout << std::endl;
*/

    std::vector<float> cpuBuckets;
    std::vector<float> gpuBuckets;

    for(int i=0;i<nbCpus;++i)
    {
        cpuBuckets.push_back(0.0f);
    }
    for(int i=0;i<nbGpus;++i)
    {
        gpuBuckets.push_back(0.0f);
    }

    for(int t=0;t<taskNumber;++t)
    {
        int minCPU = getBucketMinIndex(cpuBuckets);
        int minGPU = getBucketMinIndex(gpuBuckets);

        float currentTime = std::min(cpuBuckets[minCPU], gpuBuckets[minGPU]);

        for(auto it=pendingTasks.begin();it!=pendingTasks.end();)
        { // check if tasks have been finished
            if(currentTime >= it->second)
            {
                finishedTasks.push_back(it->first);
                it = pendingTasks.erase(it);
            }
            else
            {
                ++it;
            }
        }

        int taskType;
        float cumulatedPorportion;
        if(cpuBuckets[minCPU] < gpuBuckets[minGPU])
        { // add a task for the cpu
/*
            do
            {
                taskType = perfectTasksForCPU[rand()%perfectTasksForCPU.size()];
            }while((taskTypeProportion[taskType] < (float)taskTypeNum[taskType]/(float)(t+1)) && randf01()<0.9);
*/

            do
            {
                taskType = rand()%perfectTasksForCPU.size();

                if(randf01()<0.4f && randf01() < expectedPorportionPerfectTaskCPU[taskType])
                {
                    break;
                }
            }while(getProportionInVector(taskTypeNum, taskType) >= expectedPorportionPerfectTaskCPU[taskType] && randf01()<0.999);

            taskType = perfectTasksForCPU[taskType];

/*
            cumulatedPorportion = 0;
            for(taskType=0;taskType<perfectTasksForCPU.size();++taskType)
            {
                cumulatedPorportion += expectedPorportionPerfectTaskCPU[taskType];
                if(randf01() < cumulatedPorportion)
                {
                    break;
                }
            }
*/
        }
        else
        { // add a task for the gpu
/*
            do
            {
                taskType = perfectTasksForGPU[rand()%perfectTasksForGPU.size()];
            }while((taskTypeProportion[taskType] < (float)taskTypeNum[taskType]/(float)(t+1)) && randf01()<0.9);
*/

            do
            {
                taskType = rand()%perfectTasksForGPU.size();

                if(randf01()<0.4f && randf01() < expectedPorportionPerfectTaskGPU[taskType])
                {
                    break;
                }
            }while(getProportionInVector(taskTypeNum, taskType) >= expectedPorportionPerfectTaskGPU[taskType] && randf01()<0.999);

            taskType = perfectTasksForGPU[taskType];

/*
            cumulatedPorportion = 0;
            for(taskType=0;taskType<perfectTasksForGPU.size();++taskType)
            {std::cerr << cumulatedPorportion << std::endl;
                cumulatedPorportion += expectedPorportionPerfectTaskGPU[taskType];
                if(randf01() < cumulatedPorportion)
                {
                    break;
                }
            }
*/
        }

        ++taskTypeNum[taskType]; // a task of type taskType will be added, we need to update taskTypeNum's counter

/*
        int wantedPredecessors = 1+rand()%predecessorNumRange;
        for(int p=0;p<std::min((int)wantedPredecessors, (int)finishedTasks.size());++p)
        {
            int currentPredecessor = std::max(0, (int)finishedTasks.size()-1-rand()%10);
            if(std::find(tk.getPredecessorDependencies().begin(), tk.getPredecessorDependencies().end(), currentPredecessor) == tk.getPredecessorDependencies().end())
            {
                tk.addPredecessorDependencies(currentPredecessor);
                allTasks[currentPredecessor].addSuccessorDependencies(t);
            }
        }
*/

        std::vector<int> predecessors;
        std::vector<int> wantedNumPredecessors = predecessorRule[taskType];
        if(randf01() < 0.9)
        {
            for(int tt=0;tt<wantedNumPredecessors.size();++tt)
            {
                int wantedNumForTaskType = wantedNumPredecessors[tt];
                for(int p=finishedTasks.size()-1;p>=0&&wantedNumForTaskType>0;--p)
                {
                    if(allTasks[finishedTasks[p]].getType() == tt)
                    {
                        if(std::find(predecessors.begin(), predecessors.end(), finishedTasks[p]) == predecessors.end())
                        { // add task to predecessors
                            --wantedNumForTaskType;
                            predecessors.push_back(finishedTasks[p]);
                        }
                    }
                }
                if(wantedNumForTaskType > 0)
                { // we couldnt add as much predecessors as we wanted, lets add random ones
                    for(int p=0;p<std::min((int)wantedNumForTaskType, (int)finishedTasks.size());++p)
                    {
                        int pickedPredecessor = std::max(0, (int)finishedTasks.size()-1-rand()%10);
                        if(std::find(predecessors.begin(), predecessors.end(), pickedPredecessor) == predecessors.end())
                        {
                            --wantedNumForTaskType; // useless ?
                            predecessors.push_back(finishedTasks[pickedPredecessor]);
                        }
                    }
                }
            }
        }
        else
        {
            int wantedNumForTaskType = 1+rand()%2;
            for(int p=0;p<std::min((int)wantedNumForTaskType, (int)finishedTasks.size());++p)
            {
                int pickedPredecessor = std::max(0, (int)finishedTasks.size()-1-rand()%10);
                if(std::find(predecessors.begin(), predecessors.end(), pickedPredecessor) == predecessors.end())
                {
                    --wantedNumForTaskType; // useless ?
                    predecessors.push_back(finishedTasks[pickedPredecessor]);
                }
            }
        }

        if(cpuBuckets[minCPU] < gpuBuckets[minGPU])
        { // add a task for the cpu
            EmulTask tk(t, taskType);
            tk.setName("task "+std::to_string(taskType));

            for(int tp=0;tp<predecessors.size();++tp)
            {
                tk.addPredecessorDependencies(predecessors[tp]);
                allTasks[predecessors[tp]].addSuccessorDependencies(t);
            }

            allTasks.push_back(tk);
            cpuBuckets[minCPU] += inCpuCost[taskType];
            pendingTasks.push_back(std::pair<int, float>(t, cpuBuckets[minCPU]));
        }
        else
        { // add a task for the gpu
            EmulTask tk(t, taskType);
            tk.setName("task "+std::to_string(taskType));

            for(int tp=0;tp<predecessors.size();++tp)
            {
                tk.addPredecessorDependencies(predecessors[tp]);
                allTasks[predecessors[tp]].addSuccessorDependencies(t);
            }

            allTasks.push_back(tk);
            gpuBuckets[minGPU] += inGpuCost[taskType];
            pendingTasks.push_back(std::pair<int, float>(t, gpuBuckets[minGPU]));
        }

/*
        std::cout << "Predecessors for task " << t << " (type " << taskType << ") : ";
        for(int i=0;i<predecessors.size();++i)
        {
            std::cout << predecessors[i] << " ";
        }
        std::cout << std::endl;
*/
    }

/*
    std::cout << "Number of tasks for each type: ";
    for(int i=0;i<taskTypeNum.size();++i)
    {
        std::cout << taskTypeNum[i] << " ";
    }
    std::cout << std::endl;
*/

    float maxCpu = *max_element(cpuBuckets.begin(), cpuBuckets.end());
    float maxGpu = *max_element(gpuBuckets.begin(), gpuBuckets.end());
/*
    for(int b=0;b<cpuBuckets.size();++b)
    {
        std::cout << "CPU bucket " << b << " finished job in " << cpuBuckets[b] << "s" << std::endl;
    }
    for(int b=0;b<gpuBuckets.size();++b)
    {
        std::cout << "GPU bucket " << b << " finished job in " << gpuBuckets[b] << "s" << std::endl;
    }
*/

    //std::cout << "Generated task tree. Optimal time: " << std::max(maxCpu, maxGpu) << std::endl;


    return allTasks;
}


////////////////////////////////////////////////
////////////////////////////////////////////////

struct ExecutionStatistics
{
    std::array<double, 2> busyTime;
    std::vector<std::pair<double, double>> taskCPUGPUproportion; // proportion of each task's execution on CPU and GPU
};

double Execute(std::vector<EmulTask>& inAllTasks,
               const std::vector<double>& inCpuCost,
               const std::vector<double>& inGpuCost,
               const std::vector<int>& inPriorityCpu,
               const std::vector<int>& inPriorityGpu,
               const int nbCpus,
               const int nbGpus,
               ExecutionStatistics *executionStatistics = NULL){
    if(inAllTasks.size() == 0){
        return 0;
    }


    struct Worker{
        double scheduledAvailable;
        int currentTaskId;
        int type;
        int currentWorkerId;
        bool operator<(const Worker& other) const{
            return scheduledAvailable > other.scheduledAvailable;
        }
    };

    const int nbWorkerTypes = 2;
    std::vector<int> idleWorkerCount[2];

    idleWorkerCount[0].resize(nbCpus);
    std::iota(idleWorkerCount[0].begin(), idleWorkerCount[0].end(), 0);

    idleWorkerCount[1].resize(nbGpus);
    std::iota(idleWorkerCount[1].begin(), idleWorkerCount[1].end(), nbCpus);

    std::array<std::vector<double>,2> costs{ inCpuCost, inGpuCost};
    std::array<std::vector<int>,2> priorities{ inPriorityCpu, inPriorityGpu};

    const int nbBuckets = inCpuCost.size();
    assert(nbBuckets == inCpuCost.size());
    assert(nbBuckets == inGpuCost.size());
    assert(nbBuckets == inPriorityCpu.size());
    assert(nbBuckets == inPriorityGpu.size());

    std::vector<std::vector<int>> buckets(nbBuckets);

    for(const EmulTask& tsk : inAllTasks){
        inAllTasks[tsk.getId()].setCreationTime(0);
    }

    for(const EmulTask& tsk : inAllTasks){
        if(tsk.getPredecessorDependencies().size() == 0){
            buckets[tsk.getType()].push_back(tsk.getId());
            inAllTasks[tsk.getId()].setReadyTime(0);
        }
    }

    // stats data

    if(executionStatistics != NULL)
    {
        for(int idxType = 0 ; idxType < nbWorkerTypes ; ++idxType)
        {
            executionStatistics->busyTime[idxType] = 0;
        }

        executionStatistics->taskCPUGPUproportion.clear();
        for(int taskType = 0 ; taskType < nbBuckets ; ++taskType)
        {
            executionStatistics->taskCPUGPUproportion.push_back(std::make_pair(0.0f, 0.0f));
        }
    }


/*
    for(int idxBucket = 0 ; idxBucket < nbBuckets ; ++idxBucket){
        std::cout << "Ready tasks at starting time in bucket " << idxBucket << " => " << buckets[idxBucket].size() << std::endl;
    }
*/

    std::vector<int> countPredecessorsOver(inAllTasks.size(), 0);
    std::priority_queue<Worker> workers;
    int nbComputedTask = 0;
    double currentTime = 0;

    for(int idxType = 0 ; idxType < nbWorkerTypes ; ++idxType){
        bool noMoreTypeTask = false;
        for(int idxTypeAv = 0 ; !noMoreTypeTask && idxTypeAv < idleWorkerCount[idxType].size() ; ++idxTypeAv){
            noMoreTypeTask = true;
            for(int idxPrio = 0 ; idxPrio < nbBuckets ; ++idxPrio){
                const int bucketId = priorities[idxType][idxPrio];
                if(buckets[bucketId].size()){
                    Worker wk{currentTime + costs[idxType][bucketId],
                                                            buckets[bucketId].back(), idxType,
                                                            idleWorkerCount[idxType].back()};
                    workers.push(wk);
                    inAllTasks[buckets[bucketId].back()].setStartingTime(0);
                    inAllTasks[buckets[bucketId].back()].setThreadIdComputer(idleWorkerCount[idxType].back());
                    idleWorkerCount[idxType].pop_back();
                    buckets[bucketId].pop_back();
                    noMoreTypeTask = false;
                    nbComputedTask += 1;
                    if(executionStatistics != NULL)
                    {
                        executionStatistics->busyTime[idxType] += costs[idxType][bucketId];
                        if(idxType == 0)
                        {
                            executionStatistics->taskCPUGPUproportion[bucketId].first += 1.0f;
                        }
                        else
                        {
                            executionStatistics->taskCPUGPUproportion[bucketId].second += 1.0f;
                        }
                    }
                    break;
                }
            }
        }
    }

    assert(workers.size() != 0);

    while(nbComputedTask != inAllTasks.size()){
        {
            assert(workers.size());
            Worker worker = workers.top();
            workers.pop();

            const int currentTaskId = worker.currentTaskId;
            assert(currentTime <= worker.scheduledAvailable);
            currentTime = worker.scheduledAvailable;
            inAllTasks[currentTaskId].setEndingTime(currentTime);

            // release task
            assert(worker.currentTaskId != -1);
            {
                const std::vector<int>& successorsIds = inAllTasks[currentTaskId].getSuccessorDependencies();
                for(const int successorId : successorsIds){
                    countPredecessorsOver[successorId] += 1;
                    assert(successorId == inAllTasks[successorId].getId());
                    if(countPredecessorsOver[successorId] == inAllTasks[successorId].getPredecessorDependencies().size()){
                        buckets[inAllTasks[successorId].getType()].push_back(successorId);
                        inAllTasks[successorId].setReadyTime(currentTime);
                    }
                }
            }

            idleWorkerCount[worker.type].push_back(worker.currentWorkerId);
        }

        for(int idxType = 0 ; idxType < nbWorkerTypes ; ++idxType){
            bool noMoreTypeTask = false;
            for(int idxTypeAv = 0 ; !noMoreTypeTask && idxTypeAv < idleWorkerCount[idxType].size() ; ++idxTypeAv){
                noMoreTypeTask = true;
                for(int idxPrio = 0 ; idxPrio < nbBuckets ; ++idxPrio){
                    const int bucketId = priorities[idxType][idxPrio];
                    if(buckets[bucketId].size()){
                        workers.push(Worker{currentTime + costs[idxType][bucketId],
                                            buckets[bucketId].back(), idxType,
                                            idleWorkerCount[idxType].back()});
                        inAllTasks[buckets[bucketId].back()].setStartingTime(currentTime);
                        inAllTasks[buckets[bucketId].back()].setThreadIdComputer(idleWorkerCount[idxType].back());
                        buckets[bucketId].pop_back();
                        idleWorkerCount[idxType].pop_back();
                        noMoreTypeTask = false;
                        nbComputedTask += 1;
                        if(executionStatistics != NULL)
                        {
                            executionStatistics->busyTime[idxType] += costs[idxType][bucketId];
                            if(idxType == 0)
                            {
                                executionStatistics->taskCPUGPUproportion[bucketId].first += 1.0f;
                            }
                            else
                            {
                                executionStatistics->taskCPUGPUproportion[bucketId].second += 1.0f;
                            }
                        }
                        break;
                    }
                }
            }
        }

        assert(workers.size() != 0);
    }

    assert(workers.size() != 0);
    while(workers.size() != 1){
        Worker worker = workers.top();
        assert(currentTime <= worker.scheduledAvailable);
        currentTime = worker.scheduledAvailable;
        inAllTasks[worker.currentTaskId].setEndingTime(currentTime);
        workers.pop();
    }

    // execution data

    if(executionStatistics != NULL)
    {
        for(int taskType = 0 ; taskType < nbBuckets ; ++taskType)
        {
            double sum = executionStatistics->taskCPUGPUproportion[taskType].first + executionStatistics->taskCPUGPUproportion[taskType].second;
            if(sum > 0.0f)
            {
                executionStatistics->taskCPUGPUproportion[taskType].first /= sum;
                executionStatistics->taskCPUGPUproportion[taskType].second /= sum;
            }
        }
    }



    Worker worker = workers.top();
    assert(currentTime <= worker.scheduledAvailable);
    currentTime = worker.scheduledAvailable;
    inAllTasks[worker.currentTaskId].setEndingTime(currentTime);

    return workers.top().scheduledAvailable;
}

double LowerBound(std::vector<EmulTask>& inAllTasks,
               const std::vector<double>& inCpuCost,
               const std::vector<double>& inGpuCost,
               const std::vector<int>& inPriorityCpu,
               const std::vector<int>& inPriorityGpu,
               const int nbCpus,
               const int nbGpus){
    std::vector<int> ntaskOfType = inPriorityCpu; // to have a vector of the right size

    for(unsigned int t=0;t<ntaskOfType.size();++t)
    {
        ntaskOfType[t] = 0;
    }
    for(unsigned int t=0;t<inAllTasks.size();++t)
    {
        ++ntaskOfType[inAllTasks[t].getType()];
    }

    std::vector<std::pair<int, float>> gpuAcceleration;
    for(unsigned int t=0;t<inAllTasks.size();++t)
    {
        std::pair<int, float> elt(t, inGpuCost[inAllTasks[t].getType()] / inCpuCost[inAllTasks[t].getType()]);
        gpuAcceleration.push_back(elt);
    }

    std::sort (gpuAcceleration.begin(), gpuAcceleration.end(), 
        [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
        {
            return elt1.second > elt2.second;
        });

    int left = 0, right = gpuAcceleration.size()-1;
    float cpuTime = 0.f, gpuTime = 0.f;
    while(left <= right)
    {
        cpuTime += inCpuCost[inAllTasks[gpuAcceleration[left].first].getType()]/(float)nbCpus;
        ++left;

        for(;left<right && gpuTime < cpuTime;--right)
        {
            gpuTime += inGpuCost[inAllTasks[gpuAcceleration[right].first].getType()]/(float)nbGpus;
        }
    }

    for(int t=left-1;t>=0&&gpuAcceleration[t].second>1.0;--t)
    {
        float newCpuTime = cpuTime-inCpuCost[inAllTasks[gpuAcceleration[t].first].getType()]/(float)nbCpus;
        float newGpuTime = gpuTime+inGpuCost[inAllTasks[gpuAcceleration[t].first].getType()]/(float)nbGpus;
        if(std::max(newCpuTime, newGpuTime) <= std::max(cpuTime, gpuTime))
        {
            cpuTime = newCpuTime;
            gpuTime = newGpuTime;
        }
        else
        {
            break;
        }
    }

    /*float time = 0.f;
    for(unsigned int t=0;t<inAllTasks.size();++t)
    {
        time += std::min(inCpuCost[inAllTasks[t].getType()], inGpuCost[inAllTasks[t].getType()]);
    }*/


    float v1 = std::max(cpuTime, gpuTime);
    //float v2 = std::max(cpuTime+inCpuCost[inAllTasks[gpuAcceleration[left].first].getType()], gpuTime-inGpuCost[inAllTasks[gpuAcceleration[left].first].getType()]);
    //float v3 = std::max(cpuTime-inCpuCost[inAllTasks[gpuAcceleration[left-1].first].getType()], gpuTime+inGpuCost[inAllTasks[gpuAcceleration[left-1].first].getType()]);

    return v1;
}

std::vector<std::vector<int>> Explorer(std::vector<EmulTask>& allTasks){

    std::vector<std::vector<int>> typeTasks;
    bool alreadyInVector = false;
    size_t i;
    size_t j;
    //On explore tout le tableau contant les tâches
    for(i = 0; i < allTasks.size(); ++i){
        //On explore le tableau contenant les types de tâches déjà trouvé
        for(j = 0; j < typeTasks.size(); ++j){
            //Si le type de la tâche sur laquelle nous somme actuellement est déjà dans le tableau
            //On incrémente le nombre de ce type trouvé dans le graphe de tâche
            //et le nombre de successeur total de ce type
            if(typeTasks[j][0] == allTasks[i].getType()){
                alreadyInVector = true;
                typeTasks[j][1] = typeTasks[j][1] + 1;
                typeTasks[j][2] = typeTasks[j][2] + allTasks[i].getSuccessorDependencies().size();
            }
        }

        //Sinon on initialise une nouvelle partie du tableau pour ce type
        if(alreadyInVector == false){
            typeTasks.push_back(std::vector<int>(3));
            int indexElem = typeTasks.size() - 1;
            typeTasks[indexElem][0] = allTasks[i].getType();
            typeTasks[indexElem][1] = 1;
            typeTasks[indexElem][2] = allTasks[i].getSuccessorDependencies().size();
        }
        else{
            alreadyInVector = false;
        }
    }

    return typeTasks;
}

bool CompareRatio(std::vector<int> i1, std::vector<int> i2)
{
    return (i1[1] > i2[1]);
}


std::vector<int> FindCpuPriority(const std::vector<double> cpuCost, std::vector<std::vector<int>> typeTasks){
    std::vector<std::vector<int>> ratio;
    std::vector<int> priority;

    for(size_t i = 0; i < typeTasks.size(); ++i){
        ratio.push_back(std::vector<int>(2));
        ratio[i][0] = typeTasks[i][0];
        ratio[i][1] = static_cast<int>(typeTasks[i][2]/cpuCost[i]);
        std::cout << "Tache " << typeTasks[i][0] << " : ratio :  " << ratio[i][1] << std::endl;
    }

    sort(ratio.begin(), ratio.end(), CompareRatio);

    for(size_t i = 0; i < typeTasks.size(); ++i){
        for(size_t j = 0; j < ratio.size(); ++j){
            if(typeTasks[i][0] == ratio[j][0]){
                priority.push_back(j);
            }
        }
    }

    for(size_t i = 0; i < priority.size(); ++i){
        std::cout << "Priority " << priority[i] << std::endl;
    }

    return priority;
}

std::vector<int> FindGpuPriority(const std::vector<double> gpuCost, std::vector<std::vector<int>> typeTasks){
    std::vector<std::vector<int>> ratio;
    std::vector<int> priority;

    for(size_t i = 0; i < typeTasks.size(); ++i){
        ratio.push_back(std::vector<int>(2));
        ratio[i][0] = typeTasks[i][0];
        ratio[i][1] = static_cast<int>(typeTasks[i][2]/gpuCost[i]);
        std::cout << "Tache " << typeTasks[i][0] << " : ratio :  " << ratio[i][1] << std::endl;
    }

    sort(ratio.begin(), ratio.end(), CompareRatio);

    for(size_t i = 0; i < typeTasks.size(); ++i){
        for(size_t j = 0; j < ratio.size(); ++j){
            if(typeTasks[i][0] == ratio[j][0]){
                priority.push_back(j);
            }
        }
    }

    for(size_t i = 0; i < priority.size(); ++i){
        std::cout << "Priority " << priority[i] << std::endl;
    }

    return priority;
}

struct ExecutionData
{
    int seed;
    int taskNumber;
    std::vector<double> cpuCost;
    std::vector<double> gpuCost;
    int NbCpus;
    int NbGpus;
    std::map<int, std::vector<int>> predecessorRule;
    std::vector<float> taskTypeProportion;
};


ExecutionData loadData(int index)
{
    ExecutionData executionData;

    switch(index)
    {
        case 0:
            executionData.seed = 1585584545;
            executionData.taskNumber = 1990;
            executionData.cpuCost = std::vector<double>{7.9997, 0.3167, 0.0721, 3.9945, 0.1519, 5.1758, 0.4883};
            executionData.gpuCost = std::vector<double>{7.9997, 4.6560, 0.1885, 4.6088, 4.5752, 26.5682, 3.7514};
            executionData.NbCpus = 4;
            executionData.NbGpus = 14;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 2, 0, 3, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{1, 0, 0, 0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 1, 0, 0, 1, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 3, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{1, 0, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{1, 0, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{2, 0, 0, 2, 0, 1, 2}));
            executionData.taskTypeProportion = std::vector<float>{0.2433, 0.0328, 0.2050, 0.1352, 0.1567, 0.0657, 0.1613};
            break;


        case 1:
            executionData.seed = 1108905499;
            executionData.taskNumber = 1511;
            executionData.cpuCost = std::vector<double>{0.4540, 8.1599, 0.2142, 6.1952};
            executionData.gpuCost = std::vector<double>{7.9693, 8.1599, 2.5192, 0.1294};
            executionData.NbCpus = 13;
            executionData.NbGpus = 8;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 1, 0, 2}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{2, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 2, 1}));
            executionData.taskTypeProportion = std::vector<float>{0.2360, 0.3118, 0.2848, 0.1675};
            break;


        case 2:
            executionData.seed = 619709845;
            executionData.taskNumber = 3016;
            executionData.cpuCost = std::vector<double>{0.7259, 0.7298, 0.2171, 1.4518, 4.9606, 0.4208, 0.2747, 6.4445};
            executionData.gpuCost = std::vector<double>{6.5858, 0.7298, 0.3424, 5.1154, 3.2830, 3.5007, 0.3372, 10.7127};
            executionData.NbCpus = 11;
            executionData.NbGpus = 12;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 2, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 0, 0, 1, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 0, 0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{0, 0, 2, 0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{0, 0, 1, 0, 2, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{0, 0, 0, 1, 0, 0, 1, 1}));
            executionData.predecessorRule.insert(std::make_pair(7, std::vector<int>{0, 0, 0, 0, 2, 0, 1, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.3311, 0.0355, 0.2015, 0.0150, 0.0010, 0.0435, 0.1820, 0.1903};
            break;


        case 3:
            executionData.seed = 1403537636;
            executionData.taskNumber = 3026;
            executionData.cpuCost = std::vector<double>{3.2310, 3.9197, 5.7388, 0.4676, 0.4214, 10.6566, 4.6996, 1.5746};
            executionData.gpuCost = std::vector<double>{3.2310, 0.2367, 2.0700, 2.1676, 0.3498, 5.0946, 0.0145, 1.0320};
            executionData.NbCpus = 2;
            executionData.NbGpus = 7;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 1, 0, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{2, 0, 0, 0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 1, 1, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{1, 0, 0, 0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{0, 0, 0, 2, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{0, 1, 0, 1, 0, 0, 2, 0}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{1, 2, 0, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(7, std::vector<int>{0, 0, 0, 0, 0, 0, 2, 1}));
            executionData.taskTypeProportion = std::vector<float>{0.1691, 0.1388, 0.0161, 0.1124, 0.0553, 0.1303, 0.1846, 0.1935};
            break;


        case 4:
            executionData.seed = 162039621;
            executionData.taskNumber = 3052;
            executionData.cpuCost = std::vector<double>{0.1227, 0.4329, 4.2364, 0.4770, 0.2878, 0.5875, 0.0420, 3.8564};
            executionData.gpuCost = std::vector<double>{0.2409, 3.0461, 1.8359, 9.2555, 0.2878, 1.0105, 0.6001, 11.9827};
            executionData.NbCpus = 13;
            executionData.NbGpus = 15;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 1, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 0, 1, 0, 2, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 0, 0, 0, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 0, 0, 0, 4, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{0, 0, 1, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{0, 2, 1, 1, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{2, 0, 0, 0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(7, std::vector<int>{0, 0, 0, 0, 0, 0, 1, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.1182, 0.1976, 0.0209, 0.0214, 0.2416, 0.2045, 0.0600, 0.1357};
            break;


        case 5:
            executionData.seed = 1073114970;
            executionData.taskNumber = 2291;
            executionData.cpuCost = std::vector<double>{0.4792, 9.1602, 4.6768, 0.3834, 0.5283, 3.9562, 3.4203, 5.6410};
            executionData.gpuCost = std::vector<double>{0.6641, 1.6260, 1.3017, 0.0289, 2.3933, 3.9562, 3.4586, 0.1874};
            executionData.NbCpus = 9;
            executionData.NbGpus = 7;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{2, 0, 0, 0, 1, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 1, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{1, 0, 0, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 0, 0, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{2, 0, 0, 0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{0, 0, 1, 0, 0, 0, 2, 0}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{0, 0, 0, 2, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(7, std::vector<int>{0, 1, 0, 0, 1, 0, 0, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.1099, 0.0488, 0.1504, 0.1569, 0.0975, 0.1055, 0.0010, 0.3300};
            break;


        case 6:
            executionData.seed = 2035877476;
            executionData.taskNumber = 2186;
            executionData.cpuCost = std::vector<double>{0.6865, 3.6791, 6.5806, 7.5323, 18.4833, 2.5956, 0.1707};
            executionData.gpuCost = std::vector<double>{1.2521, 3.0839, 3.2349, 2.6291, 0.6582, 2.5956, 0.3520};
            executionData.NbCpus = 13;
            executionData.NbGpus = 3;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1, 0, 2, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{1, 0, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 0, 1, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 0, 0, 0, 0, 3}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{0, 0, 0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{0, 0, 0, 0, 0, 1, 1}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{0, 0, 0, 0, 2, 0, 1}));
            executionData.taskTypeProportion = std::vector<float>{0.3395, 0.0307, 0.1548, 0.2094, 0.0475, 0.1299, 0.0883};
            break;


        case 7:
            executionData.seed = 672129145;
            executionData.taskNumber = 2859;
            executionData.cpuCost = std::vector<double>{0.7784, 1.2362, 3.1642, 0.2038, 0.8441, 10.5615, 0.9668, 4.0176};
            executionData.gpuCost = std::vector<double>{0.5186, 3.9693, 3.1642, 0.7362, 0.1101, 0.6892, 0.3181, 5.2401};
            executionData.NbCpus = 11;
            executionData.NbGpus = 1;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 0, 1, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 0, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 0, 0, 0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 0, 0, 0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{1, 0, 2, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{0, 0, 0, 0, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{0, 0, 0, 1, 0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(7, std::vector<int>{0, 0, 1, 1, 0, 0, 0, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.0181, 0.0241, 0.1773, 0.0010, 0.2476, 0.1007, 0.3210, 0.1102};
            break;


        case 8:
            executionData.seed = 647806145;
            executionData.taskNumber = 1647;
            executionData.cpuCost = std::vector<double>{4.9083, 2.2065, 0.4631, 17.7487, 0.1828};
            executionData.gpuCost = std::vector<double>{0.9795, 0.2454, 2.9795, 17.7487, 2.3989};
            executionData.NbCpus = 9;
            executionData.NbGpus = 12;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{1, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 0, 1, 1}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{0, 2, 0, 0, 1}));
            executionData.taskTypeProportion = std::vector<float>{0.0813, 0.4051, 0.0555, 0.2443, 0.2139};
            break;


        case 9:
            executionData.seed = 965843579;
            executionData.taskNumber = 2744;
            executionData.cpuCost = std::vector<double>{0.0469, 3.1510, 1.6902, 4.5587, 5.0793, 5.4966, 5.3729};
            executionData.gpuCost = std::vector<double>{0.6680, 6.1476, 2.7832, 4.5587, 7.1656, 4.2228, 2.2973};
            executionData.NbCpus = 12;
            executionData.NbGpus = 1;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 1, 0, 0, 2, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 1, 0, 0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 1, 2, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{1, 0, 1, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{0, 1, 2, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{2, 0, 0, 0, 0, 1, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.0417, 0.3603, 0.0276, 0.3906, 0.1432, 0.0350, 0.0016};
            break;


        case 10:
            executionData.seed = 598367304;
            executionData.taskNumber = 2269;
            executionData.cpuCost = std::vector<double>{3.4590, 5.1305, 1.4114, 2.3887};
            executionData.gpuCost = std::vector<double>{4.5025, 0.6871, 1.4114, 4.4223};
            executionData.NbCpus = 2;
            executionData.NbGpus = 9;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 1, 0, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.1305, 0.2301, 0.2385, 0.4008};
            break;


        case 11:
            executionData.seed = 664430851;
            executionData.taskNumber = 1637;
            executionData.cpuCost = std::vector<double>{5.7026, 3.3522, 2.1076, 0.1892, 1.4782};
            executionData.gpuCost = std::vector<double>{0.0758, 3.3522, 1.2727, 2.3636, 4.6409};
            executionData.NbCpus = 13;
            executionData.NbGpus = 10;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{1, 0, 0, 0, 2}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{1, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{1, 0, 0, 0, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.3068, 0.1562, 0.2100, 0.1259, 0.2011};
            break;


        case 12:
            executionData.seed = 2079249305;
            executionData.taskNumber = 2582;
            executionData.cpuCost = std::vector<double>{7.7898, 6.4842, 7.1381, 0.5769, 3.8388, 0.1535};
            executionData.gpuCost = std::vector<double>{7.7898, 0.5250, 0.6843, 1.5484, 3.2646, 3.8696};
            executionData.NbCpus = 4;
            executionData.NbGpus = 6;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 0, 0, 1, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 0, 2, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{0, 0, 0, 2, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{1, 0, 0, 0, 0, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.2086, 0.1127, 0.1347, 0.2679, 0.0169, 0.2591};
            break;


        case 13:
            executionData.seed = 1172246724;
            executionData.taskNumber = 1178;
            executionData.cpuCost = std::vector<double>{0.1583, 0.3926, 1.3090, 4.8433, 3.8867};
            executionData.gpuCost = std::vector<double>{0.4828, 0.1975, 0.2929, 4.8433, 6.0516};
            executionData.NbCpus = 4;
            executionData.NbGpus = 11;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 2, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{1, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{1, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{0, 0, 0, 0, 1}));
            executionData.taskTypeProportion = std::vector<float>{0.2982, 0.1414, 0.1685, 0.0412, 0.3507};
            break;


        case 14:
            executionData.seed = 1330723520;
            executionData.taskNumber = 1650;
            executionData.cpuCost = std::vector<double>{3.7614, 3.6335, 7.2191, 1.3154};
            executionData.gpuCost = std::vector<double>{3.7614, 3.5862, 3.3771, 10.7792};
            executionData.NbCpus = 8;
            executionData.NbGpus = 1;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 1, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 2, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 0, 2}));
            executionData.taskTypeProportion = std::vector<float>{0.2020, 0.0461, 0.5185, 0.2333};
            break;


        case 15:
            executionData.seed = 10313957;
            executionData.taskNumber = 2358;
            executionData.cpuCost = std::vector<double>{2.8554, 5.2307, 4.7123, 7.2150, 23.1900};
            executionData.gpuCost = std::vector<double>{10.8526, 1.2763, 4.7123, 0.3364, 0.6652};
            executionData.NbCpus = 3;
            executionData.NbGpus = 8;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 2, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{2, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{3, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{2, 0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{0, 0, 0, 0, 1}));
            executionData.taskTypeProportion = std::vector<float>{0.0585, 0.1936, 0.3056, 0.2848, 0.1575};
            break;


        case 16:
            executionData.seed = 155289362;
            executionData.taskNumber = 1936;
            executionData.cpuCost = std::vector<double>{6.3160, 0.4964, 0.5479, 0.0556};
            executionData.gpuCost = std::vector<double>{6.3160, 1.4354, 1.2955, 17.4974};
            executionData.NbCpus = 15;
            executionData.NbGpus = 3;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 1, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 2, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.2941, 0.3066, 0.2733, 0.1260};
            break;


        case 17:
            executionData.seed = 1613317276;
            executionData.taskNumber = 2363;
            executionData.cpuCost = std::vector<double>{3.5165, 9.4330, 8.2335, 0.1805, 6.9005, 7.7065, 0.2614};
            executionData.gpuCost = std::vector<double>{2.5169, 0.2379, 0.7424, 1.0433, 0.1060, 7.7065, 4.5497};
            executionData.NbCpus = 10;
            executionData.NbGpus = 1;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 1, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{1, 0, 2, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{2, 0, 0, 0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{1, 0, 0, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{0, 0, 0, 0, 1, 0, 1}));
            executionData.taskTypeProportion = std::vector<float>{0.2077, 0.0085, 0.0745, 0.2692, 0.2178, 0.2214, 0.0010};
            break;


        case 18:
            executionData.seed = 665880930;
            executionData.taskNumber = 1372;
            executionData.cpuCost = std::vector<double>{4.6159, 4.5160, 0.9124, 2.5351, 3.4847, 3.2692};
            executionData.gpuCost = std::vector<double>{0.4525, 0.1948, 6.9087, 0.0171, 4.8381, 3.2692};
            executionData.NbCpus = 7;
            executionData.NbGpus = 3;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 1, 0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 1, 0, 0, 1, 2}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{1, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{0, 2, 1, 0, 0, 1}));
            executionData.taskTypeProportion = std::vector<float>{0.0333, 0.1488, 0.1716, 0.0200, 0.5456, 0.0808};
            break;


        case 19:
            executionData.seed = 678690410;
            executionData.taskNumber = 1342;
            executionData.cpuCost = std::vector<double>{4.4050, 0.7329, 3.8748, 3.7776, 0.4886, 0.5967, 1.8546, 0.6919};
            executionData.gpuCost = std::vector<double>{0.6433, 2.2310, 1.8265, 0.3385, 8.0022, 0.0922, 1.8546, 4.3283};
            executionData.NbCpus = 9;
            executionData.NbGpus = 14;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1, 1, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 1, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 0, 0, 3, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 1, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{0, 0, 0, 0, 0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{2, 0, 0, 0, 2, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{1, 0, 0, 0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(7, std::vector<int>{0, 0, 0, 1, 0, 0, 0, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.1520, 0.0611, 0.0654, 0.2482, 0.0051, 0.2628, 0.0718, 0.1336};
            break;


        case 20:
            executionData.seed = 853392284;
            executionData.taskNumber = 2260;
            executionData.cpuCost = std::vector<double>{0.6070, 3.1080, 0.4769, 4.6263};
            executionData.gpuCost = std::vector<double>{0.2319, 0.4609, 6.4702, 4.6263};
            executionData.NbCpus = 8;
            executionData.NbGpus = 11;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 1, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{1, 0, 0, 2}));
            executionData.taskTypeProportion = std::vector<float>{0.1627, 0.1794, 0.3537, 0.3043};
            break;


        case 21:
            executionData.seed = 1876117457;
            executionData.taskNumber = 2490;
            executionData.cpuCost = std::vector<double>{7.1197, 6.9433, 0.5134, 4.8090, 4.2094, 13.2994, 3.7994};
            executionData.gpuCost = std::vector<double>{6.6265, 10.6067, 0.5134, 0.3838, 14.9666, 3.5832, 1.3479};
            executionData.NbCpus = 8;
            executionData.NbGpus = 8;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 1, 1, 0, 0, 2}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 1, 0, 0, 2, 2, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 0, 0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 0, 0, 0, 1, 2}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{1, 0, 0, 0, 2, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{0, 0, 0, 0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{2, 0, 0, 0, 0, 0, 1}));
            executionData.taskTypeProportion = std::vector<float>{0.3473, 0.1516, 0.1073, 0.1734, 0.1534, 0.0542, 0.0129};
            break;


        case 22:
            executionData.seed = 1754564385;
            executionData.taskNumber = 2651;
            executionData.cpuCost = std::vector<double>{3.2223, 2.4474, 0.4500, 4.3173, 3.6570, 2.0142};
            executionData.gpuCost = std::vector<double>{0.4221, 8.9833, 10.3568, 0.0116, 3.6570, 1.9108};
            executionData.NbCpus = 15;
            executionData.NbGpus = 9;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 1, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{2, 0, 0, 1, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{1, 0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{1, 0, 0, 0, 2, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{1, 0, 0, 0, 0, 1}));
            executionData.taskTypeProportion = std::vector<float>{0.1809, 0.2783, 0.1350, 0.1636, 0.1777, 0.0645};
            break;


        case 23:
            executionData.seed = 433164096;
            executionData.taskNumber = 1991;
            executionData.cpuCost = std::vector<double>{3.3189, 6.8861, 0.2271, 0.0145, 3.7807};
            executionData.gpuCost = std::vector<double>{2.7026, 6.8861, 4.3214, 2.5356, 0.4912};
            executionData.NbCpus = 14;
            executionData.NbGpus = 4;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 1, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{1, 0, 0, 1, 1}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{1, 2, 0, 0, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.3206, 0.0040, 0.2179, 0.2147, 0.2429};
            break;


        case 24:
            executionData.seed = 2058928080;
            executionData.taskNumber = 1762;
            executionData.cpuCost = std::vector<double>{6.9518, 20.9833, 3.8896, 3.3031, 7.4160, 10.6913, 5.6915};
            executionData.gpuCost = std::vector<double>{2.4346, 9.6519, 0.5392, 1.8259, 10.7834, 6.2214, 5.6915};
            executionData.NbCpus = 1;
            executionData.NbGpus = 2;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 0, 0, 2, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{1, 0, 0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{0, 2, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{0, 2, 0, 0, 0, 1, 2}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{0, 0, 0, 0, 1, 0, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.0915, 0.1422, 0.1926, 0.3248, 0.0586, 0.1238, 0.0665};
            break;


        case 25:
            executionData.seed = 1313314786;
            executionData.taskNumber = 1455;
            executionData.cpuCost = std::vector<double>{0.7095, 0.2628, 4.0417, 0.7396};
            executionData.gpuCost = std::vector<double>{3.3851, 0.5770, 4.0417, 0.3096};
            executionData.NbCpus = 9;
            executionData.NbGpus = 11;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{1, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 2, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{3, 0, 0, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.3694, 0.2638, 0.2445, 0.1223};
            break;


        case 26:
            executionData.seed = 2013732168;
            executionData.taskNumber = 2578;
            executionData.cpuCost = std::vector<double>{0.4822, 1.3934, 0.0062, 5.0164, 0.1511, 4.6980, 6.9400, 9.7760};
            executionData.gpuCost = std::vector<double>{2.6779, 5.2178, 4.8536, 5.1683, 1.2208, 3.0041, 0.3198, 9.7760};
            executionData.NbCpus = 4;
            executionData.NbGpus = 14;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1, 1, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 0, 0, 0, 1, 1}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 0, 0, 0, 0, 2, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 0, 0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{0, 1, 0, 0, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{1, 1, 1, 0, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{0, 0, 0, 0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(7, std::vector<int>{0, 1, 0, 0, 0, 0, 0, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.2152, 0.0594, 0.2432, 0.1439, 0.0692, 0.1194, 0.0724, 0.0773};
            break;


        case 27:
            executionData.seed = 9946002;
            executionData.taskNumber = 2369;
            executionData.cpuCost = std::vector<double>{8.1968, 0.4815, 1.9553, 0.4811, 14.1625};
            executionData.gpuCost = std::vector<double>{0.9856, 0.0037, 0.0505, 0.2908, 14.1625};
            executionData.NbCpus = 15;
            executionData.NbGpus = 1;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1, 1}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{0, 0, 1, 1, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.3292, 0.0670, 0.3017, 0.1296, 0.1725};
            break;


        case 28:
            executionData.seed = 462549484;
            executionData.taskNumber = 3041;
            executionData.cpuCost = std::vector<double>{3.4741, 1.0386, 4.9667, 3.2192, 6.9261, 4.3706, 6.0137};
            executionData.gpuCost = std::vector<double>{0.2441, 0.3364, 1.4324, 4.6792, 6.9261, 0.1777, 4.0848};
            executionData.NbCpus = 9;
            executionData.NbGpus = 3;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 1, 2, 2, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 0, 1, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 1, 0, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{0, 0, 0, 0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{0, 2, 1, 0, 0, 2, 0}));
            executionData.predecessorRule.insert(std::make_pair(6, std::vector<int>{0, 0, 0, 0, 0, 0, 2}));
            executionData.taskTypeProportion = std::vector<float>{0.2551, 0.0584, 0.1903, 0.0041, 0.1705, 0.0759, 0.2456};
            break;


        case 29:
            executionData.seed = 378397775;
            executionData.taskNumber = 1869;
            executionData.cpuCost = std::vector<double>{0.8377, 7.4176, 1.0384, 0.2368, 5.6281, 14.5476};
            executionData.gpuCost = std::vector<double>{0.3848, 4.3347, 4.4650, 6.3348, 5.6281, 0.3735};
            executionData.NbCpus = 15;
            executionData.NbGpus = 8;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{2, 0, 2, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 0, 0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 2, 0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{2, 0, 1, 0, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(4, std::vector<int>{1, 0, 2, 2, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(5, std::vector<int>{0, 1, 2, 1, 0, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.0652, 0.1989, 0.2753, 0.1323, 0.1410, 0.1874};
            break;


        case 30:
            executionData.seed = 1505528737;
            executionData.taskNumber = 1586;
            executionData.cpuCost = std::vector<double>{2.1308, 19.0977, 0.4823, 5.3117};
            executionData.gpuCost = std::vector<double>{2.1308, 10.8828, 0.2737, 6.2676};
            executionData.NbCpus = 10;
            executionData.NbGpus = 10;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 1, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 1, 2, 4}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 0, 1, 0}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 1, 1, 0}));
            executionData.taskTypeProportion = std::vector<float>{0.2190, 0.2586, 0.2561, 0.2663};
            break;


        case 31:
            executionData.seed = 535245652;
            executionData.taskNumber = 1118;
            executionData.cpuCost = std::vector<double>{0.0079, 1.5374, 1.8738, 0.2511};
            executionData.gpuCost = std::vector<double>{0.2521, 1.5374, 0.0447, 5.3940};
            executionData.NbCpus = 12;
            executionData.NbGpus = 13;
            executionData.predecessorRule.clear();
            executionData.predecessorRule.insert(std::make_pair(0, std::vector<int>{0, 0, 0, 1}));
            executionData.predecessorRule.insert(std::make_pair(1, std::vector<int>{0, 1, 0, 0}));
            executionData.predecessorRule.insert(std::make_pair(2, std::vector<int>{0, 0, 0, 3}));
            executionData.predecessorRule.insert(std::make_pair(3, std::vector<int>{0, 0, 1, 1}));
            executionData.taskTypeProportion = std::vector<float>{0.1785, 0.3122, 0.3023, 0.2070};
            break;


        default:
            std::cerr << "Unknown index " << index << std::endl;
    }

    return executionData;
}

double getBestExecutionTime(int index)
{
    switch(index)
    {
        case 0:
            return 687.0002;
        case 1:
            return 307.5651;
        case 2:
            return 143.8225;
        case 3:
            return 781.8828;
        case 4:
            return 127.6888;
        case 5:
            return 289.9688;
        case 6:
            return 292.1597;
        case 7:
            return 504.4466;
        case 8:
            return 644.5197;
        case 9:
            return 938.3755;
        case 10:
            return 203.9088;
        case 11:
            return 171.4163;
        case 12:
            return 672.3556;
        case 13:
            return 36.4017;
        case 14:
            return 560.3995;
        case 15:
            return 408.8380;
        case 16:
            return 241.7107;
        case 17:
            return 954.2675;
        case 18:
            return 319.2704;
        case 19:
            return 90.4398;
        case 20:
            return 318.4874;
        case 21:
            return 642.1103;
        case 22:
            return 358.7427;
        case 23:
            return 51.3131;
        case 24:
            return 3093.1644;
        case 25:
            return 150.0370;
        case 26:
            return 637.3467;
        case 27:
            return 1004.9473;
        case 28:
            return 1282.0930;
        case 29:
            return 575.5150;
        case 30:
            return 510.7655;
        case 31:
            return 81.6084;
        default:
            std::cerr << "Unknown index " << index << std::endl;
    }
    return 9999999999;
}


std::vector<float> getRandomTaskProportion(int nbTaskType)
{
    std::vector<float> proportion;
    for(int i=0;i<nbTaskType;++i)
    {
        proportion.push_back(1.0f/(float)nbTaskType);
    }

    for(int i=0;i<nbTaskType*256;++i)
    {
        int from = rand()%nbTaskType;
        int to = rand()%nbTaskType;
        float qty = std::max(std::min((-0.001f+(float)proportion[from]), std::min((float)(1.001f-proportion[to]), (float)(randf01()*0.01))), 0.0f);
        proportion[from] -= qty;
        proportion[to] += qty;
    }

    return proportion;
}

std::vector<double> getRandomCosts(int nbTaskType)
{
    std::vector<double> cost;
    for(double i=0;i<nbTaskType;++i)
    {
        float value = 1.0f+randf01()*4.0f;
        if(rand()%10<4)
        {
            value *= 0.1;
        }
        if(rand()%10<1)
        {
            value *= 0.01;
        }
        if(rand()%10<5)
        {
            value *= 1.5;
        }
        if(rand()%10<4)
        {
            value *= 1.5;
        }
        if(rand()%10<2)
        {
            value *= 2.5;
        }
        cost.push_back(value);
    }

    return cost;
}

int getRandomPUnum(void)
{
    return 1+rand()%15;
}

std::map<int, std::vector<int>> getRandomPredecessorRule(int nbTaskType)
{
    std::map<int, std::vector<int>> predecessorRule;
    for(double i=0;i<nbTaskType;++i)
    {
        std::vector<int> expectedPredecessors;
        expectedPredecessors.clear();

        for(double i=0;i<nbTaskType;++i)
        {
            expectedPredecessors.push_back(0);
        }

        expectedPredecessors[rand()%nbTaskType] = 1; // at least one predecessor

        for(double i=0;i<nbTaskType;++i)
        {
            if(rand()%16 < 1)
            {
                if(rand()%2 == 0)
                {
                    expectedPredecessors[i] += 1;
                }
                else
                {
                    expectedPredecessors[i] += 2;
                }
            }
            if(rand()%16 < 1)
            {
                if(rand()%2 == 0)
                {
                    expectedPredecessors[i] += 1;
                }
                else
                {
                    expectedPredecessors[i] += 2;
                }
            }
        }

        predecessorRule.insert(std::make_pair(i, expectedPredecessors));
    }
    return predecessorRule;
}

void makeAtLeastOneCostBetterForEachArch(std::vector<double> &CPUcosts, std::vector<double> &GPUcosts)
{ // if all tasks are faster on one arch, change one task so that it becomes faster on the other arch
    bool allSuperior = true, allInferior = true;
    for(int i=0;i<CPUcosts.size();++i)
    {
        if(CPUcosts[i] < GPUcosts[i])
        {
            allSuperior = false;
        }
        if(CPUcosts[i] > GPUcosts[i])
        {
            allInferior = false;
        }
    }
    if(allInferior || allSuperior)
    {
        int index = rand()%CPUcosts.size();
        double val = std::max(CPUcosts[index], GPUcosts[index]);
        CPUcosts[index] = val;
        GPUcosts[index] = val;
    }
}

void printExecutionData(ExecutionData executionData)
{
    std::cout << "Execution data:" << std::endl;
    std::cout << " Seed: " << executionData.seed << std::endl;
    std::cout << " Total task count: " << executionData.taskNumber << std::endl;
    std::cout << " CPU cost:     ";
    for(int i=0;i<executionData.cpuCost.size();++i)
    {
        std::cout << executionData.cpuCost[i] << ", ";
    }
    std::cout << std::endl;
    std::cout << " GPU cost:     ";
    for(int i=0;i<executionData.gpuCost.size();++i)
    {
        std::cout << executionData.gpuCost[i] << ", ";
    }
    std::cout << std::endl;
    std::cout << " CPU number: " << executionData.NbCpus << std::endl;
    std::cout << " GPU number: " << executionData.NbGpus << std::endl;
    std::cout << " Predecessor rule:" << std::endl;
    for(int t=0;t<executionData.taskTypeProportion.size();++t)
    {
        std::cout << " ";
        std::vector<int> v = executionData.predecessorRule[t];
        for(int i=0;i<v.size();++i)
        {
            std::cout << v[i] << ", ";
        }
        std::cout << std::endl;
    }
    std::cout << " Expected proportion: ";
    for(int i=0;i<executionData.taskTypeProportion.size();++i)
    {
        std::cout << executionData.taskTypeProportion[i] << ", ";
    }
    std::cout << std::endl;
}

void printExecutionDataCode(ExecutionData executionData, int i)
{
    std::cout << "case " << i << ":" << std::endl;
    std::cout << "    executionData.seed = " << executionData.seed << ";" << std::endl;
    std::cout << "    executionData.taskNumber = " << executionData.taskNumber << ";" << std::endl;
    std::cout << "    executionData.cpuCost = std::vector<double>{";
    for(int i=0;i<executionData.cpuCost.size();++i)
    {
        std::cout << executionData.cpuCost[i];
        if(i < executionData.cpuCost.size()-1)
        {
            std::cout << ", ";
        }
    }
    std::cout << "};" << std::endl;
    std::cout << "    executionData.gpuCost = std::vector<double>{";
    for(int i=0;i<executionData.gpuCost.size();++i)
    {
        std::cout << executionData.gpuCost[i];
        if(i < executionData.gpuCost.size()-1)
        {
            std::cout << ", ";
        }
    }
    std::cout << "};" << std::endl;
    std::cout << "    executionData.NbCpus = " << executionData.NbCpus << ";" << std::endl;
    std::cout << "    executionData.NbGpus = " << executionData.NbGpus << ";" << std::endl;

    std::cout << "    executionData.predecessorRule.clear();" << std::endl;
    for(int t=0;t<executionData.taskTypeProportion.size();++t)
    {
        std::cout << "    executionData.predecessorRule.insert(std::make_pair(" << t << ", std::vector<int>{";
        std::vector<int> v = executionData.predecessorRule[t];
        for(int i=0;i<v.size();++i)
        {
            std::cout << v[i];
            if(i < v.size()-1)
            {
                std::cout << ", ";
            }
        }
        std::cout << "}));" << std::endl;
    }
    std::cout << "    executionData.taskTypeProportion = std::vector<float>{";
    for(int i=0;i<executionData.taskTypeProportion.size();++i)
    {
        std::cout << executionData.taskTypeProportion[i];
        if(i < executionData.taskTypeProportion.size()-1)
        {
            std::cout << ", ";
        }
    }
    std::cout << "};" << std::endl;
    std::cout << "    break;" << std::endl;
}

template<unsigned degree>
double getNOD(int index, std::vector<EmulTask> &allTasks)
{
    std::vector<int> successors = allTasks[index].getSuccessorDependencies();
    double NOD = 0;

    for(int s=0;s<successors.size();++s)
    {
        NOD += getNOD<degree-1>(successors[s], allTasks)/(double)allTasks[successors[s]].getPredecessorDependencies().size();
    }

    return NOD;
}

template<>
double getNOD<0>(int index, std::vector<EmulTask> &allTasks)
{
    return 1.0f;
}

template<unsigned degree>
double getAverageTaskTypeNOD(int taskType, std::vector<EmulTask> &allTasks)
{
    int taskTypeCount = 0;
    double totalNOD = 0.;
    for(int t=0;t<allTasks.size();++t)
    {
        if(allTasks[t].getType() == taskType)
        {
            ++taskTypeCount;
            totalNOD += getNOD<degree>(t, allTasks);
        }
    }

    if(taskTypeCount <= 0)
    {
        return 0.;
    }

    return totalNOD/(double)taskTypeCount;
}

enum NodeType
{
    CPU, GPU
};

/*
template<unsigned degree, NodeType nt>
double getAugmentedNOD(int index, std::vector<EmulTask> &allTasks, const std::vector<std::pair<double, double>> &taskProportionExecution, const std::vector<double> &cpuCost, const std::vector<double> &gpuCost)
{
    std::vector<int> successors = allTasks[index].getSuccessorDependencies();
    double NOD = 0;

    for(int s=0;s<successors.size();++s)
    {
        //NOD += getAugmentedNOD<0, nt>(successors[s], allTasks, taskProportionExecution, cpuCost, gpuCost);
        if(nt == CPU)
        {
            NOD += getAugmentedNOD<degree-1, nt>(successors[s], allTasks, taskProportionExecution, cpuCost, gpuCost);
        }
        if(nt == GPU)
        {
            NOD += getAugmentedNOD<degree-1, nt>(successors[s], allTasks, taskProportionExecution, cpuCost, gpuCost);
        }
    }

    return NOD;
}

template<>
double getAugmentedNOD<0, CPU>(int index, std::vector<EmulTask> &allTasks, const std::vector<std::pair<double, double>> &taskProportionExecution, const std::vector<double> &cpuCost, const std::vector<double> &gpuCost)
{
    int taskType = allTasks[index].getType();
    return taskProportionExecution[taskType].first*cpuCost[taskType]/(double)allTasks[index].getPredecessorDependencies().size();
}

template<>
double getAugmentedNOD<0, GPU>(int index, std::vector<EmulTask> &allTasks, const std::vector<std::pair<double, double>> &taskProportionExecution, const std::vector<double> &cpuCost, const std::vector<double> &gpuCost)
{
    int taskType = allTasks[index].getType();
    return taskProportionExecution[taskType].second*gpuCost[taskType]/(double)allTasks[index].getPredecessorDependencies().size();
}
*/

template<unsigned degree, NodeType nt>
void getChildrenOccurences(std::map<int, int> &map, int index, std::vector<EmulTask> &allTasks, bool first = true)
{
    if(!first)
    {
        auto it = map.find(index);
        if(it == map.end())
        {
            map.insert(std::make_pair(index, 1));
        }
        else
        {
            ++it->second;
        }
    }

    std::vector<int> successors = allTasks[index].getSuccessorDependencies();

    for(unsigned int s=0;s<successors.size();++s)
    {
        getChildrenOccurences<degree-1, nt>(map, successors[s], allTasks, false);
    }
}

template<>
void getChildrenOccurences<0, CPU>(std::map<int, int> &map, int index, std::vector<EmulTask> &allTasks, bool first)
{
    auto it = map.find(index);
    if(it == map.end())
    {
        map.insert(std::make_pair(index, 1));
    }
    else
    {
        ++it->second;
    }
}

template<>
void getChildrenOccurences<0, GPU>(std::map<int, int> &map, int index, std::vector<EmulTask> &allTasks, bool first)
{
    auto it = map.find(index);
    if(it == map.end())
    {
        map.insert(std::make_pair(index, 1));
    }
    else
    {
        ++it->second;
    }
}

template<unsigned degree, NodeType nt>
double getAugmentedNOD(int index, std::vector<EmulTask> &allTasks, const std::vector<std::pair<double, double>> &taskProportionExecution, const std::vector<double> &cpuCost, const std::vector<double> &gpuCost)
{
    std::map<int, int> childNum = std::map<int, int>();

    getChildrenOccurences<degree, nt>(childNum, index, allTasks);

    double NOD = 0.f;
    for(auto childNumIter : childNum)
    {
        int childIndex = childNumIter.first;
        int numPresent = childNumIter.second;
        int taskType = allTasks[childIndex].getType();
        if(nt == CPU)
        {
            NOD += taskProportionExecution[taskType].first * cpuCost[taskType] * (double)numPresent / (double)allTasks[childIndex].getPredecessorDependencies().size();
        }
        if(nt == GPU)
        {
            NOD += taskProportionExecution[taskType].second * cpuCost[taskType] * (double)numPresent / (double)allTasks[childIndex].getPredecessorDependencies().size();
        }
    }

    return NOD;
}



template<unsigned degree, NodeType nt>
double getAverageTaskTypeAugmentedNOD(int taskType, std::vector<EmulTask> &allTasks, const std::vector<std::pair<double, double>> &taskProportionExecution, const std::vector<double> &cpuCost, const std::vector<double> &gpuCost)
{
    int taskTypeCount = 0;
    double totalNOD = 0.;
    for(unsigned int t=0;t<allTasks.size();++t)
    {
        if(allTasks[t].getType() == taskType)
        {
            ++taskTypeCount;
            totalNOD += getAugmentedNOD<degree, nt>(t, allTasks, taskProportionExecution, cpuCost, gpuCost);
        }
    }

    if(taskTypeCount <= 0)
    {
        return 0.;
    }

    return totalNOD/(double)taskTypeCount;
}

double getSumSuccessorsBestCost(int index, std::vector<EmulTask> &allTasks, const std::vector<double> &cpuCost, const std::vector<double> &gpuCost)
{
    std::vector<int> successors = allTasks[index].getSuccessorDependencies();
    double sum = 0.0f;

    for(int s=0;s<successors.size();++s)
    {
        int taskType = allTasks[successors[s]].getType();
        if(cpuCost[taskType] < gpuCost[taskType])
        {
            sum += cpuCost[taskType];
        }
        else
        {
            sum += gpuCost[taskType];
        }
    }

    return sum;
}

double getAugmentedSumSuccessorsBestCost(int index, std::vector<EmulTask> &allTasks, const std::vector<double> &cpuCost, const std::vector<double> &gpuCost, double needCPU, double needGPU)
{
    std::vector<int> successors = allTasks[index].getSuccessorDependencies();
    double sum = 0.0f;

    for(int s=0;s<successors.size();++s)
    {
        int taskType = allTasks[successors[s]].getType();
        if(cpuCost[taskType] < gpuCost[taskType])
        {
            sum += needCPU*cpuCost[taskType];
        }
        else
        {
            sum += needGPU*gpuCost[taskType];
        }
    }

    return sum;
}

double getAverageSumSuccessorsBestCost(int taskType, std::vector<EmulTask> &allTasks, const std::vector<double> &cpuCost, const std::vector<double> &gpuCost)
{
    int taskTypeCount = 0;
    double totalSum = 0.;
    for(int t=0;t<allTasks.size();++t)
    {
        if(allTasks[t].getType() == taskType)
        {
            ++taskTypeCount;
            totalSum += getSumSuccessorsBestCost(t, allTasks, cpuCost, gpuCost);
        }
    }

    if(taskTypeCount <= 0)
    {
        return 0.;
    }

    return totalSum/(double)taskTypeCount;
}

double getAugmentedAverageSumSuccessorsBestCost(int taskType, std::vector<EmulTask> &allTasks, const std::vector<double> &cpuCost, const std::vector<double> &gpuCost, double needCPU, double needGPU)
{
    int taskTypeCount = 0;
    double totalSum = 0.;
    for(int t=0;t<allTasks.size();++t)
    {
        if(allTasks[t].getType() == taskType)
        {
            ++taskTypeCount;
            totalSum += getAugmentedSumSuccessorsBestCost(t, allTasks, cpuCost, gpuCost, needCPU, needGPU);
        }
    }

    if(taskTypeCount <= 0)
    {
        return 0.;
    }

    return totalSum/(double)taskTypeCount;
}


std::vector<std::pair<double, double>> normalize(std::vector<std::pair<double, double>> vec)
{
    std::vector<std::pair<double, double>> ret = vec;

    double mean = 0.0f;
    double sd = 0.0f;

    for(unsigned int i=0;i<ret.size();++i)
    {
        mean += ret[i].first*ret[i].second;
    }

    //mean /= (double)vec.size(); // should be ponderated by the first value of vector's pairs

    for(unsigned int i=0;i<ret.size();++i)
    {
        sd += ret[i].first*(ret[i].second-mean)*(ret[i].second-mean);
    }

    for(unsigned int i=0;i<ret.size();++i)
    {
        ret[i].second = (ret[i].second-mean)/sd;
    }

/*
    std::cout << "mean value : " << mean << std::endl;
    std::cout << "standard deviation : " << sd << std::endl;
*/

    return ret;
}

std::vector<std::pair<double, double>> stickInPairs(std::vector<double> v1, std::vector<double> v2)
{
    std::vector<std::pair<double, double>> ret;
    for(unsigned int i=0;i<v1.size();++i)
    {
        ret.push_back(std::make_pair(v1[i], v2[i]));
    }
    return ret;
}

std::vector<double> getAllSecondMembers(std::vector<std::pair<double, double>> vec)
{
    std::vector<double> ret;
    for(unsigned int i=0;i<vec.size();++i)
    {
        ret.push_back(vec[i].second);
    }
    return ret;
}

void printScore(std::vector<std::pair<int, double>> CPUscore, std::vector<std::pair<int, double>> GPUscore)
{
    std::cout << "CPU : ";
    for(int i=0;i<CPUscore.size();++i)
    {
        std::cout << CPUscore[i].second << " ";
    }
    std::cout << std::endl;
    std::cout << "GPU : ";
    for(int i=0;i<GPUscore.size();++i)
    {
        std::cout << GPUscore[i].second << " ";
    }
    std::cout << std::endl;
}

void normalizeCosts(std::vector<EmulTask> &allTasks, std::vector<double> &cpuCost, std::vector<double> &gpuCost)
{
    double totalIdealTime = 0.0f;
    for(unsigned int t=0;t<allTasks.size();++t)
    {
        int taskType = allTasks[t].getType();
        if(cpuCost[taskType] < gpuCost[taskType])
        {
            totalIdealTime += cpuCost[taskType];
        }
        else
        {
            totalIdealTime += gpuCost[taskType];
        }
    }

    for(int i=0;i<cpuCost.size();++i)
    {
        cpuCost[i] *= (float)allTasks.size() / totalIdealTime;
        gpuCost[i] *= (float)allTasks.size() / totalIdealTime;
    }
}

enum PriorityMethod
{
    NOD_TIME_COMBINATION, BEST_NODS, BEST_NODS_SCORES, AUGNOD_PURE, AUGNOD, AUGNOD_2, AUGNOD_DOT_DIFF_PURE, AUGNOD_DOT_DIFF_PURE_2, AUGNOD_DOT_REL_DIFF_PURE, AUGNOD_DOT_REL_DIFF_PURE_2, AUGNOD_DOT_DIFF_2, AUGNOD_DOT_DIFF_3, AUGNOD_DOT_DIFF_4, AUGNOD_DOT_DIFF_5, AUGNOD_DOT_DIFF_6, AUGNOD_DOT_DIFF_7, AUGNOD_DOT_DIFF_8, AUGNOD_DOT_DIFF_9, AUGNOD_DOT_DIFF_10, AUGNOD_DOT_DIFF_11, AUGNODS_PER_SECOND, AUGNODS_PER_SECOND_2, AUGNODS_PER_SECOND_DIFF, NODS_PER_SECOND, NODS_TIME_RELEASED, NODS_TIME_RELEASED_DIFF, AUGNODS_TIME_RELEASED_DIFF, AUGNOD_TIME_COMBINATION
};

template<PriorityMethod PriorityMethod>
std::pair<std::vector<int>, std::vector<int>> computePriorities(std::vector<EmulTask> &allTasks, ExecutionData executionData)
{
    float sizeCeil = (getenv("SIZECEIL")?atof(getenv("SIZECEIL")):1.0);
    float nodSameTimeInterval = (getenv("NODTIME")?atof(getenv("NODTIME")):1.0);
    float NTnodPond = (getenv("NTNODPOND")?atof(getenv("NTNODPOND")):1.0);
    float NTexpVal = (getenv("NTEXP")?atof(getenv("NTEXP")):1.0);
    float BNnodPond = (getenv("BNNODPOND")?atof(getenv("BNNODPOND")):1.0);
    float BNexpVal = (getenv("BNEXP")?atof(getenv("BNEXP")):1.0);


    std::vector<int> taskTypeNum; // number of task of each type
    for(unsigned int t=0;t<executionData.cpuCost.size();++t)
    { // at the beginning, there is 0 of each task
        taskTypeNum.push_back(0);
    }
    for(unsigned int t=0;t<allTasks.size();++t)
    {
        ++taskTypeNum[allTasks[t].getType()];
    }
    std::vector<double> taskProportion;
    for(int i=0;i<executionData.cpuCost.size();++i)
    {
        taskProportion.push_back((double)taskTypeNum[i] / (double)allTasks.size());
    }

    std::vector<double> cpuCost = executionData.cpuCost;
    std::vector<double> gpuCost = executionData.gpuCost;
    normalizeCosts(allTasks, cpuCost, gpuCost);

    // now the average best between CPU and GPU is 1

    std::vector<int> prioritiesCPU;
    std::vector<int> prioritiesGPU;

    if(PriorityMethod == NOD_TIME_COMBINATION)
    {
        std::vector<std::pair<int, double>> CPUscore, GPUscore;

        for(int i=0;i<cpuCost.size();++i)
        {
    /*
            CPUscore.push_back(std::make_pair(i, 
                (gpuCost[i] - cpuCost[i])));
            GPUscore.push_back(std::make_pair(i, 
                (cpuCost[i] - gpuCost[i])));
    */
            CPUscore.push_back(std::make_pair(i, 
                0));
            GPUscore.push_back(std::make_pair(i, 
                0));
        }

        for(int i=0;i<cpuCost.size();++i)
        {
            CPUscore[i].second += gpuCost[i] - cpuCost[i];
            GPUscore[i].second += cpuCost[i] - gpuCost[i];
        }

/*
        std::cout << "Scores due to time saved : " << std::endl;
        printScore(CPUscore, GPUscore);
*/

        // NOD
        std::vector<double> NODs;
        for(int i=0;i<cpuCost.size();++i)
        {
            NODs.push_back(getAverageTaskTypeNOD<3>(i, allTasks));
        }
        NODs = getAllSecondMembers(normalize(stickInPairs(taskProportion, NODs)));
/*
        std::cout << "normalized NODs : ";
        for(int i=0;i<NODs.size();++i)
        {
            std::cout << NODs[i] << " ";
        }
        std::cout << std::endl;
*/
        for(int i=0;i<cpuCost.size();++i)
        {
            float relativeDifference = cpuCost[i]/gpuCost[i];
            if(relativeDifference < 1.0f)
            {
                relativeDifference = 1.0f/relativeDifference;
            }
            float multiplier = exp(-NTexpVal*(relativeDifference-1)*(relativeDifference-1));
            CPUscore[i].second += NTnodPond*multiplier*NODs[i];
            GPUscore[i].second += NTnodPond*multiplier*NODs[i];
        }

/*
        std::cout << "Scores due to NOD : " << std::endl;
        printScore(CPUscore, GPUscore);
*/


        for(int i=0;i<cpuCost.size();++i)
        {
            CPUscore[i].second += sizeCeil*cpuCost[i];
            GPUscore[i].second += sizeCeil*gpuCost[i];
        }

/*
        std::cout << "Scores due to cost : " << std::endl;
        printScore(CPUscore, GPUscore);
*/

        std::sort (CPUscore.begin(), CPUscore.end(), 
            [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
            {
                return elt1.second > elt2.second;
            });
        std::sort (GPUscore.begin(), GPUscore.end(), 
            [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
            {
                return elt1.second > elt2.second;
            });

        for(int i=0;i<executionData.cpuCost.size();++i)
        {
            prioritiesCPU.push_back(0);
            prioritiesGPU.push_back(0);
        }

        for(int i=0;i<executionData.cpuCost.size();++i)
        {
    /*
            prioritiesCPU[CPUscore[i].first] = i;
            prioritiesGPU[GPUscore[i].first] = i;
    */
            prioritiesCPU[i] = CPUscore[i].first;
            prioritiesGPU[i] = GPUscore[i].first;
        }

        return std::make_pair(prioritiesCPU, prioritiesGPU);
    }
    else if(PriorityMethod == BEST_NODS)
    {
        // NOD
        std::vector<double> NODs;
        for(int i=0;i<cpuCost.size();++i)
        {
            NODs.push_back(getAverageTaskTypeNOD<3>(i, allTasks));
        }
        NODs = getAllSecondMembers(normalize(stickInPairs(taskProportion, NODs)));

        std::vector<std::pair<int, double>> NODscore;

        for(int i=0;i<cpuCost.size();++i)
        {
            NODscore.push_back(std::make_pair(i, NODs[i]));
        }

        std::sort (NODscore.begin(), NODscore.end(), 
            [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
            {
                return elt1.second > elt2.second;
            });

        for(int i=0;i<NODscore.size();++i)
        {
            prioritiesCPU.push_back(0);
            prioritiesGPU.push_back(0);
        }

        unsigned int beginCPU = 0;
        unsigned int beginGPU = 0;
        unsigned int endCPU = NODscore.size()-1;
        unsigned int endGPU = NODscore.size()-1;

        for(int i=0;i<NODscore.size();++i)
        {
            if(executionData.cpuCost[NODscore[i].first] < executionData.gpuCost[NODscore[i].first]-nodSameTimeInterval)
            {
                prioritiesCPU[beginCPU] = NODscore[i].first;
                prioritiesGPU[endGPU] = NODscore[i].first;
                ++beginCPU;
                --endGPU;
            }
            else if(executionData.cpuCost[NODscore[i].first] > executionData.gpuCost[NODscore[i].first]+nodSameTimeInterval)
            {
                prioritiesCPU[endCPU] = NODscore[i].first;
                prioritiesGPU[beginGPU] = NODscore[i].first;
                --endCPU;
                ++beginGPU;
            }
            else
            {
                prioritiesCPU[beginCPU] = NODscore[i].first;
                prioritiesGPU[beginGPU] = NODscore[i].first;
                ++beginCPU;
                ++beginGPU;
            }
        }

        return std::make_pair(prioritiesCPU, prioritiesGPU);
    }
    else if(PriorityMethod == BEST_NODS_SCORES)
    {
        // NOD
        std::vector<double> NODs;
        for(int i=0;i<cpuCost.size();++i)
        {
            NODs.push_back(getAverageTaskTypeNOD<3>(i, allTasks));
        }
        //NODs = getAllSecondMembers(normalize(stickInPairs(taskProportion, NODs)));

        std::vector<std::pair<int, double>> CPUscore;
        std::vector<std::pair<int, double>> GPUscore;

        for(int i=0;i<cpuCost.size();++i)
        {
            CPUscore.push_back(std::make_pair(i, 0));
            GPUscore.push_back(std::make_pair(i, 0));
        }

        for(int i=0;i<cpuCost.size();++i)
        {
            float diff = gpuCost[i] - cpuCost[i];
            float distribution = exp(-BNexpVal*(diff)*(diff));
            float CPU = distribution;
            float GPU = distribution;
            if(diff > 0.0f)
            {
                CPU = 1.0f;
            }
            else if(diff < 0.0f)
            {
                GPU = 1.0f;
            }
            CPU = 2.0f*CPU - 1.0f;
            GPU = 2.0f*GPU - 1.0f;
            CPU *= NODs[i];
            GPU *= NODs[i];
            CPUscore[i].second = BNnodPond*CPU;
            GPUscore[i].second = BNnodPond*GPU;
        }

/*
        for(int i=0;i<cpuCost.size();++i)
        {
            CPUscore[i].second += gpuCost[i] - cpuCost[i];
            GPUscore[i].second += cpuCost[i] - gpuCost[i];
        }
*/

        std::sort (CPUscore.begin(), CPUscore.end(), 
            [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
            {
                return elt1.second > elt2.second;
            });
        std::sort (GPUscore.begin(), GPUscore.end(), 
            [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
            {
                return elt1.second > elt2.second;
            });

        for(int i=0;i<executionData.cpuCost.size();++i)
        {
            prioritiesCPU.push_back(0);
            prioritiesGPU.push_back(0);
        }

        for(int i=0;i<executionData.cpuCost.size();++i)
        {
            prioritiesCPU[i] = CPUscore[i].first;
            prioritiesGPU[i] = GPUscore[i].first;
        }

        return std::make_pair(prioritiesCPU, prioritiesGPU);
    }
    else if(PriorityMethod == NODS_PER_SECOND)
    {
        std::vector<std::pair<int, double>> CPUscore, GPUscore;

        for(int i=0;i<cpuCost.size();++i)
        {
            CPUscore.push_back(std::make_pair(i, 
                0));
            GPUscore.push_back(std::make_pair(i, 
                0));
        }

        // NOD
        std::vector<double> NODs;
        for(int i=0;i<cpuCost.size();++i)
        {
            NODs.push_back(getAverageTaskTypeNOD<3>(i, allTasks));
        }
        std::cout << "NODs : ";
        for(int i=0;i<NODs.size();++i)
        {
            std::cout << NODs[i] << " ";
        }
        std::cout << std::endl;

        for(int i=0;i<cpuCost.size();++i)
        {
            CPUscore[i].second = NODs[i]/cpuCost[i];
            GPUscore[i].second = NODs[i]/gpuCost[i];
        }

        std::cout << "NODs per second : " << std::endl;
        printScore(CPUscore, GPUscore);

        std::sort (CPUscore.begin(), CPUscore.end(), 
            [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
            {
                return elt1.second > elt2.second;
            });
        std::sort (GPUscore.begin(), GPUscore.end(), 
            [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
            {
                return elt1.second > elt2.second;
            });

        for(int i=0;i<executionData.cpuCost.size();++i)
        {
            prioritiesCPU.push_back(0);
            prioritiesGPU.push_back(0);
        }

        for(int i=0;i<executionData.cpuCost.size();++i)
        {

            prioritiesCPU[i] = CPUscore[i].first;
            prioritiesGPU[i] = GPUscore[i].first;
        }

        return std::make_pair(prioritiesCPU, prioritiesGPU);
    }
    else if(PriorityMethod == NODS_TIME_RELEASED || PriorityMethod == NODS_TIME_RELEASED_DIFF)
    {
        // NOD
        std::vector<double> NODs;
        for(int i=0;i<cpuCost.size();++i)
        {
            NODs.push_back(getAverageTaskTypeNOD<1>(i, allTasks));
        }

        std::vector<std::pair<int, double>> CPUscore;
        std::vector<std::pair<int, double>> GPUscore;

        for(int i=0;i<cpuCost.size();++i)
        {
            CPUscore.push_back(std::make_pair(i, 0));
            GPUscore.push_back(std::make_pair(i, 0));
        }

        for(int i=0;i<cpuCost.size();++i)
        {
            double sum = getAverageSumSuccessorsBestCost(i, allTasks, cpuCost, gpuCost);

            if(PriorityMethod == NODS_TIME_RELEASED_DIFF)
            {
                CPUscore[i].second = NODs[i]*(sum+gpuCost[i]-cpuCost[i])/cpuCost[i];
                GPUscore[i].second = NODs[i]*(sum+cpuCost[i]-gpuCost[i])/gpuCost[i];
            }
            else
            {
                CPUscore[i].second = NODs[i]*sum/cpuCost[i];
                GPUscore[i].second = NODs[i]*sum/gpuCost[i];
            }
        }

        std::sort (CPUscore.begin(), CPUscore.end(), 
            [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
            {
                return elt1.second > elt2.second;
            });
        std::sort (GPUscore.begin(), GPUscore.end(), 
            [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
            {
                return elt1.second > elt2.second;
            });

        for(int i=0;i<executionData.cpuCost.size();++i)
        {
            prioritiesCPU.push_back(0);
            prioritiesGPU.push_back(0);
        }

        for(int i=0;i<executionData.cpuCost.size();++i)
        {
            prioritiesCPU[i] = CPUscore[i].first;
            prioritiesGPU[i] = GPUscore[i].first;
        }

        return std::make_pair(prioritiesCPU, prioritiesGPU);
    }

    std::cerr << "unrecognized priority method" << std::endl;
    exit(1);
}

double reLU(double x)
{
    if(x<0.0f)
    {
        return 0.0f;
    }
    return x;
}

double rpg(double x)
{
    if(x > 1.0f)
    {
        return 1.0f;
    }
    return sqrt(x)*sqrt(2.0f-x);
}

template<PriorityMethod PriorityMethod>
std::pair<std::vector<int>, std::vector<int>> computePrioritiesAugmentedNOD(std::vector<EmulTask> &allTasks, ExecutionData executionData,
                                                                float needCPU, float needGPU, std::vector<std::pair<double, double>> taskProportionExecution, bool verbose = false)
{
    float augNODdiff = (getenv("AUGNODDIFF")?atof(getenv("AUGNODDIFF")):1.0);
    float augNODnod = (getenv("AUGNODNOD")?atof(getenv("AUGNODNOD")):1.0);
    float augNODprop = (getenv("AUGNODPROP")?atof(getenv("AUGNODPROP")):1.0);
    float and2pond = (getenv("AND2POND")?atof(getenv("AND2POND")):1.0);
    float and3pond = (getenv("AND3POND")?atof(getenv("AND3POND")):1.0);
    float and4pond = (getenv("AND4POND")?atof(getenv("AND4POND")):1.0);
    float and5xoffset = (getenv("AND5XOFFSET")?atof(getenv("AND5XOFFSET")):1.0);
    float and5yoffset = (getenv("AND5YOFFSET")?atof(getenv("AND5YOFFSET")):1.0);
    float and9offset1 = (getenv("AND9OFFSET1")?atof(getenv("AND9OFFSET1")):1.0);
    float and9offset2 = (getenv("AND9OFFSET2")?atof(getenv("AND9OFFSET2")):1.0);
    float and10offset1 = (getenv("AND10OFFSET1")?atof(getenv("AND10OFFSET1")):1.0);
    float and10offset2 = (getenv("AND10OFFSET2")?atof(getenv("AND10OFFSET2")):1.0);
    float and11offset1 = (getenv("AND11OFFSET1")?atof(getenv("AND11OFFSET1")):1.0);
    float and11offset2 = (getenv("AND11OFFSET2")?atof(getenv("AND11OFFSET2")):1.0);
    float ant1 = (getenv("ANT1")?atof(getenv("ANT1")):1.0);
    float ant2 = (getenv("ANT2")?atof(getenv("ANT2")):1.0);
    float ANTexpVal = (getenv("ANTEXP")?atof(getenv("ANTEXP")):1.0);

    std::vector<int> taskTypeNum; // number of task of each type
    for(unsigned int t=0;t<executionData.cpuCost.size();++t)
    { // at the beginning, there is 0 of each task
        taskTypeNum.push_back(0);
    }
    for(unsigned int t=0;t<allTasks.size();++t)
    {
        ++taskTypeNum[allTasks[t].getType()];
    }
    std::vector<double> taskProportion;
    for(int i=0;i<executionData.cpuCost.size();++i)
    {
        taskProportion.push_back((double)taskTypeNum[i] / (double)allTasks.size());
    }

    std::vector<double> cpuCost = executionData.cpuCost;
    std::vector<double> gpuCost = executionData.gpuCost;
    normalizeCosts(allTasks, cpuCost, gpuCost);

    // now the average best between CPU and GPU is 1

    std::vector<int> prioritiesCPU;
    std::vector<int> prioritiesGPU;

    // NOD
    std::vector<double> CPUNODs;
    for(int i=0;i<cpuCost.size();++i)
    {
        CPUNODs.push_back(getAverageTaskTypeAugmentedNOD<3, CPU>(i, allTasks, taskProportionExecution, cpuCost, gpuCost));
    }
    std::vector<double> GPUNODs;
    for(int i=0;i<cpuCost.size();++i)
    {
        GPUNODs.push_back(getAverageTaskTypeAugmentedNOD<3, GPU>(i, allTasks, taskProportionExecution, cpuCost, gpuCost));
    }


    if(verbose)
    {
        std::cout << "Average augmented NODs of task types : " << std::endl;
        std::cout << "CPU : ";
        for(int i=0;i<CPUNODs.size();++i)
        {
            std::cout << CPUNODs[i] << " ";
        }
        std::cout << std::endl;
        std::cout << "GPU : ";
        for(int i=0;i<GPUNODs.size();++i)
        {
            std::cout << GPUNODs[i] << " ";
        }
        std::cout << std::endl;
        std::cout << "total : ";
        for(int i=0;i<GPUNODs.size();++i)
        {
            std::cout << (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU) << " ";
        }
        std::cout << std::endl;
    }


    std::vector<std::pair<int, double>> CPUscore;
    std::vector<std::pair<int, double>> GPUscore;

    for(int i=0;i<cpuCost.size();++i)
    {
        CPUscore.push_back(std::make_pair(i, 0));
        GPUscore.push_back(std::make_pair(i, 0));
    }


    if(verbose)
    {
        std::cerr << "need CPU = " << needCPU << std::endl;
        std::cerr << "need GPU = " << needGPU << std::endl;
    }


    for(int i=0;i<cpuCost.size();++i)
    {
        if(PriorityMethod == AUGNOD_PURE)
        {
            CPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU);
            GPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU);
        }
        else if(PriorityMethod == AUGNOD)
        {
            CPUscore[i].second = augNODnod*(CPUNODs[i]*needCPU + GPUNODs[i]*needGPU) + augNODdiff*(gpuCost[i] - cpuCost[i]);
            GPUscore[i].second = augNODnod*(CPUNODs[i]*needCPU + GPUNODs[i]*needGPU) + augNODdiff*(cpuCost[i] - gpuCost[i]);
        }
        else if(PriorityMethod == AUGNOD_2)
        {
            CPUscore[i].second = augNODnod*(CPUNODs[i]*needCPU + GPUNODs[i]*needGPU) + augNODdiff*(gpuCost[i] - cpuCost[i]) + augNODprop*reLU(taskProportionExecution[i].first*taskProportionExecution[i].second*(needCPU-needGPU));
            GPUscore[i].second = augNODnod*(CPUNODs[i]*needCPU + GPUNODs[i]*needGPU) + augNODdiff*(cpuCost[i] - gpuCost[i]) + augNODprop*reLU(taskProportionExecution[i].first*taskProportionExecution[i].second*(needGPU-needCPU));
        }
        else if(PriorityMethod == AUGNOD_DOT_DIFF_PURE)
        {
            CPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(gpuCost[i] - cpuCost[i]);
            GPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(cpuCost[i] - gpuCost[i]);
        }
        else if(PriorityMethod == AUGNOD_DOT_DIFF_PURE_2)
        {
            CPUscore[i].second = (1 + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(gpuCost[i] - cpuCost[i]);
            GPUscore[i].second = (1 + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(cpuCost[i] - gpuCost[i]);
        }
        else if(PriorityMethod == AUGNOD_DOT_REL_DIFF_PURE)
        {
            CPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(gpuCost[i] / cpuCost[i]);
            GPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(gpuCost[i] / cpuCost[i]);
        }
        else if(PriorityMethod == AUGNOD_DOT_REL_DIFF_PURE_2)
        {
            CPUscore[i].second = (1 + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(gpuCost[i] / cpuCost[i]);
            GPUscore[i].second = (1 + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(cpuCost[i] / gpuCost[i]);
        }
        else if(PriorityMethod == AUGNOD_DOT_DIFF_2)
        {
            CPUscore[i].second = (1 + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(gpuCost[i] - cpuCost[i]) + and2pond*cpuCost[i]*(needCPU-needGPU);
            GPUscore[i].second = (1 + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(cpuCost[i] - gpuCost[i]) + and2pond*gpuCost[i]*(needGPU-needCPU);
        }
        else if(PriorityMethod == AUGNOD_DOT_DIFF_3)
        {
            CPUscore[i].second = (1 + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(gpuCost[i] - cpuCost[i]) + and3pond*cpuCost[i]*reLU(needCPU-needGPU);
            GPUscore[i].second = (1 + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(cpuCost[i] - gpuCost[i]) + and3pond*gpuCost[i]*reLU(needGPU-needCPU);
        }
        else if(PriorityMethod == AUGNOD_DOT_DIFF_4)
        {
            CPUscore[i].second = (1 + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(gpuCost[i] - cpuCost[i]) - and4pond*cpuCost[i]*reLU(needGPU-needCPU);
            GPUscore[i].second = (1 + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(cpuCost[i] - gpuCost[i]) - and4pond*gpuCost[i]*reLU(needCPU-needGPU);
        }
        else if(PriorityMethod == AUGNOD_DOT_DIFF_5)
        {
            CPUscore[i].second = (and5xoffset + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(and5yoffset + gpuCost[i] - cpuCost[i]);
            GPUscore[i].second = (and5xoffset + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(and5yoffset + cpuCost[i] - gpuCost[i]);
        }
        else if(PriorityMethod == AUGNOD_DOT_DIFF_6)
        {
            CPUscore[i].second = (1 + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*log(1 + exp((gpuCost[i] - cpuCost[i])));
            GPUscore[i].second = (1 + CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*log(1 + exp((cpuCost[i] - gpuCost[i])));
        }
        else if(PriorityMethod == AUGNOD_DOT_DIFF_7)
        {
            double x = CPUNODs[i]*needCPU + GPUNODs[i]*needGPU;
            double y = gpuCost[i] - cpuCost[i];
            CPUscore[i].second = rpg(x)*(1+x)*(1+y)+(1-rpg(x))*(-log(1+exp(-y)));
            y = cpuCost[i] - gpuCost[i];
            GPUscore[i].second = rpg(x)*(1+x)*(1+y)+(1-rpg(x))*(-log(1+exp(-y)));
        }
        else if(PriorityMethod == AUGNOD_DOT_DIFF_8)
        {
            double x = CPUNODs[i]*needCPU + GPUNODs[i]*needGPU;
            double y = gpuCost[i] - cpuCost[i];
            CPUscore[i].second = (1/(1+exp(-x))-0.5)*(1+x)*(1+y)+(1/(1+exp(-1/x))-0.5)*(-exp(-y));
            y = cpuCost[i] - gpuCost[i];
            GPUscore[i].second = (1/(1+exp(-x))-0.5)*(1+x)*(1+y)+(1/(1+exp(-1/x))-0.5)*(-exp(-y));
        }
        else if(PriorityMethod == AUGNOD_DOT_DIFF_9)
        {
            double x = CPUNODs[i]*needCPU + GPUNODs[i]*needGPU;
            double y = gpuCost[i] - cpuCost[i];//0.6*LOG(1.2+A1)*ATAN(B1+0.05*A1)
            CPUscore[i].second = log(and9offset1+x)*atan(y+and9offset2*x);
            y = cpuCost[i] - gpuCost[i];
            GPUscore[i].second = log(and9offset1+x)*atan(y+and9offset2*x);
        }
        else if(PriorityMethod == AUGNOD_DOT_DIFF_10)
        {
            double x = CPUNODs[i]*needCPU + GPUNODs[i]*needGPU;
            double y = gpuCost[i] - cpuCost[i];
            CPUscore[i].second = (and10offset1+x)*atan(y) + and10offset2*x;
            y = cpuCost[i] - gpuCost[i];
            GPUscore[i].second = (and10offset1+x)*atan(y) + and10offset2*x;
        }
        else if(PriorityMethod == AUGNOD_DOT_DIFF_11)
        {
            double x = CPUNODs[i]*needCPU + GPUNODs[i]*needGPU;
            double y = gpuCost[i] - cpuCost[i];
            CPUscore[i].second = (and11offset1+x)*(y+and11offset2*x);
            y = cpuCost[i] - gpuCost[i];
            GPUscore[i].second = (and11offset1+x)*(y+and11offset2*x);
        }
        else if(PriorityMethod == AUGNODS_PER_SECOND)
        {
            CPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU) / cpuCost[i];
            GPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU) / gpuCost[i];
        }
        else if(PriorityMethod == AUGNODS_PER_SECOND_2)
        {
            CPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU + gpuCost[i] - cpuCost[i]) / cpuCost[i];
            GPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU + cpuCost[i] - gpuCost[i]) / gpuCost[i];
        }
        else if(PriorityMethod == AUGNODS_PER_SECOND_DIFF)
        {
            CPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU) / cpuCost[i] + gpuCost[i] - cpuCost[i];
            GPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU) / gpuCost[i] + cpuCost[i] - gpuCost[i];
        }
        else if(PriorityMethod == AUGNODS_TIME_RELEASED_DIFF)
        {
            double sum = getAverageSumSuccessorsBestCost(i, allTasks, cpuCost, gpuCost);

            CPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(sum+gpuCost[i]-cpuCost[i])/cpuCost[i];
            GPUscore[i].second = (CPUNODs[i]*needCPU + GPUNODs[i]*needGPU)*(sum+cpuCost[i]-gpuCost[i])/gpuCost[i];
        }
        else if(PriorityMethod == AUGNOD_TIME_COMBINATION)
        {
            //NODs = getAllSecondMembers(normalize(stickInPairs(taskProportion, NODs)));

            float relativeDifference = cpuCost[i]/gpuCost[i];
            if(relativeDifference < 1.0f)
            {
                relativeDifference = 1.0f/relativeDifference;
            }
            float multiplier = exp(-ANTexpVal*(relativeDifference-1)*(relativeDifference-1));

            double x = CPUNODs[i]*needCPU + GPUNODs[i]*needGPU;
            double y = gpuCost[i] - cpuCost[i];
            CPUscore[i].second = y*ant1 + x*multiplier*ant2;
            y = cpuCost[i] - gpuCost[i];
            GPUscore[i].second = y*ant1 + x*multiplier*ant2;
        }
    }



    if(verbose)
    {
        std::cout << "Scores : " << std::endl;
        printScore(CPUscore, GPUscore);
    }


    std::sort (CPUscore.begin(), CPUscore.end(), 
        [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
        {
            return elt1.second > elt2.second;
        });
    std::sort (GPUscore.begin(), GPUscore.end(), 
        [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
        {
            return elt1.second > elt2.second;
        });

    for(int i=0;i<executionData.cpuCost.size();++i)
    {
        prioritiesCPU.push_back(0);
        prioritiesGPU.push_back(0);
    }

    for(int i=0;i<executionData.cpuCost.size();++i)
    {
        prioritiesCPU[i] = CPUscore[i].first;
        prioritiesGPU[i] = GPUscore[i].first;
    }

    return std::make_pair(prioritiesCPU, prioritiesGPU);
}


std::pair<std::vector<int>, std::vector<int>> computePrioritiesFromGrid(std::vector<EmulTask> &allTasks, ExecutionData executionData,
                                                                float needCPU, float needGPU, std::vector<std::pair<double, double>> taskProportionExecution, double grid[GRID_X_NUM][GRID_Y_NUM])
{
    std::vector<int> taskTypeNum; // number of task of each type
    for(unsigned int t=0;t<executionData.cpuCost.size();++t)
    { // at the beginning, there is 0 of each task
        taskTypeNum.push_back(0);
    }
    for(unsigned int t=0;t<allTasks.size();++t)
    {
        ++taskTypeNum[allTasks[t].getType()];
    }
    std::vector<double> taskProportion;
    for(int i=0;i<executionData.cpuCost.size();++i)
    {
        taskProportion.push_back((double)taskTypeNum[i] / (double)allTasks.size());
    }

    std::vector<double> cpuCost = executionData.cpuCost;
    std::vector<double> gpuCost = executionData.gpuCost;
    normalizeCosts(allTasks, cpuCost, gpuCost);

    // now the average best between CPU and GPU is 1

    std::vector<int> prioritiesCPU;
    std::vector<int> prioritiesGPU;

    // NOD
    std::vector<double> CPUNODs;
    for(int i=0;i<cpuCost.size();++i)
    {
        CPUNODs.push_back(getAverageTaskTypeAugmentedNOD<3, CPU>(i, allTasks, taskProportionExecution, cpuCost, gpuCost));
    }
    std::vector<double> GPUNODs;
    for(int i=0;i<cpuCost.size();++i)
    {
        GPUNODs.push_back(getAverageTaskTypeAugmentedNOD<3, GPU>(i, allTasks, taskProportionExecution, cpuCost, gpuCost));
    }


    std::vector<std::pair<int, double>> CPUscore;
    std::vector<std::pair<int, double>> GPUscore;

    for(int i=0;i<cpuCost.size();++i)
    {
        CPUscore.push_back(std::make_pair(i, 0));
        GPUscore.push_back(std::make_pair(i, 0));
    }


    for(int i=0;i<cpuCost.size();++i)
    {
        double x = CPUNODs[i]*needCPU + GPUNODs[i]*needGPU;
        double y = gpuCost[i] - cpuCost[i];

        CPUscore[i].second = getInterpolatedValue(x, y, grid);
        y = cpuCost[i] - gpuCost[i];
        GPUscore[i].second = getInterpolatedValue(x, y, grid);
    }


    std::sort (CPUscore.begin(), CPUscore.end(), 
        [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
        {
            return elt1.second > elt2.second;
        });
    std::sort (GPUscore.begin(), GPUscore.end(), 
        [&](const std::pair<int, float> elt1, const std::pair<int, float> elt2) -> bool
        {
            return elt1.second > elt2.second;
        });

    for(int i=0;i<executionData.cpuCost.size();++i)
    {
        prioritiesCPU.push_back(0);
        prioritiesGPU.push_back(0);
    }

    for(int i=0;i<executionData.cpuCost.size();++i)
    {
        prioritiesCPU[i] = CPUscore[i].first;
        prioritiesGPU[i] = GPUscore[i].first;
    }

    return std::make_pair(prioritiesCPU, prioritiesGPU);
}


double getGridScore(double grid[GRID_X_NUM][GRID_Y_NUM], int seed)
{
    int oldseed = rand();

    srand(seed);

    std::vector<EmulTask> allTasks;

    ExecutionData executionData;

    int taskTypeNum;

    double totalExecutionTime = 0.0f;

    for(int scenario=0;scenario<1024;++scenario)
    {
        taskTypeNum = 2+rand()%16;
        executionData.taskNumber = 1024+rand()%2048;
        executionData.cpuCost = getRandomCosts(taskTypeNum);
        executionData.gpuCost = getRandomCosts(taskTypeNum);
        makeAtLeastOneCostBetterForEachArch(executionData.cpuCost, executionData.gpuCost);
        executionData.taskTypeProportion = getRandomTaskProportion(taskTypeNum);
        executionData.NbCpus = getRandomPUnum();
        executionData.NbGpus = getRandomPUnum();
        executionData.predecessorRule = getRandomPredecessorRule(taskTypeNum);
        executionData.seed = rand();

        allTasks = GenerateTasksWithPerfectSolution(executionData.seed, executionData.taskNumber, executionData.predecessorRule, executionData.taskTypeProportion, executionData.cpuCost, executionData.gpuCost, executionData.NbCpus, executionData.NbGpus);

        // normalize costs :
        std::vector<double> cpuCost = executionData.cpuCost;
        std::vector<double> gpuCost = executionData.gpuCost;
        normalizeCosts(allTasks, cpuCost, gpuCost);


        std::pair<std::vector<int>, std::vector<int>> priorities;
        priorities.first.clear();
        priorities.second.clear();

        priorities = computePriorities<BEST_NODS>(allTasks, executionData);

        ExecutionStatistics executionStatistics;

        for(int i=0;i<3;++i)
        {
            double executionTime = Execute(allTasks, executionData.cpuCost, executionData.gpuCost, priorities.first, priorities.second,
                                             executionData.NbCpus, executionData.NbGpus, &executionStatistics);
            priorities = computePrioritiesFromGrid(allTasks, executionData,
                                                            1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                            1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                            executionStatistics.taskCPUGPUproportion, grid);
        }

        double finalExecutionTime = Execute(allTasks, executionData.cpuCost, executionData.gpuCost, priorities.first, priorities.second,
                                         executionData.NbCpus, executionData.NbGpus);

        totalExecutionTime += finalExecutionTime;
    }

    srand(oldseed);

    return totalExecutionTime;
}

void copyGrid(double target[GRID_X_NUM][GRID_Y_NUM], double source[GRID_X_NUM][GRID_Y_NUM])
{
    for(int xi=0;xi<GRID_X_NUM;++xi)
    {
        for(int yi=0;yi<GRID_Y_NUM;++yi)
        {
            target[xi][yi] = source[xi][yi];
        }
    }
}

void normalizeGrid(double grid[GRID_X_NUM][GRID_Y_NUM])
{
    double mean = 0.0f;
    double sd = 0.0f;

    for(int xi=0;xi<GRID_X_NUM;++xi)
    {
        for(int yi=0;yi<GRID_Y_NUM;++yi)
        {
            mean += grid[xi][yi];
        }
    }

    mean /= (double)(GRID_X_NUM*GRID_Y_NUM);

    for(int xi=0;xi<GRID_X_NUM;++xi)
    {
        for(int yi=0;yi<GRID_Y_NUM;++yi)
        {
            sd += (grid[xi][yi]-mean)*(grid[xi][yi]-mean);
        }
    }

    sd /= (double)(GRID_X_NUM*GRID_Y_NUM);

    for(int xi=0;xi<GRID_X_NUM;++xi)
    {
        for(int yi=0;yi<GRID_Y_NUM;++yi)
        {
            grid[xi][yi] = (grid[xi][yi]-mean)/sqrt(sd);
        }
    }
}

void smoothenGrid(double grid[GRID_X_NUM][GRID_Y_NUM])
{
    double copy[GRID_X_NUM][GRID_Y_NUM];

    for(int xi=0;xi<GRID_X_NUM;++xi)
    {
        for(int yi=0;yi<GRID_Y_NUM;++yi)
        {
            double val = grid[xi][yi];
            double x=getXdouble(xi);
            double y=getYdouble(yi);

            int mx = getGridXindexFloor(x);
            int Mx = getGridXindexCeil(x);
            int my = getGridYindexFloor(y);
            int My = getGridYindexCeil(y);

            double neighbourhood = getInterpolatedValue(x+0.5, y-0.5, grid)
                                 + getInterpolatedValue(x-0.5, y+0.5, grid)
                                 + getInterpolatedValue(x+0.5, y+0.5, grid)
                                 + getInterpolatedValue(x-0.5, y-0.5, grid)
                                 + getInterpolatedValue(x+0.7, y, grid)
                                 + getInterpolatedValue(x, y+0.7, grid)
                                 + getInterpolatedValue(x-0.7, y, grid)
                                 + getInterpolatedValue(x, y-0.7, grid);
            /*double neighbourhood = grid[mx][my]
                                 + grid[xi][my]
                                 + grid[Mx][my]
                                 + grid[mx][yi]
                                 + grid[Mx][yi]
                                 + grid[mx][My]
                                 + grid[xi][My]
                                 + grid[Mx][My];*/
            neighbourhood /= 8.0f;

            copy[xi][yi] = val*0.9 + neighbourhood*0.1;
        }
    }

    for(int xi=0;xi<GRID_X_NUM;++xi)
    {
        for(int yi=0;yi<GRID_Y_NUM;++yi)
        {
            grid[xi][yi] = copy[xi][yi];
        }
    }
}

#include <signal.h>

double bestGrid[GRID_X_NUM][GRID_Y_NUM];

void saveBest(void)
{
    //smoothenGrid(bestGrid);
    normalizeGrid(bestGrid);

    std::ofstream csvfile("best_grid.csv");
    if(csvfile.is_open() == false){
        throw std::invalid_argument("Cannot open filename");
    }
    for(int xi=0;xi<GRID_X_NUM;++xi)
    {
        for(int yi=0;yi<GRID_Y_NUM;++yi)
        {
            double x=getXdouble(xi);
            double y=getYdouble(yi);

            csvfile << x << ";" << y << ";" << getInterpolatedValue(x, y, bestGrid) << std::endl;
        }
    }

    csvfile.close();

    std::ofstream pcdfile("best_grid.pcd");
    if(pcdfile.is_open() == false){
        throw std::invalid_argument("Cannot open filename");
    }
    int pointNum = 0;
    for(double x=GRID_X_MIN-5.0f;x<=GRID_X_MAX+5.0f;x+=0.2f)
    {
        for(double y=GRID_Y_MIN;y<=GRID_Y_MAX+5.0f;y+=0.2f)
        {
            ++pointNum;
        }
    }
    pcdfile << "# .PCD v.7 - Point Cloud Data file format" << std::endl;
    pcdfile << "VERSION .7" << std::endl;
    pcdfile << "FIELDS x y z r g b" << std::endl;
    pcdfile << "SIZE 4 4 4 4 4 4" << std::endl;
    pcdfile << "TYPE F F F F F F" << std::endl;
    pcdfile << "COUNT 1 1 1 1 1 1" << std::endl;
    pcdfile << "WIDTH " << pointNum << std::endl;
    pcdfile << "HEIGHT 1" << std::endl;
    pcdfile << "VIEWPOINT 0 0 0 1 0 0 0" << std::endl;
    pcdfile << "POINTS " << pointNum << std::endl;
    pcdfile << "DATA ascii" << std::endl;
    for(double x=GRID_X_MIN;x<=GRID_X_MAX+5.0f;x+=0.2f)
    {
        for(double y=GRID_Y_MIN-5.0f;y<=GRID_Y_MAX+5.0f;y+=0.2f)
        {
            double z=getInterpolatedValue(x, y, bestGrid);

            double r = 1;
            double g = 1;
            double b = 1;

            r = std::min((double)1.0f, (double)r);
            b = std::min((double)1.0f, (double)b);
            r = std::max((double)0.0f, (double)r);
            b = std::max((double)0.0f, (double)b);

            pcdfile << y << " " << x << " " << z << " " << r << " " << g << " " << b << std::endl;
        }
    }

    pcdfile.close();
}

void saveBestAndExit(int dummy)
{
    saveBest();
    exit(0);
}

void test_train(void)
{
    signal(SIGINT, saveBestAndExit);

    double currentGrid[GRID_X_NUM][GRID_Y_NUM];

    for(int xi=0;xi<GRID_X_NUM;++xi)
    {
        for(int yi=0;yi<GRID_Y_NUM;++yi)
        {
            double x=getXdouble(xi);
            double y=getYdouble(yi);

            currentGrid[xi][yi] = (1+x)*(1+y);
            bestGrid[xi][yi] = (1+x)*(1+y);
        }
    }
    normalizeGrid(bestGrid);

    double bestScore = 99999999999.f;
    int normalizeCounter = 0;

    while(1)
    {
        if(normalizeCounter > 100)
        {
            saveBest();
            bestScore = getGridScore(bestGrid, 42); // grid score should stay about the same, but we recompute it
            std::cout << "Best grid has been saved" << std::endl;

            normalizeCounter = 0;
        }

        double currentScore = getGridScore(currentGrid, 42);

        if(currentScore < bestScore)
        {
            bestScore = currentScore;
            copyGrid(bestGrid, currentGrid);
            normalizeCounter += 5;
            std::cout << "New best score : " << bestScore << std::endl;
        }

        copyGrid(currentGrid, bestGrid);

        for(int i=0;i<3;++i)
        {
            int x = rand()%GRID_X_NUM;
            int y = rand()%GRID_X_NUM;
            int mx = std::max(x-1, 0);
            int Mx = std::min(x+1, GRID_X_NUM-1);
            int my = std::max(y-1, 0);
            int My = std::min(y+1, GRID_Y_NUM-1);

            double minVal = std::min({currentGrid[mx][my],
                                        currentGrid[x][my],
                                        currentGrid[Mx][my],
                                        currentGrid[mx][y],
                                        currentGrid[x][y],
                                        currentGrid[Mx][y],
                                        currentGrid[mx][My],
                                        currentGrid[x][My],
                                        currentGrid[Mx][My]});
            double maxVal = std::max({currentGrid[mx][my],
                                        currentGrid[x][my],
                                        currentGrid[Mx][my],
                                        currentGrid[mx][y],
                                        currentGrid[x][y],
                                        currentGrid[Mx][y],
                                        currentGrid[mx][My],
                                        currentGrid[x][My],
                                        currentGrid[Mx][My]});

            float offs = (maxVal-minVal)*0.2f;
            minVal -= offs;
            maxVal += offs;

            currentGrid[x][y] = minVal + ((double)(rand()%10001))*0.0001f*(maxVal-minVal);
        }

        ++normalizeCounter;
    }
}


double getCloseTaskProportion(std::vector<EmulTask> &allTasks, ExecutionData executionData, float maxDiff = 1.2f)
{
    int count = 0;
    for(unsigned int t=0;t<allTasks.size();++t)
    {
        int taskType = allTasks[t].getType();
        float diff = executionData.cpuCost[taskType]/executionData.gpuCost[taskType];
        if(diff < 1.0f)
        {
            diff = 1.0f/diff;
        }

        if(diff <= maxDiff)
        {
            ++count;
        }
    }

    if(count <= 0)
    {
        return 0.;
    }

    return (double)count/(double)allTasks.size();
}

double getNumerousPredecessorsTaskProportion(std::vector<EmulTask> &allTasks, ExecutionData executionData, int minCount = 4)
{
    int count = 0;
    for(unsigned int t=0;t<allTasks.size();++t)
    {
        if(allTasks[t].getPredecessorDependencies().size() >= minCount)
        {
            ++count;
        }
    }

    if(count <= 0)
    {
        return 0.;
    }

    return (double)count/(double)allTasks.size();
}

double getAveragePredecessorNumber(std::vector<EmulTask> &allTasks, ExecutionData executionData)
{
    double count = 0.;
    for(unsigned int t=0;t<allTasks.size();++t)
    {
        count += (double)allTasks[t].getPredecessorDependencies().size();
    }

    if(count <= 0.)
    {
        return 0.;
    }

    return (double)count/(double)allTasks.size();
}

double getNumerousSuccessorsTaskProportion(std::vector<EmulTask> &allTasks, ExecutionData executionData, int minCount = 5)
{
    int count = 0;
    for(unsigned int t=0;t<allTasks.size();++t)
    {
        if(allTasks[t].getSuccessorDependencies().size() >= minCount)
        {
            ++count;
        }
    }

    if(count <= 0)
    {
        return 0.;
    }

    return (double)count/(double)allTasks.size();
}

int getMaxPredecessorNumber(std::vector<EmulTask> &allTasks, ExecutionData executionData)
{
    int max = 0.;
    for(unsigned int t=0;t<allTasks.size();++t)
    {
	max = std::max((int)max, (int)allTasks[t].getPredecessorDependencies().size());
    }

    return max;
}

int getMaxSuccessorNumber(std::vector<EmulTask> &allTasks, ExecutionData executionData)
{
    int max = 0.;
    for(unsigned int t=0;t<allTasks.size();++t)
    {
	max = std::max((int)max, (int)allTasks[t].getSuccessorDependencies().size());
    }

    return max;
}

double getAverageNormalizedCPUGPUdiff(std::vector<EmulTask> &allTasks, ExecutionData executionData)
{
    std::vector<double> cpuCost = executionData.cpuCost;
    std::vector<double> gpuCost = executionData.gpuCost;
    normalizeCosts(allTasks, cpuCost, gpuCost);


    double totalDiff = 0.0f;
    for(unsigned int t=0;t<allTasks.size();++t)
    {
        int taskType = allTasks[t].getType();
        totalDiff += fabs(cpuCost[taskType] - gpuCost[taskType]);
    }

    return totalDiff / (double)allTasks.size();
}

void saveDataStatisticsToFile()
{
    std::ofstream csvfile("data_statistics.csv");
    if(csvfile.is_open() == false)
    {
        throw std::invalid_argument("Cannot open filename");
    }

    csvfile << "data index;CPU number;GPU number;CPU/GPU;close CPU-GPU task proportion;far CPU-GPU task proportion;task with numerous predecessors proportion;average predecessor number;max predecessor number;task without successor proportion;task with numerous successors proportion;max successor number;average CPU-GPU diff;" << std::endl;

    for(int dataNum=0;dataNum<32;++dataNum)
    {
        ExecutionData executionData = loadData(dataNum);
        std::vector<EmulTask> allTasks = GenerateTasksWithPerfectSolution(executionData.seed, executionData.taskNumber, executionData.predecessorRule, executionData.taskTypeProportion, executionData.cpuCost, executionData.gpuCost, executionData.NbCpus, executionData.NbGpus);

        csvfile << dataNum << ";";
        csvfile << executionData.NbCpus << ";";
        csvfile << executionData.NbGpus << ";";

        csvfile << (double)((double)executionData.NbCpus/(double)executionData.NbGpus) << ";";
        csvfile << (double)(getCloseTaskProportion(allTasks, executionData, 1.2f)) << ";";
        csvfile << (double)(1.0f - getCloseTaskProportion(allTasks, executionData, 3.0f)) << ";";
        csvfile << (double)(getNumerousPredecessorsTaskProportion(allTasks, executionData)) << ";";
        csvfile << (double)(getAveragePredecessorNumber(allTasks, executionData)) << ";";
        csvfile << (int)(getMaxPredecessorNumber(allTasks, executionData)) << ";";
        csvfile << (double)(1.0f-getNumerousSuccessorsTaskProportion(allTasks, executionData, 1)) << ";";
        csvfile << (double)(getNumerousSuccessorsTaskProportion(allTasks, executionData)) << ";";
        csvfile << (int)(getMaxSuccessorNumber(allTasks, executionData)) << ";";
        csvfile << (double)(getAverageNormalizedCPUGPUdiff(allTasks, executionData)) << ";";

        csvfile << std::endl;
    }

    csvfile.close();
}

std::pair<std::vector<int>, std::vector<int>> computePrioritiesAugmentedNODrounds(PriorityMethod priorityMethod, std::vector<EmulTask> &allTasks, ExecutionData executionData)
{
    std::pair<std::vector<int>, std::vector<int>> priorities = computePriorities<NOD_TIME_COMBINATION>(allTasks, executionData);;
    ExecutionStatistics executionStatistics;

    const int nbIter = 3;
    for(int step=0;step<nbIter;++step)
    {
        double executionTime = Execute(allTasks, executionData.cpuCost, executionData.gpuCost, priorities.first, priorities.second,
                                         executionData.NbCpus, executionData.NbGpus, &executionStatistics);

        if(step == nbIter-1)
        {
            std::cout << "CPU busy time : " << executionStatistics.busyTime[0] << std::endl;
            std::cout << "GPU busy time : " << executionStatistics.busyTime[1] << std::endl;
            std::cout << "CPU busy % : " << (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus) << std::endl;
            std::cout << "GPU busy % : " << (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus) << std::endl;
            std::cout << "Task exec CPU % :";
            for(int i=0;i<executionStatistics.taskCPUGPUproportion.size();++i)
            {
                std::cout << executionStatistics.taskCPUGPUproportion[i].first << " ";
            }
            std::cout << std::endl;
            std::cout << "Task exec GPU % :";
            for(int i=0;i<executionStatistics.taskCPUGPUproportion.size();++i)
            {
                std::cout << executionStatistics.taskCPUGPUproportion[i].second << " ";
            }
            std::cout << std::endl;
        }

        if(priorityMethod == AUGNOD_PURE)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_PURE>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_2)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_2>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_DIFF_PURE)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_DIFF_PURE>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_DIFF_PURE_2)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_DIFF_PURE_2>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_REL_DIFF_PURE)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_REL_DIFF_PURE>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_REL_DIFF_PURE_2)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_REL_DIFF_PURE_2>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_DIFF_2)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_DIFF_2>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_DIFF_3)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_DIFF_3>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_DIFF_4)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_DIFF_4>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_DIFF_5)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_DIFF_5>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_DIFF_6)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_DIFF_6>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_DIFF_7)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_DIFF_7>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_DIFF_8)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_DIFF_8>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_DIFF_9)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_DIFF_9>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_DIFF_10)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_DIFF_10>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_DOT_DIFF_11)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_DOT_DIFF_11>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNODS_PER_SECOND)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNODS_PER_SECOND>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNODS_PER_SECOND_2)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNODS_PER_SECOND_2>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNODS_PER_SECOND_DIFF)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNODS_PER_SECOND_DIFF>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNODS_TIME_RELEASED_DIFF)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNODS_TIME_RELEASED_DIFF>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }
        else if(priorityMethod == AUGNOD_TIME_COMBINATION)
        {
            priorities = computePrioritiesAugmentedNOD<AUGNOD_TIME_COMBINATION>(allTasks, executionData,
                                                        1.0f - (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus),
                                                        1.0f - (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus),
                                                        executionStatistics.taskCPUGPUproportion, step == nbIter-1);
        }

    }

    return priorities;
}

////////////////////////////////////////////////
////////////////////////////////////////////////

int main(int argc, char** argv){
    std::vector<EmulTask> allTasks;

    ExecutionData executionData;

    std::cout << std::fixed;
    std::cout << std::setprecision(4);
    srand(0);

/*
    int taskTypeNum;

    for(int i=0;i<32;++i)
    {
        taskTypeNum = 4+rand()%5;
        executionData.taskNumber = 1024+rand()%2048;
        executionData.cpuCost = getRandomCosts(taskTypeNum);
        executionData.gpuCost = getRandomCosts(taskTypeNum);
        makeAtLeastOneCostBetterForEachArch(executionData.cpuCost, executionData.gpuCost);
        executionData.taskTypeProportion = getRandomTaskProportion(taskTypeNum);
        executionData.NbCpus = getRandomPUnum();
        executionData.NbGpus = getRandomPUnum();
        executionData.predecessorRule = getRandomPredecessorRule(taskTypeNum);
        executionData.seed = rand();

        printExecutionDataCode(executionData, i);

        std::cout << std::endl << std::endl;
    }
*/



/*
    std::vector<std::vector<int>> typeTasks = Explorer(allTasks);
    for(size_t j = 0; j < typeTasks.size(); ++j){
        std::cout << "Tache " << typeTasks[j][0] << " : L'arbre contient " << typeTasks[j][1] << " fois ce type de tâche et elle a " << typeTasks[j][2] << " successeurs" << std::endl;
    }

    const std::vector<int> priorityCpu = FindCpuPriority(executionData.cpuCost, typeTasks);
    const std::vector<int> priorityGpu = FindGpuPriority(executionData.gpuCost, typeTasks);
*/


/*
    std::vector<int> priorityCpuExample;
    std::vector<int> priorityGpuExample;

    priorityCpuExample.clear();
    priorityGpuExample.clear();


    for(int i=0;i<executionData.cpuCost.size();++i)
    {
        priorityCpuExample.push_back(i);
        priorityGpuExample.push_back(i);
    }


    // ----------------- FIND THE PRIORITY HERE ------------------

    std::vector<int> priorityCpu = priorityCpuExample;
    std::vector<int> priorityGpu = priorityGpuExample;


    double bestExecTime = 9999999.f;
    std::vector<std::pair<std::vector<int>, std::vector<int>>> bestpriorities;
    bestpriorities.push_back(std::make_pair(priorityCpu, priorityGpu));

    for(int step=0;step<3;++step)
    {
        bestpriorities.clear();
        bestExecTime = 9999999.f;
        do {
            double executionTime = Execute(allTasks, executionData.cpuCost, executionData.gpuCost, priorityCpu, priorityGpu,
                                             executionData.NbCpus, executionData.NbGpus);

            if(executionTime < bestExecTime)
            {
                bestExecTime = executionTime;
                bestpriorities.clear();
                bestpriorities.push_back(std::make_pair(priorityCpu, priorityGpu));
            }
            else if(executionTime == bestExecTime)
            {
                bestpriorities.push_back(std::make_pair(priorityCpu, priorityGpu));
            }
        } while (next_permutation(priorityCpu.begin(), priorityCpu.end()));
        priorityCpu = bestpriorities[rand()%bestpriorities.size()].first;
        priorityGpu = bestpriorities[rand()%bestpriorities.size()].second;

        bestpriorities.clear();
        bestExecTime = 9999999.f;
        do {
            double executionTime = Execute(allTasks, executionData.cpuCost, executionData.gpuCost, priorityCpu, priorityGpu,
                                             executionData.NbCpus, executionData.NbGpus);

            if(executionTime < bestExecTime)
            {
                bestExecTime = executionTime;
                bestpriorities.clear();
                bestpriorities.push_back(std::make_pair(priorityCpu, priorityGpu));
            }
            else if(executionTime == bestExecTime)
            {
                bestpriorities.push_back(std::make_pair(priorityCpu, priorityGpu));
            }
        } while (next_permutation(priorityGpu.begin(), priorityGpu.end()));
        priorityCpu = bestpriorities[rand()%bestpriorities.size()].first;
        priorityGpu = bestpriorities[rand()%bestpriorities.size()].second;
    }

    std::cout << "Best found priorities :" << std::endl;
    int p=rand()%bestpriorities.size();
    std::cout << "CPU: ";
    for(int i=0;i<bestpriorities[p].first.size();++i)
    {
        std::cout << bestpriorities[p].first[i] << ", ";
    }
    std::cout << std::endl << "GPU: ";
    for(int i=0;i<bestpriorities[p].second.size();++i)
    {
        std::cout << bestpriorities[p].second[i] << ", ";
    }
    std::cout << std::endl;
    std::cout << "Best found priorities execution time : " << bestExecTime << std::endl;


*/


    float combinatedBestSlowdown = 0.0f;
    std::vector<float> slowdown;
    std::vector<PriorityMethod> priorityMethods;
    std::vector<std::string> priorityMethodNames;
    priorityMethods.push_back(NOD_TIME_COMBINATION);
    priorityMethodNames.push_back("NOD_TIME_COMBINATION");
    priorityMethods.push_back(BEST_NODS_SCORES);
    priorityMethodNames.push_back("BEST_NODS_SCORE");
    priorityMethods.push_back(BEST_NODS);
    priorityMethodNames.push_back("BEST_NODS");
    priorityMethods.push_back(AUGNOD_PURE);
    priorityMethodNames.push_back("AUGNOD_PURE");
    priorityMethods.push_back(AUGNOD);
    priorityMethodNames.push_back("AUGNOD");
    priorityMethods.push_back(AUGNOD_2);
    priorityMethodNames.push_back("AUGNOD_2");
    priorityMethods.push_back(AUGNOD_DOT_DIFF_PURE);
    priorityMethodNames.push_back("AUGNOD_DOT_DIFF_PURE");
    priorityMethods.push_back(AUGNOD_DOT_DIFF_PURE_2);
    priorityMethodNames.push_back("AUGNOD_DOT_DIFF_PURE_2");
    priorityMethods.push_back(AUGNOD_DOT_REL_DIFF_PURE);
    priorityMethodNames.push_back("AUGNOD_DOT_REL_DIFF_PURE");
    priorityMethods.push_back(AUGNOD_DOT_REL_DIFF_PURE_2);
    priorityMethodNames.push_back("AUGNOD_DOT_REL_DIFF_PURE_2");
    priorityMethods.push_back(AUGNOD_DOT_DIFF_2);
    priorityMethodNames.push_back("AUGNOD_DOT_DIFF_2");
    priorityMethods.push_back(AUGNOD_DOT_DIFF_3);
    priorityMethodNames.push_back("AUGNOD_DOT_DIFF_3");
    priorityMethods.push_back(AUGNOD_DOT_DIFF_4);
    priorityMethodNames.push_back("AUGNOD_DOT_DIFF_4");
    priorityMethods.push_back(AUGNOD_DOT_DIFF_5);
    priorityMethodNames.push_back("AUGNOD_DOT_DIFF_5");
    priorityMethods.push_back(AUGNOD_DOT_DIFF_6);
    priorityMethodNames.push_back("AUGNOD_DOT_DIFF_6");
    priorityMethods.push_back(AUGNOD_DOT_DIFF_7);
    priorityMethodNames.push_back("AUGNOD_DOT_DIFF_7");
    priorityMethods.push_back(AUGNOD_DOT_DIFF_8);
    priorityMethodNames.push_back("AUGNOD_DOT_DIFF_8");
    priorityMethods.push_back(AUGNOD_DOT_DIFF_9);
    priorityMethodNames.push_back("AUGNOD_DOT_DIFF_9");
    priorityMethods.push_back(AUGNOD_DOT_DIFF_10);
    priorityMethodNames.push_back("AUGNOD_DOT_DIFF_10");
    priorityMethods.push_back(AUGNOD_DOT_DIFF_11);
    priorityMethodNames.push_back("AUGNOD_DOT_DIFF_11");
    priorityMethods.push_back(AUGNODS_PER_SECOND);
    priorityMethodNames.push_back("AUGNODS_PER_SECOND");
    priorityMethods.push_back(AUGNODS_PER_SECOND_2);
    priorityMethodNames.push_back("AUGNODS_PER_SECOND_2");
    priorityMethods.push_back(AUGNODS_PER_SECOND_DIFF);
    priorityMethodNames.push_back("AUGNODS_PER_SECOND_DIFF");
    priorityMethods.push_back(AUGNODS_TIME_RELEASED_DIFF);
    priorityMethodNames.push_back("AUGNODS_TIME_RELEASED_DIFF");
    priorityMethods.push_back(AUGNOD_TIME_COMBINATION);
    priorityMethodNames.push_back("AUGNOD_TIME_COMBINATION");
    priorityMethods.push_back(NODS_PER_SECOND);
    priorityMethodNames.push_back("NODS_PER_SECOND");
    priorityMethods.push_back(NODS_TIME_RELEASED);
    priorityMethodNames.push_back("NODS_TIME_RELEASED");
    priorityMethods.push_back(NODS_TIME_RELEASED_DIFF);
    priorityMethodNames.push_back("NODS_TIME_RELEASED_DIFF");
    int columnsPerMethod = 3;

    std::ofstream csvfile("execution_data.csv");
    if(csvfile.is_open() == false){
        throw std::invalid_argument("Cannot open filename");
    }
    csvfile << "priority method;";
    for(unsigned int i=0;i<priorityMethods.size();++i)
    {
        csvfile << priorityMethodNames[i];
        for(unsigned int c=1;c<columnsPerMethod;++c)
        {
            csvfile << ";";
        }
        csvfile << ";";
    }
    csvfile << ";;" << std::endl;
    csvfile << "data index;";
    for(unsigned int i=0;i<priorityMethods.size();++i)
    {
        csvfile << "best execution time;execution time;ratio;";
    }
    csvfile << ";;best heuristic execution time" << std::endl;

    for(unsigned int i=0;i<priorityMethods.size();++i)
    {
        slowdown.push_back(0.0f);
    }

    for(int dataNum=0;dataNum<32;++dataNum)
    {
//if(dataNum != 19)
//{
//continue;
//}

        csvfile << dataNum << ";";

        std::cout << std::endl;
        std::cout << std::endl;
        std::cout << "======== Data " << dataNum << " ========" << std::endl;
        std::cout << std::endl;

        executionData = loadData(dataNum);

        allTasks = GenerateTasksWithPerfectSolution(executionData.seed, executionData.taskNumber, executionData.predecessorRule, executionData.taskTypeProportion, executionData.cpuCost, executionData.gpuCost, executionData.NbCpus, executionData.NbGpus);

        float bestSlowdown = 999999.;


// compute num of each task type
        std::vector<int> taskTypeNum; // number of task of each type

        for(unsigned int t=0;t<executionData.cpuCost.size();++t)
        { // at the beginning, there is 0 of each task
            taskTypeNum.push_back(0);
        }

        for(unsigned int t=0;t<allTasks.size();++t)
        {
            ++taskTypeNum[allTasks[t].getType()];
        }

        std::cout << "Number of tasks for each type : ";
        for(int i=0;i<taskTypeNum.size();++i)
        {
            std::cout << taskTypeNum[i] << " ";
        }
        std::cout << std::endl;

        std::vector<double> cpuCost = executionData.cpuCost;
        std::vector<double> gpuCost = executionData.gpuCost;
        normalizeCosts(allTasks, cpuCost, gpuCost);
        std::cout << "Normalized costs : " << std::endl;
        std::cout << "CPU : ";
        for(int i=0;i<cpuCost.size();++i)
        {
            std::cout << cpuCost[i] << " ";
        }
        std::cout << std::endl;
        std::cout << "GPU : ";
        for(int i=0;i<gpuCost.size();++i)
        {
            std::cout << gpuCost[i] << " ";
        }
        std::cout << std::endl;


        for(unsigned int m=0;m<priorityMethods.size();++m)
        {


            std::vector<int> priorityCpu;
            std::vector<int> priorityGpu;

            std::pair<std::vector<int>, std::vector<int>> priorities;
            priorities.first.clear();
            priorities.second.clear();

            std::cout << "Executing on method " << priorityMethodNames[m] << std::endl;

            if(priorityMethods[m] == BEST_NODS_SCORES)
            {
                priorities = computePriorities<BEST_NODS_SCORES>(allTasks, executionData);
            }
            else if(priorityMethods[m] == BEST_NODS)
            {
                priorities = computePriorities<BEST_NODS>(allTasks, executionData);
            }
            else if(priorityMethods[m] == NOD_TIME_COMBINATION)
            {
                priorities = computePriorities<NOD_TIME_COMBINATION>(allTasks, executionData);
            }
            else if(priorityMethods[m] == AUGNOD_PURE || priorityMethods[m] == AUGNOD || priorityMethods[m] == AUGNOD_2 || priorityMethods[m] == AUGNOD_DOT_DIFF_PURE || priorityMethods[m] == AUGNOD_DOT_DIFF_PURE_2 || priorityMethods[m] == AUGNOD_DOT_REL_DIFF_PURE || priorityMethods[m] == AUGNOD_DOT_REL_DIFF_PURE_2 || priorityMethods[m] == AUGNOD_DOT_DIFF_2 || priorityMethods[m] == AUGNOD_DOT_DIFF_3 || priorityMethods[m] == AUGNOD_DOT_DIFF_4 || priorityMethods[m] == AUGNOD_DOT_DIFF_5 || priorityMethods[m] == AUGNOD_DOT_DIFF_6 || priorityMethods[m] == AUGNOD_DOT_DIFF_7 || priorityMethods[m] == AUGNOD_DOT_DIFF_8 || priorityMethods[m] == AUGNOD_DOT_DIFF_9 || priorityMethods[m] == AUGNOD_DOT_DIFF_10 || priorityMethods[m] == AUGNOD_DOT_DIFF_11 || priorityMethods[m] == AUGNODS_PER_SECOND || priorityMethods[m] == AUGNODS_PER_SECOND_2 || priorityMethods[m] == AUGNODS_PER_SECOND_DIFF || priorityMethods[m] == AUGNODS_TIME_RELEASED_DIFF || priorityMethods[m] == AUGNOD_TIME_COMBINATION)
            {
                // round priorities :
                priorities = computePrioritiesAugmentedNODrounds(priorityMethods[m], allTasks, executionData);
            }
            else if(priorityMethods[m] == NODS_PER_SECOND)
            {
                priorities = computePriorities<NODS_PER_SECOND>(allTasks, executionData);
            }
            else if(priorityMethods[m] == NODS_TIME_RELEASED)
            {
                priorities = computePriorities<NODS_TIME_RELEASED>(allTasks, executionData);
            }
            else if(priorityMethods[m] == NODS_TIME_RELEASED_DIFF)
            {
                priorities = computePriorities<NODS_TIME_RELEASED_DIFF>(allTasks, executionData);
            }

//if(dataNum == 19)
//{
//    priorities.first = std::vector<int> {7, 1, 6, 5, 0, 3, 2, 4};
//    priorities.second = std::vector<int>{2, 3, 0, 5, 1, 6, 7, 4};
//}

            priorityCpu = priorities.first;
            priorityGpu = priorities.second;

            // -----------------------------------------------------------

            ExecutionStatistics executionStatistics;

            double executionTime = Execute(allTasks, executionData.cpuCost, executionData.gpuCost, priorityCpu, priorityGpu,
                                                 executionData.NbCpus, executionData.NbGpus, &executionStatistics);

            std::cout << "Found priorities:" << std::endl;
            std::cout << "CPU :";
            for(int i=0;i<priorities.first.size();++i)
            {
                std::cout << priorities.first[i] << " ";
            }
            std::cout << std::endl;
            std::cout << "GPU :";
            for(int i=0;i<priorities.second.size();++i)
            {
                std::cout << priorities.second[i] << " ";
            }
            std::cout << std::endl;


            std::cout << "Execution time is " << executionTime << std::endl;
            //std::cout << "Theoretical lower bound is " << LowerBound(allTasks, executionData.cpuCost, executionData.gpuCost, priorityCpu, priorityGpu, executionData.NbCpus, executionData.NbGpus) << std::endl;
            std::cout << "Best execution was " << getBestExecutionTime(dataNum) << " (x" << executionTime / getBestExecutionTime(dataNum) << ")" << std::endl;



            csvfile << getBestExecutionTime(dataNum) << ";" << executionTime << ";" << (executionTime / getBestExecutionTime(dataNum)) << ";";


            slowdown[m] += executionTime / getBestExecutionTime(dataNum);
            if((executionTime / getBestExecutionTime(dataNum)) < bestSlowdown)
            {
                bestSlowdown = (executionTime / getBestExecutionTime(dataNum));
            }
        }

        combinatedBestSlowdown += bestSlowdown;
        csvfile << ";;" << bestSlowdown << std::endl;
    }

    csvfile << "average;";
    for(unsigned int m=0;m<priorityMethods.size();++m)
    {
        std::cout << "Slowdown method " << priorityMethodNames[m] << " : " << slowdown[m]/32.0f << std::endl;
        csvfile << ";;" << (slowdown[m]/32.0f) << ";";
    }
    csvfile << ";;" << (combinatedBestSlowdown/32.0f);
    std::cout << "Combinated best slowdown : " << combinatedBestSlowdown/32.0f << std::endl;



/*
    allTasks = GenerateTasksFromDotFile("/home/clement/Documents/Seafile/StageM2/auto-heteroprio/Examples/Emulator/fmm2.dot");

    std::cout << "There are " << allTasks.size() << " tasks." << std::endl;

    srand(0);

    int taskTypeNum = 8;
    ExecutionStatistics executionStatistics;

    executionData.taskNumber = allTasks.size();
    executionData.cpuCost = getRandomCosts(taskTypeNum);
    executionData.gpuCost = getRandomCosts(taskTypeNum);
    makeAtLeastOneCostBetterForEachArch(executionData.cpuCost, executionData.gpuCost);
    //executionData.taskTypeProportion = getRandomTaskProportion(taskTypeNum);
    executionData.NbCpus = getRandomPUnum();
    executionData.NbGpus = getRandomPUnum();
    //executionData.predecessorRule = getRandomPredecessorRule(taskTypeNum);
    //executionData.seed = rand();

    std::pair<std::vector<int>, std::vector<int>> priorities = computePriorities<NOD_TIME_COMBINATION>(allTasks, executionData);

    double executionTime = Execute(allTasks, executionData.cpuCost, executionData.gpuCost, priorities.first, priorities.second,
                                                 executionData.NbCpus, executionData.NbGpus, &executionStatistics);

    std::cout << "execution time : " << executionTime << std::endl;
    std::cout << "CPU busy time : " << executionStatistics.busyTime[0] << std::endl;
    std::cout << "GPU busy time : " << executionStatistics.busyTime[1] << std::endl;
    std::cout << "CPU busy % : " << (executionStatistics.busyTime[0] / executionTime / (double)executionData.NbCpus) << std::endl;
    std::cout << "GPU busy % : " << (executionStatistics.busyTime[1] / executionTime / (double)executionData.NbGpus) << std::endl;

    std::cout << "Lower bound : " << LowerBound(allTasks, executionData.cpuCost, executionData.gpuCost, priorities.first, priorities.second,
                                                 executionData.NbCpus, executionData.NbGpus) << std::endl;
*/


    // -----------------------------------------------------------
/*
    std::vector<std::string> workerLabels;
    for(int idxCpu = 0 ; idxCpu < executionData.NbCpus ; ++idxCpu){
        workerLabels.emplace_back("Cpu-"+std::to_string(idxCpu));
    }
    for(int idxGpu = 0 ; idxGpu < executionData.NbGpus ; ++idxGpu){
        workerLabels.emplace_back("Gpu-"+std::to_string(idxGpu));
    }

    EmulGenerateTrace("/tmp/trace.svg", allTasks, 0, executionData.NbCpus + executionData.NbGpus, workerLabels);
*/

    //test_train();
    saveDataStatisticsToFile();

    return 0;
}

